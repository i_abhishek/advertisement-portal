<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%>
<script src="scripts/homeV2.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="content animate-panel">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Dashboard Information And Statistics
                    </div>
                    <div class="panel-body">
                        <div class="text-left">
                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                        </div>
                        <div id="report_data_button">
                            <div style="padding-left: 60%">
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="dailyTxReport()"> Daily expenditure</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="lastSevenDayEx()"> Last 7 day expenditure</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="monthlyTxReport()"> Last 30 day's expenditure</button>
                            </div>
                        </div>
                        <hr class="m-b-xl"/>
                         
                         <div  id="expenditureReport">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp"%> 
    </div>
</div>
    <script>
        document.getElementById("report_data_button").style.display = "none";
    </script>
