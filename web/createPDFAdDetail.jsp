<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/pdfAd.js" type="text/javascript"></script>

<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                PDF Ad
            </h2>
            <small>Add your Ad image </small>
        </div>
    </div>
</div>

<%
    SgAdvertiseSubscriptionDetails subscriptionDetails = (SgAdvertiseSubscriptionDetails) request.getSession().getAttribute("advertiserSubscriptionObject") ;
    String pdfImageLength = ""; String message = "NA"; int width = 0; int height = 0;  
    if(subscriptionDetails != null){
        String pdfAdDetails = subscriptionDetails.getPdfAdConfiguration();
        JSONObject pdfDetails = new JSONObject(pdfAdDetails);
        if(pdfDetails.has("pdfImageLength")){
            pdfImageLength = pdfDetails.getString("pdfImageLength");
        }
    }
    if(!pdfImageLength.isEmpty()){
        if(pdfImageLength.equalsIgnoreCase("1")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 1024, Height = 768";
           width = 1024;
           height = 768;
        }else if(pdfImageLength.equalsIgnoreCase("1/2")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 640, Height = 480";
           width = 640;
           height = 480;
        }else if(pdfImageLength.equalsIgnoreCase("1/4")){
           message = "Upload Advertisement image. Support Image File size limit 5mb with at least Width = 368, Height = 245";
           width = 368;
           height = 245;
        }
    }
%>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                                                                                                           
                    <h6><i class="fa fa-image fa-2x text-info"> PDF Ad Image</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                            
                            <small><%=message%></small>
                        </div>
                    </div> 
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="updatePDFAd()">Save</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 50%"></div>
    </div>
</div>
<script>
    $(document).ready(function () {        
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadPDFAdImage",
            uploadMultiple: false,            
            maxFilesize: 5,
            maxFiles: 1,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop advertisement image here to upload",
            init: function() {
                // Register for the thumbnail callback.
                // When the thumbnail is created the image dimensions are set.
                this.on("thumbnail", function(file) {
                  // Do the dimension checks you want to do
                  console.log("file.width == "+file.width);
                  console.log("file.height == "+file.height);                  
                  if (file.width < <%=width%> || file.height < <%=height%>) {
                    file.rejectDimensions()
                  }
                  else {
                    file.acceptDimensions();
                    //file.rejectDimensions()
                  }
                });
              },
              // Instead of directly accepting / rejecting the file, setup two
            // functions on the file that can be called later to accept / reject
            // the file.
            accept: function(file, done) {
              file.acceptDimensions = done;
              file.rejectDimensions = function() { done("The image must be at least <%=width%> x <%=height%>"); };
              // Of course you could also just put the `done` function in the file
              // and call it either with or without error in the `thumbnail` event
              // callback, but I think that this is cleaner.
            },
            removedfile: function(file) {
                removePDFAdImageFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

