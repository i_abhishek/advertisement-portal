
<!DOCTYPE HTML>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      		
      <meta charset="utf-8">
      		
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--      <script type="text/javascript">
          window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1059.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);
                </script>-->
      		
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      		      
      <link href="//ajax.googleapis.com" rel="dns-prefetch">
      		
<!--      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css">
      		-->
      <link rel="stylesheet" type="text/css" href="https://www.usu.edu/style/classic-full.min.css">
      		
      		
<!--      <link rel="stylesheet" type="text/css" href="https://ouresources.usu.edu/_resources/css/global-custom.css">-->
      		
      		
      		
      		
      					
      				
<!--      <link rel="stylesheet" type="text/css" href="https://ouresources.usu.edu/_resources/css/profile-page-v0-classic.css">-->
      			
      		
      
      		
<!--      <link href="/_resources/css/prettyPhoto.css" rel="stylesheet" type="text/css">-->
      		
      		
      		
      		
      
      		
      		
      		 
      
      		
      
      		
      		
      		
      		
      		
      		
<!--      		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
                <script src="vendor/jquery/dist/jquery.min.js"></script>
                <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<!--      		<script src="https://www.usu.edu/assets/bootstrap/3.3.2/dist/js/bootstrap.min.js"></script>-->
      		
      		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<!--      		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>-->
                 <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
      		
      		<!-- This is the sites headCode -->
<!-- please don't remove, not pushed to production -->

<link rel="icon" href="/_resources/global_includes/favicon.ico" />

<!-- End of sites headCode -->

<!-- styles of education outreach -->
<!--<link href='/_resources/css/marketing_template_styles.css?v=2' rel='stylesheet' type='text/css' />

<link href="/_resources/css/custom.css" rel='stylesheet' type='text/css' />
<link href="/_resources/css/tiledHome.css" rel='stylesheet' type='text/css' />-->
<script src=""></script>
 
      
      	</head>
   <body class=""><a href="#content" class="sr-only">Skip to main content</a>             
      <div class="main-container-wide">
         <div class="&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;main-container-wrapper&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;">
            <div class="row" id="content">
               <div class="main-content" role="main">

			
                  <hr>
			
                  <div class="row">
				
                     <div class="col-sm-8 col-sm-offset-2">
					
                        <div class="lightbox-gallery dark mrb35">
                           <ul id="videos-without-poster" class="list-unstyled row">
                              <li class="video" data-src="https://www.youtube.com/watch?v=CZd6jWdm6eU" data-sub-html="Good Girls become Guide Dogs"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9a655a8d-622a-4a43-849d-79de45846ce3.jpg" alt="Good Girls become Guide Dogs"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=dQw4w9WgXcQ" data-sub-html="Red Hat Club"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/25df9b02-3c31-4e27-80a2-948cbe043a5d.jpg" alt="Red Hat Club"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Curabitur non placerat lorem"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/166b6e0e-156b-4e99-8409-fad627ee398a.jpg" alt="Curabitur non placerat lorem"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/09194722-27aa-4ea1-b6c4-c84cfeae6641.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/ecaa08f9-b30e-4102-9f50-ae53bf539809.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9ec3b7e1-9f72-42c1-a8de-ec8a87721979.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/e0ab68dc-f0a0-4b14-8bb5-9e82eba3fec1.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                              <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="A New Zealand Working Dog"><a href=""><img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/00a3c536-1152-4908-9418-490caf5e95e8.jpg" alt="A New Zealand Working Dog"><div class="demo-gallery-poster"><img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png"></div></a></li>
                           </ul>
                        </div>
				
                     </div>
			
                  </div>
			
			
                  
			
                  <hr>
               </div>
            </div>
         </div>
      </div>

       <script type="text/javascript">
			/*for disqus comments*/
			//var page_id="https://templates.usu.edu/content/advanced/features/images-videos/lightbox-video-gallery.php";
		</script>
<!--                <script src="https://templates.usu.edu/_resources/assets/js/template/init.js"></script>-->
                <script src="scripts/video/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
                <script src="scripts/video/fileinput.min.js" type="text/javascript"></script>
                <script src="scripts/video/bootstrap-select.min.js" type="text/javascript"></script>
                <script src="scripts/video/accordionSnippet.js" type="text/javascript"></script>
                <script src="scripts/video/tabSnippet.js" type="text/javascript"></script>
<!--                <script src="https://templates.usu.edu/_resources/assets/components/blueimp-gallery/js/jquery.blueimp-gallery.min.js"></script>
                <script src="https://templates.usu.edu/_resources/assets/components/bootstrap-jasny/js/fileinput.min.js"></script>
                <script src="https://templates.usu.edu/_resources/assets/components/bootstrap-select/js/bootstrap-select.min.js"></script>-->
<!--                <script src="https://ouresources.usu.edu/_resources/js/passwordProtect.js"></script>-->
<!--                <script src="https://ouresources.usu.edu/_resources/js/accordionSnippet.js"></script>
                <script src="https://ouresources.usu.edu/_resources/js/tabSnippet.js"></script>-->
                <script>
			$(document).ready(function() {
			$('.selectpicker').selectpicker();
			});
		</script>
<!--                <script src="https://templates.usu.edu/_resources/assets/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>-->
                <script src="scripts/video/bootstrap-datetimepicker.js" type="text/javascript"></script>
                <script type="text/javascript">
			$(".form_datetime").datetimepicker({
			format: 'mm-dd-yyyy H:ii P',
			autoclose: 1,
			showMeridian: 1
			});
			$(".form_date").datetimepicker({
			format: 'mm-dd-yyyy',
			autoclose: 1,
			showMeridian: 1,
			minView: 2,
			forceParse: 0
			});
			$(".form_time").datetimepicker({
			format: 'H:ii P',
			autoclose: 1,
			showMeridian: 1,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0
			});
		</script>

<!--<script type="text/javascript" src="https://ouresources.usu.edu/_assets/forms/ouforms.js"></script>-->
<!--<script type="text/javascript" src="https://ouresources.usu.edu/_assets/forms/jquery.datetimepicker.full.min.js"></script>-->
<script src="scripts/video/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

<script src="scripts/video/picturefill.min.js" type="text/javascript"></script>
<script src="scripts/video/lightgallery.js" type="text/javascript"></script>
<script src="scripts/video/lg-fullscreen.js" type="text/javascript"></script>
<script src="scripts/video/lg-thumbnail.js" type="text/javascript"></script>
<script src="scripts/video/lg-video.js" type="text/javascript"></script>
<script src="scripts/video/lg-autoplay.js" type="text/javascript"></script>
<script src="scripts/video/demos.js" type="text/javascript"></script>
<script src="scripts/video/lg-zoom.js" type="text/javascript"></script>
<script src="scripts/video/lg-hash.js" type="text/javascript"></script>
<script src="scripts/video/lg-pager.js" type="text/javascript"></script>
<script src="scripts/video/jquery.mousewheel.min.js" type="text/javascript"></script>
<link href="scripts/video/lightgallery.css" rel="stylesheet" type="text/css"/>
<link href="scripts/video/videogallery.css" rel="stylesheet" type="text/css"/>

<!--<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lightgallery.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-fullscreen.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-thumbnail.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-video.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-autoplay.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/demos.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-zoom.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-hash.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/lg-pager.js"></script>
<script src="https://ouresources.usu.edu/_assets/galleries/light-gallery/js/jquery.mousewheel.min.js"></script>
<link rel="stylesheet" href="https://ouresources.usu.edu/_assets/galleries/light-gallery/css/lightgallery.css">
<link rel="stylesheet" href="https://ouresources.usu.edu/_assets/galleries/light-gallery/css/videogallery.css">-->
      <!-- please don't remove, not pushed to production -->
<!--<script src="/_resources/js/custom.js"></script>-->
<!--   <script type="text/javascript">
       window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"bab786ce59","applicationID":"39467101","transactionName":"ZAZQYEoCCBBXAkxdX11MZ0ZRTAUMWBVdWkQcAlZCWQ0FBlJOXlFRRxZAUUtMDw5XBl1HHUUKVlFXEEkPXwZQQFJcGx9CUQcDDBsGWVhcVhFLGkgLFg==","queueTime":0,"applicationTime":2,"atts":"SEFTFgIYGx4=","errorBeacon":"bam.nr-data.net","agent":""}
    </script>-->
<!--   </body>
</html>-->