
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_ChannelId");
    String apName = request.getParameter("_apId");
    String resourceName = request.getParameter("_resourceId");
    String version = request.getParameter("version");
    //String apiName = request.getParameter("apiName");
%> 
<br><br><br>
<form class="form-horizontal" id="assign_token_form" name="assign_token_form" role="form">
    <input type="hidden" name="apName" id="apName" value="<%=apName%>">
    <input type="hidden" name="resourceName" id="resourceName" value="<%=resourceName%>">
    <input type="hidden" name="version" id="version" value="<%=version%>">
    <table id="api" class="table table-striped table-bordered table-hover">  
        <thead>
            <tr>
                <th style="text-align: center">No.</th>
                <th style="text-align: center">API</th>
                <th style="text-align: center">(Re)Assign Token</th>
                <th style="text-align: center">View Token</th>
                <th style="text-align: center">Send Token (Email)</th>
            </tr>
        </thead>
        <tbody>
        <%
            AccessPointManagement ppw = new AccessPointManagement();
            Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apName);
            Map methodMap = null;
            String value = apName;
            ResourceDetails rd = new ResourceManagement().getResourceByName(SessionId, channelId, resourceName);
            TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, accesspoint.getApId(), rd.getResourceId());
            byte[] meBy = tf.getMethods();
            Object obj = Serializer.deserialize(meBy);
            Map tmpMethods = (Map) obj;
            Methods methods = (Methods) tmpMethods.get("" + version);
            if (methodMap == null) {
                methodMap = new HashMap();
            }
            methodMap.put(value, methods);
            String mName = "";
            List list = methods.methodClassName.methodNames;
            int count = 0;
            for (int i = 0; i < list.size(); i++) {
                MethodName methodName = (MethodName) list.get(i);
                if (methodName.visibility.equalsIgnoreCase("yes")) {
                    mName = methodName.methodname.split("::")[0];
                    if (!methodName.transformedname.equals("")) {
                        mName = methodName.transformedname.split("::")[0];
                    }
                    count = count+1;
        %>
        <tr style="text-align: center">    
            <td>
                <%=count%>                
            </td>
            <td><%=mName%>
                <!--<label class=" control-label"><%=mName%></label>-->
                <input type="hidden" name="apiName" id="apiName" value="<%=mName%>">
            </td>
            <td>
                <a class="btn btn-warning btn-xs ladda-button" data-style="zoom-in" id="reAssignToken<%=i%>" onclick="assignAPIToken('<%=mName%>', '<%=i%>')" type="button" href="#"><i class="fa fa-eye"></i> <span class="bold">(Re)Assign Token</span></a>

            <td>
                <a class="btn btn-success btn-xs ladda-button" data-style="zoom-in" id="viewToken<%=i%>" onclick="viewAPIToken('<%=mName%>', '<%=i%>')" type="button" href="#"><i class="fa fa-refresh"></i> <span class="bold">View Token</span></a>

            </td>
            <td>
                <a class="btn btn-info btn-xs ladda-button" data-style="zoom-in" id="sendApi<%=i%>" onclick="emailAPIToken('<%=mName%>', '<%=i%>')" type="button<%=i%>" href="#"><i class="fa fa-envelope"></i> <span class="bold">Send Token (Email)</span></a>
            </td>   
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
</form>
<script>
                $(document).ready(function () {
                    $('#api').DataTable({
                        responsive: true
                    });
                });
    </script>
    
<script>
         function copy(text,id) {
                console.log('text >> '+text);
                console.log('Decode ?? '+Base64.decode(text));
                console.log("id >> "+id);
                document.getElementById("copyTarget").value = Base64.decode(text);
                document.getElementById("copyTarget").style.display = 'block';
                copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
                document.getElementById("copyTarget").style.display = 'none';
            }
        function copyToClipboardMsg(elem, msgElem) {
                var succeed = copyToClipboard(elem);
                var msg;
                if (!succeed) {
                    msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
                } else {
                    msg = "Text copied to the clipboard."
                }
                console.log(msg);
//                if (typeof msgElem === "string") {
//                    msgElem = document.getElementById(msgElem);
//                }
//                msgElem.innerHTML = msg;
//                setTimeout(function () {
//                    msgElem.innerHTML = "";
//                }, 2000);
            }
            
        function copyToClipboard(elem) {
                // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                var origSelectionStart, origSelectionEnd;
                if (isInput) {
                    // can just use the original source element for the selection and copy
                    target = elem;
                    origSelectionStart = elem.selectionStart;
                    origSelectionEnd = elem.selectionEnd;
                } else {
                    // must use a temporary form element for the selection and copy
                    target = document.getElementById(targetId);
                    if (!target) {
                        var target = document.createElement("textarea");
                        target.style.position = "absolute";
                        target.style.left = "-9999px";
                        target.style.top = "0";
                        target.id = targetId;
                        document.body.appendChild(target);
                    }
                    target.textContent = elem.textContent;
                }
                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);

                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                } catch (e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }

                if (isInput) {
                    // restore prior selection
                    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                } else {
                    // clear temporary content
                    target.textContent = "";
                }
                return succeed;
            }
            
            var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                    var t = "";
                    var n, r, i, s, o, u, a;
                    var f = 0;
                    e = Base64._utf8_encode(e);
                    while (f < e.length) {
                        n = e.charCodeAt(f++);
                        r = e.charCodeAt(f++);
                        i = e.charCodeAt(f++);
                        s = n >> 2;
                        o = (n & 3) << 4 | r >> 4;
                        u = (r & 15) << 2 | i >> 6;
                        a = i & 63;
                        if (isNaN(r)) {
                            u = a = 64
                        } else if (isNaN(i)) {
                            a = 64
                        }
                        t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                    }
                    return t
                }, decode: function (e) {
                    var t = "";
                    var n, r, i;
                    var s, o, u, a;
                    var f = 0;
                    e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                    while (f < e.length) {
                        s = this._keyStr.indexOf(e.charAt(f++));
                        o = this._keyStr.indexOf(e.charAt(f++));
                        u = this._keyStr.indexOf(e.charAt(f++));
                        a = this._keyStr.indexOf(e.charAt(f++));
                        n = s << 2 | o >> 4;
                        r = (o & 15) << 4 | u >> 2;
                        i = (u & 3) << 6 | a;
                        t = t + String.fromCharCode(n);
                        if (u != 64) {
                            t = t + String.fromCharCode(r)
                        }
                        if (a != 64) {
                            t = t + String.fromCharCode(i)
                        }
                    }
                    t = Base64._utf8_decode(t);
                    return t
                }, _utf8_encode: function (e) {
                    e = e.replace(/rn/g, "n");
                    var t = "";
                    for (var n = 0; n < e.length; n++) {
                        var r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r)
                        } else if (r > 127 && r < 2048) {
                            t += String.fromCharCode(r >> 6 | 192);
                            t += String.fromCharCode(r & 63 | 128)
                        } else {
                            t += String.fromCharCode(r >> 12 | 224);
                            t += String.fromCharCode(r >> 6 & 63 | 128);
                            t += String.fromCharCode(r & 63 | 128)
                        }
                    }
                    return t
                }, _utf8_decode: function (e) {
                    var t = "";
                    var n = 0;
                    var r = c1 = c2 = 0;
                    while (n < e.length) {
                        r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r);
                            n++
                        } else if (r > 191 && r < 224) {
                            c2 = e.charCodeAt(n + 1);
                            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                            n += 2
                        } else {
                            c2 = e.charCodeAt(n + 1);
                            c3 = e.charCodeAt(n + 2);
                            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                            n += 3
                        }
                    }
                    return t
                }}
    </script>