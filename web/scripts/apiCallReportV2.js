/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function apiCallReportAccesspoint(value) {    
    var s = './apiCallVersion.jsp?_accesspointId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPICallReport').html(data); 
            var version = document.getElementById("_VersionForAPICallReport").value;
            apiCallReportAPI(version);
        }
    });
}

function apiCallReportAPI(versionData) {
    var ap = document.getElementById("_resource").value;    
    var s = './apiCallReportAPI.jsp?versionData=' + versionData + '&_apId=' + ap ;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForAPICallReport').html(data);                        
        }
    });
}

function generateAPICallreports() {
    var l = $('#generateButton').ladda();
    l.ladda('start');
    var month = document.getElementById('_apiCallMonth').value;
    var year = document.getElementById('_apiCallYear').value;
    var version = document.getElementById("_VersionForAPICallReport").value;
    var api = document.getElementById("_APIForAPICallReport").value;
    var accesspointPlusResource = document.getElementById("_resource").value;
    initializeToastr(); 
    if(accesspointPlusResource == "-1"){
        setTimeout(function () {
            toastr.error('Error - ' + 'Please select resource first');
            l.ladda('stop');
        }, 3000);
        return;
    }
    var s = './apichartreport.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            //$('#report_data').html(data);                        
                var linedata = UserAPIForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var arrYaxis1 = [];
                var arrXaxis1 = [];
                arrXaxis1.push("Data e Ora");
                arrYaxis1.push('Call Count');
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxis1.push(attrValue1.label);
                    arrYaxis1.push(attrValue1.value);
                    //arrYaxis1.push(10);
                }
                if (arrXaxis1.length === 1) {
                    var tempStore = arrXaxis1;
                    var date = new Date(arrXaxis1);
                    date.setDate(date.getDate() - 1);
                    arrXaxis1 = [];
                    arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis1.push(tempStore);
                    arrYaxis1.push(0);
                }
                $("#linegraph").empty();
                console.log("Date >> "+arrXaxis1);
                console.log("Call Count >> "+arrYaxis1);
                c3.generate({
                    bindto: '#report_data',
                    data: {
                        x: 'Data e Ora',
                        xFormat: '%Y-%m-%d %H:%M:%S',
                        columns: [
                            arrXaxis1,
                            arrYaxis1
                            //data1
                        ],
                        colors: {
                            //arrYaxis1: '#62cb31',
                            arrYaxis1: '#FF7F50'
                        },
                        types: {
                            arrXaxis1: 'bar'
                        },
                        groups: [
                            ['Call Count']
                        ]
                    },
                    subchart: {
                            show: true
                        },
                    axis: {
                        x: {
                            type: 'timeseries',
                            // if true, treat x value as localtime (Default)
                            // if false, convert to UTC internally                        
                            tick: {
                                format: '%Y-%m-%d'
                                        //fomat:function (x) { return x.getFullYear(); }
                            }
                        }
                    }
                });    
                //document.getElementById("report").style.display = "block";
                l.ladda('stop');          
        }
    });
}

function UserAPIForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APICallForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function generateAPITranscationreports() {
    var l = $('#generateButton').ladda();
    l.ladda('start');
    var month = document.getElementById('_apiCallMonth').value;
    var year = document.getElementById('_apiCallYear').value;
    var version = document.getElementById("_VersionForAPICallReport").value;
    var api = document.getElementById("_APIForAPICallReport").value;
    var accesspointPlusResource = document.getElementById("_resource").value;
    initializeToastr();      
    if(accesspointPlusResource == "-1"){
        setTimeout(function () {
                    toastr.error('Error - ' + 'Please select resource first');
                    l.ladda('stop');   
                }, 3000);
                return;
    }
    var s = './apichartreport.jsp?type='+2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            //$('#report_data').html(data);                        
                var linedata       = UserAPITranscationForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var notAllowedData = UserAPIFailedTranscationForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var arrYaxis1 = [];
                var arrXaxis1 = [];
                var arrYaxis = [];
                var arrXaxis = [];
                arrXaxis1.push("Data e Ora");
                arrYaxis1.push('Success Transaction');
                arrYaxis.push('Failed Transaction');
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxis1.push(attrValue1.label);
                    arrYaxis1.push(attrValue1.value);
                }
                if (arrXaxis1.length === 1) {
                    var tempStore = arrXaxis1;
                    var date = new Date(arrXaxis1);
                    date.setDate(date.getDate() - 1);
                    arrXaxis1 = [];
                    arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis1.push(tempStore);
                    arrYaxis1.push(0);
                }
                for (var key in notAllowedData) {
                    var attrName1 = key;
                    var attrValue1 = notAllowedData[key];
                    arrXaxis.push(attrValue1.label);
                    arrYaxis.push(attrValue1.value);
                }
                if (arrXaxis.length === 1) {
                    var tempStore = arrXaxis;
                    var date = new Date(arrXaxis);
                    date.setDate(date.getDate() - 1);
                    arrXaxis = [];
                    arrXaxis.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis.push(tempStore);
                    arrYaxis.push(0);
                }
                $("#report_data").empty();
                console.log("arrXaxis1 >> "+arrXaxis1);
                console.log("arrYaxis1 >> "+arrYaxis1);
                console.log("arrYaxis  >> "+arrYaxis);
                c3.generate({
                        bindto: '#report_data',
                        data: {
                            x: 'Data e Ora',
                            xFormat: '%Y-%m-%d %H:%M:%S',
                            columns: [
                                arrXaxis1,
                                arrYaxis1,
                                arrYaxis
                            ],
                            colors: {
                                arrYaxis1: '#62cb31',
                                arrYaxis: '#FF7F50'
                            },
                            types: {
                                arrYaxis1: 'bar'
                            },
                            groups: [
                                ['Transcation Amount', 'Call Count']
                            ]
                        },
                        subchart: {
                            show: true
                        },
                        axis: {
                            x: {
                                type: 'timeseries',
                                // if true, treat x value as localtime (Default)
                                // if false, convert to UTC internally                        
                                tick: {
                                    format: '%d-%m-%Y'
                                            //fomat:function (x) { return x.getFullYear(); }
                                }
                            }
                        }
                    });
                l.ladda('stop');          
        }
    });
}
function UserAPITranscationForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APITranscationForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function UserAPIFailedTranscationForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APIFailedTranscationForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}