/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function checkOut(packageName, brandName, newPackage) {
    var promocodeId = document.getElementById('_promocodeId').value;
    var count = document.getElementById('countDetails').value;
    var totalPaymentAmount = document.getElementById('totalBenefitAmount').value;
    var s = './CheckOut?_packageName=' + packageName + '&_brandname=' + brandName + '&_newPackage=' + newPackage + '&_totalPaid=' + totalPaymentAmount + '&_promocodeId=' + promocodeId;

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
            } else if (strCompare(data.result, "success") === 0) {
                callXpay(data);
            }
        }
    });
}

function payWithXPAY() {
    var s = './XpayPayment';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#paymentDetailsForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                //$('#validate-promo-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strCompare(data.result, "success") === 0) {
                callXpay(data);
            }
        }
    });
}


function callXpay(data) {

    var s = data.strXPAYPaymentPage;

    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", s);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "merchantID");
    field.attr("value", data.merchantID);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "sessionid");
    field.attr("value", data.sessionid);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "paymenttokenid");
    field.attr("value", data.payTokenid);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "description");
    field.attr("value", data.description);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "expirytime");
    field.attr("value", data.expirytime);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "amount");
    field.attr("value", data.purchaseAmount);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "quantity");
    field.attr("value", data.quantity);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "item");
    field.attr("value", data.item);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "invoiceid");
    field.attr("value", data.invoiceid);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "currencytype");
    field.attr("value", data.currency);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "transactionID");
    field.attr("value", data.transactionID);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "successUrlToReturn");
    field.attr("value", data.successUrlToReturn);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "errorUrl");
    field.attr("value", data.errorUrl);
    form.append(field);

    $(document.body).append(form);
    form.submit();
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function payWithTM() {
    var s = './TMPayment';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#paymentDetailsForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                //$('#validate-promo-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strCompare(data.result, "success") === 0) {
                callTM(data);
            }
        }
    });
}

function callTM(data) {

    var s = data.strXPAYPaymentPage;

    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", s);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "MERCHANTID");
    field.attr("value", data.merchantID);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "MERCHANT_TRANID");
    field.attr("value", data.merchantTranscationId);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "CURRENCYCODE");
    field.attr("value", data.currencyCode);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "AMOUNT");
    field.attr("value", data.amount);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "CUSTNAME");
    field.attr("value", data.custName);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "CUSTEMAIL");
    field.attr("value", data.custEMAIL);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "RETURN_URL");
    field.attr("value", data.returnURL);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "SIGNATURE");
    field.attr("value", data.signature);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "TOTAL_CHILD_TRANSACTION");
    field.attr("value", data.totalChildTransaction);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "CHILD_TRANSACTION");
    field.attr("value", data.childTranscation);
    form.append(field);

    $(document.body).append(form);
    form.submit();
}

function downloadInvoice(invoiceId) {
    var s = './GetInvoiceDownload?invoiceId=' + invoiceId;
    window.location.href = s;
    return false;
}
