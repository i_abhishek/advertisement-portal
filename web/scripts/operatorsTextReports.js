function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Operatortextreport(Optype) {
    var l = $('#generateReport').ladda();
    l.ladda('start');
    var type = document.getElementById('_Rtype').value;
    var sdate = document.getElementById('datapicker1').value;
    var edate = document.getElementById('datapicker2').value;
    if (sdate === '' || edate === '') {
        initializeToastr();
        toastr.error('Error - '+'Please Select the Date ranges !');
        return;
    } else if (type === -1) {
        bootboxmodel("<h3>Please Select the Report type!!!<h3>");
        return;
    } else {
        l.ladda('start');
        if (Optype === 1) {
            var Accesspoint = document.getElementById('_Accesspoint').value;
            var Group = document.getElementById('_Group').value; 
            var Resource = document.getElementById('_Resources').value;
            var partner = document.getElementById('_Partner').value;
            var stime = document.getElementById('_ApStartTime').value;
            var etime = document.getElementById('_ApEndTime').value;
            if (type === 'pdf') {
                userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
            else if (type === 'txt') {
                userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
            else if (type === 'csv') {
                userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
            waiting.modal('hide');
        }
        else {
            var Accesspoint = document.getElementById('_Accesspoint').value;;
            var Group = -1;
            var Resource = -1;
            var partner = document.getElementById('pId').value;
//            var stime = document.getElementById('_ApStartTime').value;
//            var etime = document.getElementById('_ApEndTime').value;
            var stime = "00:00 AM";
            var etime = "23:59 PM";
            if (type === 'pdf') {
                userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
            else if (type === 'txt') {
                userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
            else if (type === 'csv') {
                userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                l.ladda('stop');
            }
        }
    }
}
function userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=2";
    window.location.href = s;
    return false;
}
