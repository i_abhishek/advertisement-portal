/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var i = 0;
var c = 0;
var a = 0;
function addNewRMethods(_resId, _resName) {
    i = 0;
    window.location = "./addResourceWithParams.jsp?_resId=" + _resId + "&_resName=" + _resName + "&paramCount=";
}

k = 0;
function  addHeader() {
    k++;
    var table = document.getElementById("_headerTable");
    var row = table.insertRow(k);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = "<input type='text' class='form-control' id='RMText" + i + "'name='RMText" + i + "'placeholder='Key'>";

    cell2.innerHTML = "<div style='padding: 1%'><input type='text' class='form-control' id='RMType" + i + "' name='RMType" + i + "' placeholder='Value'></div>";

    cell3.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addHeader()' id='addUserButton" + i + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeHeader()' id='minusUserButton" + i + "' ><span class='fa fa-minus-circle'></span></a>";

    var data = document.getElementById("RMText" + i);

    if (i >= 1) {
        var j = i - 1;
        document.getElementById("addUserButton" + j).style.display = 'none';
        document.getElementById("minusUserButton" + j).style.display = 'none';
    } else {
        document.getElementById("addUserButton").style.display = 'none';
        document.getElementById("minusUserButton").style.display = 'none';
    }
    i = i + 1;
    document.getElementById("paramcountRM").value = i;
    document.getElementById("soapUserDefinedHeader").value = i;
    
}
function  removeHeader() {
    if (k >= 1) {
        document.getElementById("_headerTable").deleteRow(k);
        k--;
        i = i - 1;
        document.getElementById("soapUserDefinedHeader").value = i;
        var m = i - 1;
        if (m < 0) {
            document.getElementById("addUserButton").style.display = 'block';
            document.getElementById("minusUserButton").style.display = 'block';
        } else {
            document.getElementById("addUserButton" + m).style.display = 'block';
            document.getElementById("minusUserButton" + m).style.display = 'block';
        }
    }
}

// rest header parameter
l = 0;
function  addRestHeader() {
    l++;
    var table = document.getElementById("_headerRestTable");
    var row = table.insertRow(l);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = "<input type='text' class='form-control' id='RMTextRest" + c + "'name='RMTextRest" + c + "'placeholder='Key'>";

    cell2.innerHTML = "<div style='padding: 1%'><input type='text' class='form-control' id='RMTypeRest" + c + "' name='RMTypeRest" + c + "' placeholder='Value'></div>";

    cell3.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addRestHeader()' id='addUserButtonRest" + c + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeRestHeader()' id='minusUserButtonRest" + c + "' ><span class='fa fa-minus-circle'></span></a>";

    if (c >= 1) {
        var j = c - 1;
        document.getElementById("addUserButtonRest" + j).style.display = 'none';
        document.getElementById("minusUserButtonRest" + j).style.display = 'none';
    } else {
        document.getElementById("addUserButtonRest").style.display = 'none';
        document.getElementById("minusUserButtonRest").style.display = 'none';
    }
    c = c + 1;
    document.getElementById("paramcountRMRest").value = c;
    document.getElementById("restUserDefinedHeader").value = c;
}
function  removeRestHeader() {
    if (l >= 1) {
        document.getElementById("_headerRestTable").deleteRow(l);
        l--;
        c = c - 1;
        document.getElementById("restUserDefinedHeader").value = c;
        var m = c - 1;
        if (m < 0) {
            document.getElementById("addUserButtonRest").style.display = 'block';
            document.getElementById("minusUserButtonRest").style.display = 'block';
        } else {
            document.getElementById("addUserButtonRest" + m).style.display = 'block';
            document.getElementById("minusUserButtonRest" + m).style.display = 'block';
        }
    }
}


// form data header parameter
formDataCell = 0;
formDataParam = 0;
function  addFormData() {
    formDataCell++;
    var table = document.getElementById("_formDataTable");
    var row = table.insertRow(formDataCell);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = "<input type='text' class='form-control' id='_formDataKey" + formDataParam + "'name='_formDataKey" + formDataParam + "'placeholder='Key'>";

    cell2.innerHTML = "<div style='padding: 1%'><input type='text' class='form-control' id='_formDataValue" + formDataParam + "' name='_formDataValue" + formDataParam + "' placeholder='Value'></div>";

    cell3.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addFormData()' id='addFormDataButton" + formDataParam + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeFormData()' id='minusFormDataButton" + formDataParam + "' ><span class='fa fa-minus-circle'></span></a>";

    if (formDataParam >= 1) {
        var j = formDataParam - 1;
        document.getElementById("addFormDataButton" + j).style.display = 'none';
        document.getElementById("minusFormDataButton" + j).style.display = 'none';
    } else {
        document.getElementById("addFormDataButton").style.display = 'none';
        document.getElementById("minusFormDataButton").style.display = 'none';
    }
    formDataParam = formDataParam + 1;
    document.getElementById("paramcountFormData").value = formDataParam;
}
function  removeFormData() {
    if (formDataCell >= 1) {
        document.getElementById("_formDataTable").deleteRow(formDataCell);
        formDataCell--;
        formDataParam = formDataParam - 1;
        var m = formDataParam - 1;
        if (m < 0) {
            document.getElementById("addFormDataButton").style.display = 'block';
            document.getElementById("minusFormDataButton").style.display = 'block';
        } else {
            document.getElementById("addFormDataButton" + m).style.display = 'block';
            document.getElementById("minusFormDataButton" + m).style.display = 'block';
        }
    }
}

// url encoded form data header parameter
urlEncodedformDataCell = 0;
urlEncodedformDataParam = 0;
function  addURLEncodedFormData() {
    urlEncodedformDataCell++;
    var table = document.getElementById("_urlencodedformDataTable");
    var row = table.insertRow(urlEncodedformDataCell);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = "<input type='text' class='form-control' id='_urlEncodedFormDataKey" + urlEncodedformDataParam + "'name='_urlEncodedFormDataKey" + urlEncodedformDataParam + "'placeholder='Key'>";

    cell2.innerHTML = "<div style='padding: 1%'><input type='text' class='form-control' id='_urlEncodedFormDataValue" + urlEncodedformDataParam + "' name='_urlEncodedFormDataValue" + urlEncodedformDataParam + "' placeholder='Value'></div>";

    cell3.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addURLEncodedFormData()' id='addUrlencodedFormDataButton" + urlEncodedformDataParam + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeURLEncodedFormData()' id='minusUrlencodedFormDataButton" + urlEncodedformDataParam + "' ><span class='fa fa-minus-circle'></span></a>";

    if (urlEncodedformDataParam >= 1) {
        var j = urlEncodedformDataParam - 1;
        document.getElementById("addUrlencodedFormDataButton" + j).style.display = 'none';
        document.getElementById("minusUrlencodedFormDataButton" + j).style.display = 'none';
    } else {
        document.getElementById("addUrlencodedFormDataButton").style.display = 'none';
        document.getElementById("minusUrlencodedFormDataButton").style.display = 'none';
    }
    urlEncodedformDataParam = urlEncodedformDataParam + 1;
    document.getElementById("paramcountUrlEncodedFormData").value = urlEncodedformDataParam;
}
function  removeURLEncodedFormData() {
    if (urlEncodedformDataCell >= 1) {
        document.getElementById("_urlencodedformDataTable").deleteRow(urlEncodedformDataCell);
        urlEncodedformDataCell--;
        urlEncodedformDataParam = urlEncodedformDataParam - 1;
        var m = urlEncodedformDataParam - 1;
        if (m < 0) {
            document.getElementById("addUrlencodedFormDataButton").style.display = 'block';
            document.getElementById("minusUrlencodedFormDataButton").style.display = 'block';
        } else {
            document.getElementById("addUrlencodedFormDataButton" + m).style.display = 'block';
            document.getElementById("minusUrlencodedFormDataButton" + m).style.display = 'block';
        }
    }
}

// query parameter
queryParameterCell = 0;
queryDataParam = 0;
function  addQueryParamData() {
    queryParameterCell++;
    var table = document.getElementById("_queryParamTable");
    var row = table.insertRow(queryParameterCell);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = "<input type='text' class='form-control' id='_queryParameterKey" + queryDataParam + "'name='_queryParameterKey" + queryDataParam + "'placeholder='Key'>";

    cell2.innerHTML = "<div style='padding: 1%'><input type='text' class='form-control' id='_queryParameterValue" + queryDataParam + "' name='_queryParameterValue" + queryDataParam + "' placeholder='Value'></div>";

    cell3.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addQueryParamData()' id='addQueryParamButton" + queryDataParam + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeQueryParamData()' id='minusQueryParamButton" + queryDataParam + "' ><span class='fa fa-minus-circle'></span></a>";

    if (queryDataParam >= 1) {
        var j = queryDataParam - 1;
        document.getElementById("addQueryParamButton" + j).style.display = 'none';
        document.getElementById("minusQueryParamButton" + j).style.display = 'none';
    } else {
        document.getElementById("addQueryParamButton").style.display = 'none';
        document.getElementById("minusQueryParamButton").style.display = 'none';
    }
    queryDataParam = queryDataParam + 1;
    document.getElementById("paramcountUrlEncodedFormData").value = queryDataParam;
}
function  removeQueryParamData() {
    if (queryParameterCell >= 1) {
        document.getElementById("_queryParamTable").deleteRow(queryParameterCell);
        queryParameterCell--;
        queryDataParam = queryDataParam - 1;
        var m = queryDataParam - 1;
        if (m < 0) {
            document.getElementById("addQueryParamButton").style.display = 'block';
            document.getElementById("minusQueryParamButton").style.display = 'block';
        } else {
            document.getElementById("addQueryParamButton" + m).style.display = 'block';
            document.getElementById("minusQueryParamButton" + m).style.display = 'block';
        }
    }
}

function LoadBody() {
    var s = './GetXmlContent';
    var splashData = '<br><br><br><br><br><br><br><br><br><br><br><div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div>' +
            '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';

    $('#_APIbody').val("");
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getBody").serialize(),
        success: function (data) {
            $('#_APIbody').val(data.apibody);
            var doc = document.getElementById('_APIbody');
            var dataCode = CodeMirror.fromTextArea(doc, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                tabMode: "indent",
            });
            dataCode.on('change', function ash() {
                $('#_APIbody').val(dataCode.getValue());
            });
//            LoadRestBody();
        }
    });
}

var loadedRest = false;

function LoadRestBody() {
    var s = './GetXmlContent';
    if (loadedRest == false) {
        loadedRest = true;
    } else {
        return;
    }
    var splashData = '<br><br><br><br><br><br><br><br><br><br><br><div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div>' +
            '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';

    $('#_RESTAPIbody').val("");
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getRestBody").serialize(),
        success: function (data) {
            $('#_RESTAPIbody').val(data.apibody);
            var doc = document.getElementById('_RESTAPIbody');
            var dataCode = CodeMirror.fromTextArea(doc, {
                lineNumbers: true,
                mode: "application/ld+json",
                matchBrackets: true,
                styleActiveLine: true
            });
            dataCode.on('change', function ash() {
                $('#_RESTAPIbody').val(dataCode.getValue());
            });
        }
    });
}

$(document).ready(function () {
    $("input[name$='bodyRadio']").click(function () {
        var test = $(this).val();
        $("div.bodydesc").hide();
        $("#body" + test).show();
    });
});

var editor = null;
//
//function SendRequest() {
//    var s = './TestSoap';
//    var l = $('#apiConsoleRequest').ladda();
//    var timer = new Timer();
//    timer.start({precision: 'secondTenths', callback: function (values) {
//        $('#startValuesAndTargetExample .values').html(values.toString(['minutes', 'seconds', 'secondTenths']));
//    }});
////    timer.start({precision: 'secondTenths'});
////    $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString(['minutes', 'seconds','secondTenths']));
//    timer.addEventListener('secondsUpdated', function (e) {
//        $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString(['minutes', 'seconds','secondTenths']));
//        //$('#startValuesAndTargetExample .progress_bar').html($('#startValuesAndTargetExample .progress_bar').html() + '.');
//    });
//    
//    if (editor != null) {
//        editor.toTextArea();
//    }
//    document.getElementById("responseID").value = "";
//    l.ladda('start');
//    $.ajax({
//        type: 'POST',
//        url: s,
//        datatype: 'json',
//        data: $("#getBody").serialize(),
//        success: function (data) {
//            $('#responseID').val("");
//            if (strCompare(data.result, "error") === 0) {
//                setTimeout(function () {
//                l.ladda('stop');
//                mirror(data.responseData, document.getElementById("responseID"));                    
//                $('#startValuesAndTargetExample .values').html('<p class="text-info">Your API takes '+timer.getTimeValues().toString(['minutes', 'seconds'])+' sec to get response. </p>');
//                timer.stop();
//                document.getElementById("questionAndAnswer").style.display='block';
//            }, 3000);
//            
//            }else{
//            setTimeout(function () {
//                l.ladda('stop');
//                mirror(data.responseData, document.getElementById("responseID"));                    
//                $('#startValuesAndTargetExample .values').html('<p class="text-info">Your API takes '+timer.getTimeValues().toString(['minutes', 'seconds'])+' sec to get response. </p>');
//                timer.stop();
//            }, 3000);
//        }
//        }
//    });
//}

function SendRequest() {
    var s = './TestSoap';
    document.getElementById("questionAndAnswer").style.display = 'none';
    var l = $('#apiConsoleRequest').ladda();
    var timer = new Timer();
    timer.start({precision: 'secondTenths', callback: function (values) {
            $('#startValuesAndTargetExample').html(values.toString(['minutes', 'seconds', 'secondTenths']));
        }});
//    timer.start({precision: 'secondTenths'});
//    $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString(['minutes', 'seconds','secondTenths']));
    timer.addEventListener('secondsUpdated', function (e) {
        $('#startValuesAndTargetExample').html(timer.getTimeValues().toString(['minutes', 'seconds', 'secondTenths']));
        //$('#startValuesAndTargetExample .progress_bar').html($('#startValuesAndTargetExample .progress_bar').html() + '.');
    });
    var str;
    if (editor != null) {
        editor.toTextArea();
    }
    //$('#startValuesAndTargetExampleREST').style.display='block';

    //  $('#startValuesAndTargetExample').style.display='block';
    document.getElementById("responseID").value = "";
    l.ladda('start');
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getBody").serialize(),
        success: function (data) {
            $("#responseID").focus();
            $("#responseID").trigger("click");
            $('#responseID').val("");

            

            if (strCompare(data.result, "error") === 0) {
                document.getElementById("questionAndAnswer").style.display = 'block';
                setTimeout(function () {
                    l.ladda('stop');
                }, 3000);
                str = (JSON.stringify(data.responseData));
                document.getElementById("responseID");
                document.getElementById("responseIDCopy").value=data.responseData;
                mirror(data.responseData, document.getElementById("responseID"));
                var responseTakesTime = timer.getTimeValues().toString(['minutes', 'seconds']);
                timer.stop();
                console.log("responseTakesTime " + responseTakesTime);

                $('#startValuesAndTargetExample').html('<span class="text-danger col-lg-12"><i class="fa fa-exclamation-circle"></i> OOPS, Something Went Wrong. </span>');

                document.getElementById("questionAndAnswer").style.display = 'block';
                if (str.includes("Connection refused")) {
                    document.getElementById("connectionrefuse").style.display = 'block';
                } else if (str.includes("API Level Token")) {
                    document.getElementById("_apiLevelToken").style.display = 'block';
                } else if (str.includes("Headers")) {
                    document.getElementById("_headerError").style.display = 'block';
                } else if (str.includes("No Such Partner is Available.")) {
                    document.getElementById("_noPartnerAvailable").style.display = 'block';
                } else if (str.includes("Internal Resource")) {
                    document.getElementById("_resourseUnAvai").style.display = 'block';
                } else if (str.includes("Account Is Not Active")) {
                    document.getElementById("_accDiasable").style.display = 'block';
                } else if (str.includes("Partner Is Not Active")) {
                    document.getElementById("_partnerDisable").style.display = 'block';
                } else if (str.includes("Please Initialise API Level Token")) {
                    document.getElementById("initializeApiToken").style.display = 'block';
                } else if (str.includes("Associated Group Is Not Active")) {
                    document.getElementById("_groupDisable").style.display = 'block';
                } else if (str.includes("You Are Not Autherized Partner")) {
                    document.getElementById("_unAuthorizedPart").style.display = 'block';
                } else if (str.includes("You Are Not Autherized Partner")) {
                    document.getElementById("_reachedLimit").style.display = 'block';
                } else if (str.includes("Transaction Per Second Error")) {
                    document.getElementById("_transactionsec").style.display = 'block';
                } else if (str.includes("invalid IP")) {
                    document.getElementById("_accessinvalidIp").style.display = 'block';
                } else if (str.includes("cannot access service")) {
                    document.getElementById("_serNotaccesible").style.display = 'block';
                } else if (str.includes("service now as it is not permitted")) {
                    document.getElementById("serNOtPermited").style.display = 'block';
                } else if (str.includes("invalid/empty certificate")) {
                    document.getElementById("invalidCert").style.display = 'block';
                } else if (str.includes("IP BLOCK")) {
                    document.getElementById("_ipblock").style.display = 'block';
                } else if (str.includes("invalid/empty Token")) {
                    document.getElementById("_invalidToken").style.display = 'block';
                } else if (str.includes("Operation timed out (Connection timed out)")) {
                    document.getElementById("_operationTimeOut").style.display = 'block';
                }

            } else {
                mirror(data.responseData, document.getElementById("responseID"));
                var responseTakesTime = timer.getTimeValues().toString(['minutes', 'seconds']);
                timer.stop();
                document.getElementById("responseIDCopy").value=data.responseData;
                console.log(responseTakesTime)
                if (responseTakesTime <= "00:30") {
                    $('#startValuesAndTargetExample').html('<span class="text-info col-lg-12"><i class="fa fa-check-square"></i> Congratulations, Your API takes ' + responseTakesTime + ' second to get response. </span>');
                } else {
                    $('#startValuesAndTargetExample').html('<span class="text-danger col-lg-12"><i class="fa fa-exclamation-circle"></i> OOPS, Your API takes ' + responseTakesTime + ' second to get response. </span>');
                }
                setTimeout(function () {
                    l.ladda('stop');
                    getCreditStatus();
                }, 3000);
            }
            var soapResponseData = "SOAPResponse";
            var copySOAPResponse = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + soapResponseData + "','copySOAPResponse')><b>Copy</b></a><br/><br/>";
            console.log("copySOAPResponse >>> " + copySOAPResponse);
            document.getElementById('soapResponseCopyDiv').innerHTML = copySOAPResponse;
            
            $('#responseID').focus();
            $('#responseID').select();
            $('#responseID').trigger('focus');
            $('#responseID').trigger('click');
            
        }
    });
}

function getCreditStatus() {
    var s = './GetCredit';
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getBody").serialize(),
        success: function (data) {
            if (data.credits !== 'NA') {
                $('#mainCreditOfDev').html(data.credits);
            }
        }});
}

function mirror(msg, doc) {

    if (editor != null) {
        editor.toTextArea();
    }
    editor = CodeMirror.fromTextArea(doc, {
//        mode: "javascript",
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true
    });
    editor.getDoc().setValue(msg);
}
function redirectToToken(apName, resName, version) {
    Alert4T("<font color=red>Please Initialize Token.</font>")
    setTimeout(function () {
        window.location = "./getToken.jsp?apName=" + apName + "&resName=" + resName + "&version=" + version;
    }, 2000)
}

function bootboxmodelAl(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body" id=tokenDetail>' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            '<div >' +
            content +
            '</div>' +
            '<a class="btn btn-success btn-xs" data-dismiss="modal" class="close"><i class="fa fa-thumbs-o-up"></i> OK</a>' +
            '</div></div></div></div>');
    popup.modal();
}

function Alert4T(msg) {
    bootboxmodelAl("<h4>" + msg + "</h4>");
}

function submitRequest() {
    var s = './SubmitProductionRequest';

    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getBody,#apiRespone").serialize(),
        success: function (data) {
            data = (JSON.parse(data));
            if (strCompare4API(data.result, "success") !== 0) {
                Alert4T("<h4><font color=red>" + data.message + "</font></h4>");
            } else {
                Alert4T("<h4><font color=blue>" + data.message + "</font></h4>");
            }
        }
    });
}

function strCompare4API(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

var waiting;

function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}

function getRestWindow(rest) {
    var apiMethodName = document.getElementById("methodName").value;
    if (strCompare4API(rest, "rest") == 0) {
        //LoadRestBody();
        $('#consoleType a[href="#restful"]').tab('show');
    } else if (strCompare4API(rest, "soap") == 0) {
        $('#consoleType a[href="#soap"]').tab('show');
    }
    getAPIConsoleWindow(apiMethodName);
}

function onlyRequestType() {
    var requestType = document.getElementById("apiConsoleSelectRequest").value;
    if (strCompare4API(requestType, "rest") == 0) {
        LoadRestBody();
        $('#consoleType a[href="#restful"]').tab('show');
    } else if (strCompare4API(requestType, "soap") == 0) {
        $('#consoleType a[href="#soap"]').tab('show');
    }
}

function getAPIConsoleWindow(apiName) {
    var splashData = '<br><br><br><br><br><br><br><br><br><br><br><div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div>' +
            '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
    $('#apiConsoleWindow').html(splashData);
    var apiResource = document.getElementById("apiConsoleSelectService").value;
    var s = './api_console_window.jsp?methodName=' + apiName + '&apiResource=' + apiResource;
//    $('#startValuesAndTargetExampleREST').html(" ");
    $('#startValuesAndTargetExample').html(" ");

    $.ajax({
        type: 'GET',
        url: s,
        data: $('#apiConsoleData').serialize(),
        success: function (data) {
            document.getElementById("apiConsoleWindow").classList.remove("content");
            document.getElementById("apiConsoleWindow").classList.remove("animate-panel");
            $('#apiConsoleWindow').html(data);


        }
    });
}

function getRequestType() {
    var s = './api_console_request.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        data: $('#apiConsoleData').serialize(),
        success: function (data) {
            $('#apiRequestAsPerService').html(data);
            var requestType = document.getElementById("apiConsoleSelectRequest").value;
            getRestWindow(requestType);
        }
    });
}
function getAPIList(apiName, passedAPI) {
    var apiResource = document.getElementById("apiConsoleSelectService").value;

    var s = './api_console_apiList.jsp?methodName=' + apiResource + '&passedAPI=' + passedAPI;
    $.ajax({
        type: 'GET',
        url: s,
        //data: $('#apiConsoleData').serialize(),
        success: function (data) {
            $('#apiListAsPerService').html(data);
            //$('#apiNameChanged').text(apiName);  
            getRequestType();
        }
    });
}

function submitRestRequest() {
    document.getElementById("questionAndAnswerREST").style.display = 'none';
    var s = './TestRest';
    var l = $('#apiConsoleRestRequest').ladda();
//    $('#startValuesAndTargetExampleREST').style.display='block';
//    

    var str;
    if (editor != null) {
        editor.toTextArea();
    }
    var timerRest = new Timer();
    timerRest.start({precision: 'secondTenths', callback: function (values) {
            $('#startValuesAndTargetExampleREST .values').html(values.toString(['minutes', 'seconds', 'secondTenths']));
        }});
//    timer.start({precision: 'secondTenths'});
//    $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString(['minutes', 'seconds','secondTenths']));
    timerRest.addEventListener('secondsUpdated', function (e) {
        $('#startValuesAndTargetExampleREST .values').html(timerRest.getTimeValues().toString(['minutes', 'seconds', 'secondTenths']));
        //$('#startValuesAndTargetExample .progress_bar').html($('#startValuesAndTargetExample .progress_bar').html() + '.');
    });
    document.getElementById("restResponseID").value = "";
    l.ladda('start');
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#getRestBody").serialize(),
        success: function (data) {
            $('#restResponseID').val(data.responseData);
            document.getElementById("restresponseIDCopy").value=data.responseData;
            $("#restResponseID").focus();
            $("#restResponseID").trigger("click");
            $('#restResponseID').focus();

            $('#restResponseID').select();

            $('#restResponseID').trigger('focus');

            $('#restResponseID').trigger('click');

            if (strCompare(data.result, "error") === 0) {
                str = (JSON.stringify(data.responseData));
                setTimeout(function () {
                    l.ladda('stop');
                }, 3000);
                mirror(data.responseData, document.getElementById("restResponseID"));
                var responseTakesTime = timerRest.getTimeValues().toString(['minutes', 'seconds']);
                timerRest.stop();
                console.log("responseTakesTime " + responseTakesTime);

                $('#startValuesAndTargetExampleREST').html('<span class="text-danger col-lg-12"><i class="fa fa-exclamation-circle"></i> OOPS, Something Went Wrong. </span>');

                document.getElementById("questionAndAnswerREST").style.display = 'block';
                if (str.includes("Connection refused")) {
                    document.getElementById("connectionrefuseRest").style.display = 'block';
                } else if (str.includes("API Level Token")) {
                    document.getElementById("_apiLevelTokenRest").style.display = 'block';
                } else if (str.includes("Headers")) {
                    document.getElementById("_headerErrorRest").style.display = 'block';
                } else if (str.includes("No Such Partner is Available.")) {
                    document.getElementById("_noPartnerAvailableRest").style.display = 'block';
                } else if (str.includes("Internal Resource")) {
                    document.getElementById("_resourseUnAvaiRest").style.display = 'block';
                } else if (str.includes("Account Is Not Active")) {
                    document.getElementById("_accDiasableRest").style.display = 'block';
                } else if (str.includes("Partner Is Not Active")) {
                    document.getElementById("_partnerDisableRest").style.display = 'block';
                } else if (str.includes("Please Initialise API Level Token")) {
                    document.getElementById("initializeApiTokenRest").style.display = 'block';
                } else if (str.includes("Associated Group Is Not Active")) {
                    document.getElementById("_groupDisableRest").style.display = 'block';
                } else if (str.includes("You Are Not Autherized Partner")) {
                    document.getElementById("_unAuthorizedPartRest").style.display = 'block';
                } else if (str.includes("You Are Not Autherized Partner")) {
                    document.getElementById("_reachedLimitRest").style.display = 'block';
                } else if (str.includes("Transaction Per Second Error")) {
                    document.getElementById("_transactionsecRest").style.display = 'block';
                } else if (str.includes("invalid IP")) {
                    document.getElementById("_accessinvalidIpRest").style.display = 'block';
                } else if (str.includes("cannot access service")) {
                    document.getElementById("_serNotaccesibleRest").style.display = 'block';
                } else if (str.includes("service now as it is not permitted")) {
                    document.getElementById("serNOtPermitedRest").style.display = 'block';
                } else if (str.includes("invalid/empty certificate")) {
                    document.getElementById("invalidCertRest").style.display = 'block';
                } else if (str.includes("IP BLOCK")) {
                    document.getElementById("_ipblockRest").style.display = 'block';
                } else if (str.includes("invalid/empty Token")) {
                    document.getElementById("_invalidTokenRest").style.display = 'block';
                } else if (str.includes("Signature Verification Failed")) {
                    document.getElementById("_signFailedRest").style.display = 'block';
                } else if (str.includes("Operation timed out (Connection timed out)")) {
                    document.getElementById("_operationTimeOutRest").style.display = 'block';
                }

            } else {
                mirror(data.responseData, document.getElementById("restResponseID"));
                var responseTakesTime = timerRest.getTimeValues().toString(['minutes', 'seconds']);
                timerRest.stop();
                if (responseTakesTime <= "00:30") {
                    $('#startValuesAndTargetExampleREST .values').html('<span class="text-info col-lg-12"><i class="fa fa-check-square"></i> Congratulations, Your API takes ' + responseTakesTime + ' second to get response. </span>');
                } else {
                    $('#startValuesAndTargetExampleREST .values').html('<span class="text-danger col-lg-12"><i class="fa fa-exclamation-circle"></i> OOPS, Your API takes ' + responseTakesTime + ' second to get response. </span>');
                }
                setTimeout(function () {
                    l.ladda('stop');
                    getCreditStatus();
                }, 3000);

            }
            var restResponseData = "RESTResponse";
            var copyRESTResponse = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + restResponseData + "','copyRESTResponse')><b>Copy</b></a><br/><br/>";
            console.log("copyRESTResponse >>> " + copyRESTResponse);
            document.getElementById('restResponseCopyDiv').innerHTML = copyRESTResponse;
        }
    });
}

function copy(text, target) {
    document.getElementById(target).value = (text);
    document.getElementById(target).style.display = 'block';
    copyToClipboardMsg(document.getElementById(target), "msg");
    document.getElementById(target).style.display = 'none';
}

function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
    } else {
        msg = "Text copied to the clipboard."
    }
    console.log(msg);

}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

function apiHelpDesk() {
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = "compose_email_technical.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);

        }, complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}