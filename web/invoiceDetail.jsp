<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LatePenalitiesCalculationManagement"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.GenerateInvoiceId"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="invoice_css/style.css" rel="stylesheet" type="text/css"/>
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/promocode.js" type="text/javascript"></script>
        <title>Developer Portal</title>
        <link href="invoice_css/invoiceDetail.css" rel="stylesheet" type="text/css"/>
        <script src="js/promocode.js" type="text/javascript"></script>
        <script src="invoice_css/invoiceDetail.js" type="text/javascript"></script>
        <script src="js/paymentOperation.js" type="text/javascript"></script>

        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <!--        <script src="js/bootstrap.min.js"></script> -->
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <!--        <script src="js/bootbox.min.js" type="text/javascript"></script>-->
        <script src="./js/modal.js"></script>
        <script src="jshash/sha1-min.js" type="text/javascript"></script>
        <script src="jshash/sha1.js" type="text/javascript"></script>
    </head>
    <body onload="havePromoCode()">
        <%
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (SessionId == null) {
                response.sendRedirect("logout.jsp");
                return;
            }
            PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            String packageName = request.getParameter("_packageName");
            String paymentMode = request.getParameter("_paymentMode");
            String outPackageName = packageName;
            if(packageName != null && !packageName.isEmpty()){
                 if(packageName.length() >= 16){
                    outPackageName = packageName.substring(0, 14);
                }  
            }
            SgPartnerrequest reqPar = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, parObj.getPartnerId());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            String invoiceDate = dateFormat.format(new Date());
            NumberFormat form = new DecimalFormat("#0.00");
            String invoiceid = GenerateInvoiceId.getDate() + parObj.getPartnerId() + GenerateInvoiceId.getRandom();

            SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, ChannelId, packageName);
            //String paymentType = packageObject.getPaymentMode();
            SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, ChannelId, parObj.getPartnerId());
            Double totalPaid = new Double(0);
            Double changeOnPackageCharge = null;
            Double reActivationCharge = null;
            Double cancellationCharge = null;
            Double latePenaltyCharge = null;
            SgPaymentdetails payObj = null;
            Map<String, Double> amount = new LinkedHashMap<String, Double>();
            Map<String, String> taxMap = new LinkedHashMap<String, String>();
            // variable for gst tax cal
            Double totalAmountWithoutTax = 0.00;
            Double gstTax = 0.00;
            Double vatTax = 0.00;
            Double serviceTax = 0.00;
            
            if (subscriObj != null) {
                payObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObj.getBucketId(), parObj.getPartnerId());
                if (payObj != null) {
                    Date paymentDate = payObj.getPaidOn();
                    invoiceDate = dateFormat.format(paymentDate);
                }
                double planAmount = subscriObj.getPlanAmount();
                amount.put("Package Amount", planAmount);
                totalPaid += planAmount;
                packageName = subscriObj.getBucketName();
                paymentMode = subscriObj.getPaymentMode();
                double serviceCharge = Double.parseDouble(subscriObj.getServiceCharge());
                amount.put("Service Charge", serviceCharge);
                totalPaid += serviceCharge;
                if (packageObject != null) {
                    if (!packageObject.getBucketName().equals(subscriObj.getBucketName())) {
                        double changePackageCharge = subscriObj.getChangePackageRate();
                        amount.put("Change Package Charge", changePackageCharge);
                        totalPaid += changePackageCharge;
                        changeOnPackageCharge = changePackageCharge;
                    }
                    if (packageObject.getBucketName().equals(subscriObj.getBucketName())) {
                        double reactivationCharge = Float.parseFloat(String.valueOf(subscriObj.getReActivationCharge()));
                        amount.put("Reactivation Charge", reactivationCharge);
                        totalPaid += reactivationCharge;
                        reActivationCharge = reactivationCharge;
                    }
                }
                String expiryDate = dateFormat.format(subscriObj.getExpiryDateNTime());
                String strDate = dateFormat.format(new Date());
                Date curDate = dateFormat.parse(strDate);
                Date exDate = dateFormat.parse(expiryDate);
                if (curDate.before(exDate)) {
                    double packageCancellationCharge = subscriObj.getCancellationRate();
                    amount.put("Cancellation Charge", packageCancellationCharge);
                    totalPaid += packageCancellationCharge;
                    cancellationCharge = packageCancellationCharge;
                }
                if (subscriObj.getPaymentMode().equalsIgnoreCase("postpaid")) {
                    if (curDate.after(exDate)) {
                        String latePenalites = subscriObj.getLatePenaltyRate();
                        if (latePenalites != null) {
                            double latePenalty = LatePenalitiesCalculationManagement.calculateLatePenalty(subscriObj);
                            amount.put("Late Penalty", latePenalty);
                            totalPaid += latePenalty;
                            latePenaltyCharge = latePenalty;
                        }
                    }
                }
                totalAmountWithoutTax = totalPaid;
                
                String taxRate = subscriObj.getTax();

                taxMap = TaxCalculationManagement.calculateTax(taxRate, totalPaid);

                String calculatedGSTDetail = (String) taxMap.get("GST Tax");
                String calculatedVATDetail = (String) taxMap.get("VAT Tax");
                String calculatedSTDetail = (String) taxMap.get("Service Tax");

                String[] calculatedGST = calculatedGSTDetail.split(":");
                String[] calculatedVAT = calculatedVATDetail.split(":");
                String[] calculatedST = calculatedSTDetail.split(":");

                totalPaid += Double.parseDouble(calculatedGST[1]);
//                totalPaid += Double.parseDouble(calculatedVAT[1]);
//                totalPaid += Double.parseDouble(calculatedST[1]);
                
                gstTax = Double.parseDouble(calculatedGST[0]);
                vatTax = Double.parseDouble(calculatedVAT[0]);
                serviceTax = Double.parseDouble(calculatedST[0]); 

            }
            
            double roundedTMREQAMOUNT = (double) Math.round(totalPaid * 100) / 100;
            
            String tmWellFormAmount       = form.format(roundedTMREQAMOUNT);
            String wellFormTotalBeforeTax = form.format(totalAmountWithoutTax);
            String tmMerchantID           = LoadSettings.g_sSettings.getProperty("tm.pg.merchantId");
            String tmMerchantPWD          = LoadSettings.g_sSettings.getProperty("tm.pg.merchantPWD");
            
            long tmMerchant_TranID        = (long) (Math.random() * 100000000000000L);
            tmMerchant_TranID            += 500000000000000L;            

//            if (subscriObj == null && packageObject != null) {
//                SgSubscriptionDetails subscriObjPrepaid = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
//                packageName = packageObject.getBucketName();
//                brandname = packageObject.getPaymentMode();
//                SgLoanDetails loanObj = null;
//                double planAmount = packageObject.getPlanAmount();
//                amount.put("Package Amount", planAmount);
//                totalPaid += planAmount;
//                double serviceCharge = Float.parseFloat(packageObject.getServiceCharge());
//                amount.put("Service Charge", serviceCharge);
//                totalPaid += serviceCharge;
//
//                // get info about loan
//                if (subscriObjPrepaid != null) {
//                    loanObj = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObjPrepaid.getBucketId());
//                    if (loanObj != null) {
//                        if (loanObj.getStatus() == GlobalStatus.UNPAID) {
//                            String approveDate = dateFormat.format(loanObj.getLoanApproveOn());
//                            String loanDetails = "Loan " + loanObj.getLoanAmount() + " on package " + subscriObjPrepaid.getBucketName() + " approved on " + approveDate + " with interest " + loanObj.getInterestRate();
//                            float intererstOnLoan = (loanObj.getLoanAmount() * loanObj.getInterestRate()) / 100;
//                            float totalLoan = loanObj.getLoanAmount() + intererstOnLoan;
//                            loanDetails = loanDetails + ":" + totalLoan;
//                            taxMap.put("Loan ", loanDetails);
//                        }
//                    }
//                }
//                String taxRate = packageObject.getTax();
//                taxMap = TaxCalculationManagement.calculateTax(taxRate, totalPaid);
//
//                String calculatedGSTDetail = (String) taxMap.get("GST Tax");
//                String calculatedVATDetail = (String) taxMap.get("VAT Tax");
//                String calculatedSTDetail = (String) taxMap.get("Service Tax");
//
//                String[] calculatedGST = calculatedGSTDetail.split(":");
//                String[] calculatedVAT = calculatedVATDetail.split(":");
//                String[] calculatedST = calculatedSTDetail.split(":");
//
//                totalPaid += Double.parseDouble(calculatedGST[1]);
//                totalPaid += Double.parseDouble(calculatedVAT[1]);
//                totalPaid += Double.parseDouble(calculatedST[1]);
//            }
        %>

        <div id="havePromocode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 id="validatesPackageModal">Have a promocode ?</h4>
                    </div>          
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="validatePromoForm">                        
                                <div class="control-group">
                                    <label class="control-label col-lg-2"  for="partnername">Promo code</label>
                                    <div class="controls col-lg-10">
                                        <input type="hidden"  id="_promoId" name="_promoId" >
                                        <input type="hidden"  id="_promoDiscount" name="_promoDiscount" value=0 >
                                        <input type="hidden"  id="_promostatus" name="_promostatus" >
                                        <input type="hidden" id="_promoPackage" name="_promoPackage" value="<%=packageName%>">
                                        <input type="text"  id="_promoCode" name="_promoCode" autofocus>                                    
                                    </div>
                                </div>                                                    
                            </form>
                        </div>
                    </div>                                  
                    <div class="modal-footer">
                        <div id="validate-promo-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal" >Close</button>
                        <button class="btn btn-success btn-xs" onclick="validatePromoCode('<%=paymentMode%>', '<%=packageName%>')" id="addPartnerButtonE">Validate</button>
                    </div>
                    <script>
                        var input = document.getElementById('_promoCode');
                        input.onkeyup = function () {
                            this.value = this.value.toUpperCase();
                        }
                    </script>                                    
                </div>
            </div>
        </div>



        <form class="form-horizontal" id="paymentDetailsForm"> 
            <input type="hidden" id="totalBenefitAmount" name="totalBenefitAmount" value="<%=totalPaid%>">
            <input type="hidden" id="_promocodeId" name="_promocodeId">
            <input type="hidden" id="_packageName" name="_packageName" value="<%=outPackageName%>">
            <input type="hidden" id="originalPackageN" name="originalPackageN" value="<%=packageName%>">
            <input type="hidden" id="_invoiceId" name="_invoiceId" value="<%=invoiceid%>">
            <input type="hidden" id="_paymentMode" name="_paymentMode" value="<%=paymentMode%>">
            <input type="hidden" id="_totalAmountWithoutTax" name="_totalAmountWithoutTax" value="<%=wellFormTotalBeforeTax%>">
            <input type="hidden" id="_gstTax" name="_gstTax" value="<%=gstTax%>">
            <input type="hidden" id="_vatTax" name="_vatTax" value="<%=vatTax%>">
            <input type="hidden" id="_serviceTax" name="_serviceTax" value="<%=serviceTax%>">
            
            <input type="hidden" id="MERCHANTID" name="MERCHANTID" value="<%=tmMerchantID%>">
            <input type="hidden" id="PSWD" name="PSWD" value="<%=tmMerchantPWD%>">
            <input type="hidden" id="MERCHANT_TRANID" name="MERCHANT_TRANID" value="<%=tmMerchant_TranID%>">
            <input type="hidden" id="AMOUNT" name="AMOUNT" value="<%=tmWellFormAmount%>">
            <input type="hidden" id="SIGNATURE" name="SIGNATURE" >
            
            <%if (changeOnPackageCharge != null) {%>
            <input type="hidden" id="_changeOnPackageCharge" name="_changeOnPackageCharge" value="<%=changeOnPackageCharge%>">
            <%}
                if (reActivationCharge != null) {%>       
            <input type="hidden" id="_reactivationCharge" name="_reactivationCharge" value="<%=reActivationCharge%>">
            <%}
                if (cancellationCharge != null) {%>        
            <input type="hidden" id="_cancellationCharge" name="_cancellationCharge" value="<%=cancellationCharge%>">
            <%}
                if (latePenaltyCharge != null) {%>
            <input type="hidden" id="_latePenaltyCharge" name="_latePenaltyCharge" value="<%=latePenaltyCharge%>">
            <%}%>
        </form>
        <script language="JavaScript">
            function generateHash(paymentDetailsForm) {
            var str = "##" + paymentDetailsForm.MERCHANTID.value.toUpperCase() + "##"+paymentDetailsForm.PSWD.value.toUpperCase()+"##" +
            paymentDetailsForm.MERCHANT_TRANID.value.toUpperCase() + "##" + paymentDetailsForm.AMOUNT.value.toUpperCase() + "##0##";                      
            paymentDetailsForm.SIGNATURE.value = hex_sha1(str).toUpperCase();                       
            }
            generateHash(paymentDetailsForm);
        </script> 
        
        <div id="menu-container">            
            <a href="#" class="btn btn-primary" onclick="print_specific_div_content()"><i class="fa fa-print"></i> Print</a>
<!--            <a href="#" class="btn btn-success" onclick="payWithXPAY()"> Pay</a> -->
            <a href="#" class="btn btn-info" onclick="payWithTM()"> Pay</a>
            <a onclick="havePromoCode()" style="margin-left: 2%"> Have a Promo code ?</a>
        </div>
        <div id="invoice-container">
            <div id="header">
                <table>
                    <tbody><tr>
                            <td id="company-name">
                                <%
                                    String imageURL = LoadSettings.g_strPath + "LOGO.png";
                                    String path = URLDecoder.decode(imageURL, "UTF-8");
                                    String companyName = LoadSettings.g_sSettings.getProperty("company.name");
                                    String addressline1 = LoadSettings.g_sSettings.getProperty("address.line1");
                                    String addressline2 = LoadSettings.g_sSettings.getProperty("address.line2");
                                    String addressline3 = LoadSettings.g_sSettings.getProperty("address.line3");
                                    String companyPhone = LoadSettings.g_sSettings.getProperty("company.phone");
                                %>
                                <img src="img/imgpsh_fullsize.jpg" alt="" width="25%" height="25%"/>                            <h2 style="color:#3b4966;"><%=companyName%></h2>

                                <p><%=addressline1%><br>                                <%=addressline2%><br>                                <%=addressline3%>                                                                  <br>                                                                <abbr>F:</abbr><%=companyPhone%>                            </p>
                            </td>
                            <td class="alignr"><h2 style="color:#3b4966;">Invoice Number : <%=invoiceid%></h2></td>
                        </tr>
                    </tbody></table>
            </div>

            <div style="height:20px"></div>

            <div id="invoice-to">

                <table style="width: 100%;">
                    <tbody><tr>
                            <td>
                                <h2><%=parObj.getPartnerName()%></h2>
                                <p><%=reqPar.getAddress()%><br><abbr>P:</abbr><%=reqPar.getPhone()%><br>                            </p>
                            </td>
                            <td style="width:40%;"></td>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Invoice Date : </td>
                                            <td><%=invoiceDate%></td>
                                        </tr>                                    
                                        <tr>
                                            <td>Package Name : </td>
                                            <td> <%=packageName%></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div id="invoice-items">
                <table class="table table-striped" id="invoiceDetailsTable">
                    <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>


                    <tbody>
                        <%
                            int count = 1;
 //                           NumberFormat form = new DecimalFormat("#0.00");
                            double totalPayment = 0.0f;
                            for (Map.Entry<String, Double> entry : amount.entrySet()) {
                        %> 
                        <tr>
                            <td><%=count%></td>
                            <td><%=entry.getKey()%></td>
                            <td></td>
                            <td><%=form.format(entry.getValue())%></td>
                            <td><%=form.format(entry.getValue())%></td>
                        </tr>
                        <%
                                count++;
                                totalPayment = totalPayment + entry.getValue();
                            }
                            if (!taxMap.isEmpty()) {
                                for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                    if(entry.getKey().equalsIgnoreCase("GST Tax")) {
                                        String[] taxArr = entry.getValue().split(":");
                        %>
                        <tr>
                            <td><%=count%></td>
                            <td><%=entry.getKey()%></td>
                            <td><%=taxArr[0]%> %</td>
                            <td><%=form.format(Double.parseDouble(taxArr[1]))%></td>
                            <td><%=form.format(Double.parseDouble(taxArr[1]))%></td>
                        </tr>
                        <%
                                        count++;
                                        totalPayment = totalPayment + Float.parseFloat(taxArr[1]);
                                    }
                                }
                            }                            
                            double roundedTotalPaid = (double) Math.round(totalPayment * 100) / 100;
                            String totalPaymentAmount = form.format(roundedTotalPaid);
                        %>

                        <tr>
                            <td class="no-bottom-border" colspan="3"></td>                
                            <td id="totalAmount">Total</td>
                            <td><strong><%=totalPaymentAmount%></strong></td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" id="countDetails" name="countDetails" value="<%=count-1%>">
                <input type="hidden" id="appliedPromocode" name="appliedPromocode" value="no">
                <input type="hidden" id="totalPaymentAmount" name="totalPaymentAmount" value="<%=roundedTotalPaid%>">
                <input type="hidden" id="finalPaymentAmount" name="finalPaymentAmount" value="<%=roundedTotalPaid%>">
                <div class="seperator"></div>

                <table class="table table-striped" style="width: 100%; font-size:9px; color:#3b4966;">
                    <thead>
                        <tr>
                            <th><h4>Terms &amp; Condition</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>- All prices exclusive of sales/service tax which is to be paid by the client.<br>
                                - Molla Technologies shall reserves the right to review the pricing in this proposal once the job has commenced should there be a drastic change in the specification of the job.<br>
                                - 50% upon job confirmation, 50% upon delivery or project completion. 100% upon job confirmation for Maintenance, Domain &amp; Hosting Services.<br>
                                - The being designed or developed system belongs to Molla Technologies and the ownership will be transferred to the purchaser on Full Payment.<br>
                                - Order Cancellation: 30% cancellation charge (Based on total proposal value).<br>
                                - Approx. 3 weeks from the receive of content for the job. Molla Technologies reserves the right to make further adjustments to the<br>
                                duration from time-to-time, if new requirement carry out.<br>
                                - 30 days from the date mentioned above or based on Expiration Date.<br>
                                - The price quoted inclusive of 6 months maintenance for email, phone and remote support. For outstation support, Flipnix Solutions has the right to charges additional service charges based on the said location.<br>
                                <br>
                                Northern &amp; Southern Region : RM350.00 per day<br>
                                Klang Valley Areas : RM150.00 per trip<br>
                                Working Hour : Monday to Friday, 930am – 500pm (Within 24 hours response time)<br>
                                <br>
                                <!--                                Payment Made To:<br>
                                                                BANK NAME : HONG LEONG BANK<br>
                                                                BENEFICIARY NAME : FLIPNIX SOLUTIONS<br>
                                                                BENEFICIARY ACCOUNT : 05600317978</td>-->
                        </tr>
                    </tbody>
                </table>

                <table class="table table-striped" style="width: 100%; font-size:8px; color:#3b4966;">
                    <tbody>
                        <tr>
                            <td>This is computer generated invoice, no signature required.</td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <hr>
            <div style="font-size:10px; padding-left:10px">Invoice generated by <a href="#" target="_blank">Molla Technologies</a></div>
        </div>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>                          
    </body>
</html>
<%@include file="footer.jsp" %>