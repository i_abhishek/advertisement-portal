
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<style>
    .container1 {
        display: flex;
        align-content: center;

        margin-bottom: 20px;
        width: 100%;

        background-position: center;
        background-size: cover;
    }

    .content {
        width: 100%;
    }

    .container1 {
        /*        background-color: hsl(200, 100%, 95%);*/

        margin: auto;
        padding: 0%;
    }

    .comparecontainer {
        margin: 0%;
        float: left;

        height: auto;
        width: 40%;
    }

    .compare {
        padding: 5%;

        height: 100%;
        width: 100%;
    }

    /*    .lite {
            background-color: hsl(40, 100%, 50%);
        }
    
        .full {
            background-color: hsl(150, 80%, 50%);
        }*/


    /*
        .button {
            background-color: rgba(255, 255, 255, 0.2);
            color: #fff;
            font-family: ;
            font-size: 18px;
            font-weight: 500;
            line-height: 200%;
            text-decoration: none;
            text-transform: uppercase;
            display: inline-block;
            padding: 1% 3%;
            border: 2px solid #fff;
            border-radius: 0;
            outline: none;
        }*/

    /*    .button:hover,
        .button:active {
            background-color: #fff;
        }
    
        .button:hover .lite {
            color: hsl(40, 100%, 50%);
        }
    
        .button .full:hover {
            color: hsl(150, 80%, 50%);
        }*/



    /*    h2 {
            font-size: 36px;
            margin: auto;
        }
    
        h3 {
            font-size: 21px;
            margin: 2.5% 0;
        }
    
        h4 {
            font-size: 21px;
            margin: 10% 0;
        }*/

    li {
        list-style: none;
        /*        margin: 5% 0;*/
    }

    /*    li {
            margin: 2.5% 0;
        }*/
</style>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Video Gallery
            </h2>
            <small>Learn with video tutorials </small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">
                <div class="panel-body">
                    <p>
                        <strong>Video Gallery,</strong>  within our gallery you will find Dashboard Tutorials and Service Tutorials. Learn More about Ready APIs from here.
                    </p>
                    <br>
                    <ul class="nav nav-tabs">                        
                        <li class="active"><a data-toggle="tab" href="#tab-2"> Dashboard</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Services</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active">  
                            <div class="container1">
                                <div class="content">
                                    <div class="comparecontainer">
                                        <div class="compare lite">
                                            <ul>
<br>
                                                <li>
                                                    <b>1) Dashboard </b><br> 
                                                    <p>
                                                        This video gives the details about dashboard  screen with explanation of each menu. 
                                                    </p>
                                                </li><br><br><br><br><br><br><br><br>
                                                <li><b>2) Change Password and My Profile</b><br> 
                                                    <p>
                                                        This video gives the details about how you can change your password and your profile details.
                                                    </p>
                                                </li><br><br><br><br><br><br><br><br>
                                                <li>
                                                    <b>3) Quick Tour</b>
                                                    <p>
                                                        This video gives the details about the tour option and how it work.
                                                    </p>
                                                </li><br><br><br><br><br><br><br>
                                                <li>
                                                    <b>4) Overview of Services</b>
                                                    <p>
                                                        This video gives the details about the services and how it work.
                                                    </p>
                                                </li><br><br><br><br><br><br>
                                                <li>
                                                    <b>5) Reports</b>
                                                    <p>
                                                        This video gives the details about the reports menu and explain each report options.
                                                    </p>
                                                </li><br><br><br><br><br><br><br>
                                                <li>
                                                    <b>6) Invoice</b>
                                                    <p>
                                                        This video gives the details about the invoice menu and explain how it works.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="comparecontainer">
                                        <div class="compare full">                                            
                                            <div class="col-sm-12 col-sm-offset-2">
                                                <div class="lightbox-gallery dark mrb35">
                                                    <ul id="videos-without-poster" class="list-unstyled row">
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=ZIMbwhKZ7cs" data-sub-html="Dashboard" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="images/video/dashoard.jpg" alt="Dashboard">
                                                                <div class="demo-gallery-poster">                                                                    
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=2X6XOykpFJY" data-sub-html="Change Password and My Profile" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/2X6XOykpFJY/maxresdefault.jpg" alt="Change Password and My Profile">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=gpyyIJ1d-a0" data-sub-html="Quick Tour" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="images/video/quicktour.jpg" alt="Quick Tour">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=N-uO7D9lb8c" data-sub-html="Overview of Services" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="images/video/service.jpg" alt="Overview of Services">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=MCo3XL1H62g" data-sub-html="Reports" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/MCo3XL1H62g/maxresdefault.jpg" alt="Reports">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>                                                        
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=YKzofsLeVB4" data-sub-html="Invoice" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="images/video/invoice.jpg" alt="Invoice">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>                                                                                                                                                                           
                                                    </ul>
                                                </div>                                    
                                            </div>				                                     
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="container1">
                                <div class="content">
                                    <div class="comparecontainer">
                                        <div class="compare lite">                                            
                                            <ul>
                                                <br>
                                                <li><b>1) DB As Service  </b><br> <p>By using DB As Service we can expose our database like oracle, mysql and cassandra . User can fired various queries on database as per the requirement and easily access the database. We can also save our database connection using this services.</p></li><br><br><br>
                                                <li>
                                                    <b>2) Document Utility (Document Signing)</b><br> 
                                                    <p>
                                                        Service that lets us sign documents digitally without printing, scanning, or faxing. It's a win for both business and personal users because electronic signing is fast, easy, and secure. Electronic signatures are trusted by millions around the world and are available in dozens of languages. Send and sign anywhere, anytime, on any Internet-enabled device. Contracts, agreements, loans, leases, all can be signed quickly and securely.
                                                    </p> 
                                                <li><br>
                                                    <b>3) Document Utility (Operation On PDF)</b>
                                                    <p>
                                                        This video gives the details about operations perform on PDF like compress PDF, Merge multiple PDF into one, rotate PDF, image to PDF convert and text with all setting, and encryption decryption of PDF. All this features combined into one in document utility service.                                                        
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>4) Document Utility (Operation On PDF 2)</b>
                                                    <p>
                                                        This video gives the details about operations like conversion of PDF to image convert and vice versa, adding water mark image and text on PDF this API.
                                                    </p>
                                                </li><br><br><br><br>
                                                <li>
                                                    <b>5) Google Auth Token (Company and user creation with token assignment and verify otp)</b>
                                                    <p>
                                                        Google Authentication  is an application that implements two-step verification services for authenticating users of mobile applications by Google.
                                                        This video gives the details about company and user creation with token assignment and verify otp.
                                                    </p>
                                                </li><br><br>
                                                <li>
                                                    <b>6) Google Auth Token (Generating reports and remove token, user and company details)</b>
                                                    <p>
                                                        Google Authentication  is an application that implements two-step verification services for authenticating users of mobile applications by Google.
                                                        This video gives the details about generating reports and remove token, user and company details.
                                                    </p>
                                                </li><br><br>
                                                <li>
                                                    <b>7) Image utility</b>
                                                    <p>
                                                        The Image Utility service provides various image processing features like image format conversion to convert an image form one to another image format, extract image metadata feature to retrieve meta data of image, making GIF file from using multiple images, adding text watermark on image, encrypt data in the image and retrieve encrypted data from image.
                                                    </p>
                                                </li><br><br>
                                                <li>
                                                    <b>8) Tokanization</b>
                                                    <p>
                                                        Tokenization, when applied to data security, is the process of substituting a sensitive data element with a non-sensitive equivalent, referred to as a token, This video explain data processing applications with the authority and interfaces to request tokens, or detokenize back to sensitive data.
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>9) Value Added Services (Bulk email validation)</b>
                                                    <p>
                                                        Value Added Services provides various features, This video gives the details about EmailId validation to validate email ids
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>10) Value Added Services (Issue QR code with and without password)</b>
                                                    <p>
                                                        Value Added Services provides various features, This video gives the details about issue QR code with and without password
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>11) Value Added Services (Shorten url and event calender generation)</b>
                                                    <p>
                                                        Value Added Services provides various features, This video gives the details about shorten url and event calender generation.
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>12) Value Added Services (Validation of mobile number, credit card, format of file and bank details)</b>
                                                    <p>
                                                        Value Added Services provides various features, This video gives the details about Validation of mobile number, credit card, format of file and bank details.
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>13) IP2GEO</b>
                                                    <p>
                                                        This service has a goal of providing geo location service to all users . It provide all details of server location like country, state, latitude, longitude, etc. For  which ip address we have pass as input.
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>14) Instant web chat</b>
                                                    <p>
                                                        This Service is used to do conference video call. The generated URL will be accessible on chrome, opera, firefox as well android and iOS browsers.
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>15) Reverse Proxy</b>
                                                    <p>
                                                        A reverse proxy provides an additional level of abstraction and control to ensure the smooth flow of network traffic between clients and servers. This service provides high level of security for servers using reverse proxy url.
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>16) InApp Push (Creation of DeveloperId and ApplicationId)</b>
                                                    <p>
                                                       This video is the first step in your InApp Push service, you need to create DeveloperId and ApplicationId for your Push application 
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>17) InApp Push (Safari Push Notification)</b>
                                                    <p>
                                                       This video explain the InApp Push service for Safari browser.
                                                    </p>
                                                </li><br><br><br><br><br><br><br>
                                                <li>
                                                    <b>18) InApp Push (iOS Push Notification)</b>
                                                    <p>
                                                       This video explain the InApp Push service for iOS mobile.
                                                    </p>
                                                </li><br><br><br><br><br><br><br>
                                                <li>
                                                    <b>19) Server Monitoring (Creation of app groups and app and make them start)</b>
                                                    <p>
                                                        Monitor your entire infrastructure and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to create of app groups and app and make them start.
                                                    </p>
                                                </li><br>
                                                <li>
                                                    <b>20) Server Monitoring (Editing of app group and app details)</b>
                                                    <p>
                                                        Monitor your entire infrastructure and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how editing of app group and app details.
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>21) Server Monitoring (Listing of apps group and app details)</b>
                                                    <p>
                                                        Monitor your entire infrastructure and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to listing of apps group and app details.
                                                    </p>
                                                </li><br><br>
                                                <li>
                                                    <b>22) Server Monitoring (Stopping groups, apps and remove app details)</b>
                                                    <p>
                                                        Monitor your entire infrastructure and get in-depth visibility into key performance indicators of server.Server monitoring as a service for all your servers and devices. We support java servers. Please have a look this video for how to stop groups, app and remove app details.
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>23) Certificate Issuer</b>
                                                    <p>
                                                        Certificate issuance is used for generating the certificate, and issuing it to respective user, we can also issue certificate for application server, by two ways certificate is generated 1) It create the user and issue the cert and 2) without creating the developer and adding it .
                                                    </p>
                                                </li><br><br><br>
                                                <li>
                                                    <b>24) SSL Cert Discovery</b>
                                                    <p>
                                                        Identify all certificates on your public-facing domains, including every SSL termination endpoint. Scan multiple networks and ports for internal certificates to find old certificates. And find much more with this video.
                                                    </p>
                                                </li><br><br><br><br><br>
                                                <li>
                                                    <b>25) Dynamic Image Auth</b>
                                                    <p>
                                                        This video explain, Now one line transactions supports Multi factor authentication. This service provide option for 2nd factor authentication with secure phrase. use can set his own phase as image with selected color and security sweet spot on created image and set sweet spot deviation for set image and sweet spot. 
                                                    </p>
                                                </li><br><br><br>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="comparecontainer">
                                        <div class="compare full">                                                                                        
                                            <div class="col-sm-12 col-sm-offset-2">
                                                <div class="lightbox-gallery dark mrb35">
                                                    <ul id="videos-without-poster2" class="list-unstyled row">
                                                        <li class="video" data-src="https://youtu.be/HKo41a2aCHg" data-sub-html="DB As Service" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/HKo41a2aCHg/maxresdefault.jpg" alt="DB As Service">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://youtu.be/zPHjftuZdvI" data-sub-html="Document Utility (Document Signing)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/zPHjftuZdvI/maxresdefault.jpg" alt="Document Utility (Document Signing)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=_zNhOuOgbro" data-sub-html="Document Utility (Operation On PDF)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg" alt="Document Utility (Operation On PDF)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=Hjpmyhs8aLg" data-sub-html="Document Utility (Operation On PDF 2)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/Hjpmyhs8aLg/maxresdefault.jpg" alt="Document Utility (Operation On PDF 2)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=0AdbHoT_LKE" data-sub-html="Google Auth Token (Company and user creation with token assignment and verify otp)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg" alt="Company and user creation with token assignment and verify otp">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>                                                        
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=tVgIXPe-gn0" data-sub-html="Google Auth Token (Generating reports and remove token, user and company details)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg" alt="Google Auth Token (Generating reports and remove token, user and company details)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>   
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=ck7u1V0Sjtg" data-sub-html="Image utility" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/ck7u1V0Sjtg/maxresdefault.jpg" alt="Image utility">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li> 
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=XXcM5_PmzYU" data-sub-html="Tokanization" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/_zNhOuOgbro/maxresdefault.jpg" alt="Tokanization">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=5uXmt2iZfSU" data-sub-html="Value Added Services (Bulk email validation)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/5uXmt2iZfSU/maxresdefault.jpg" alt="Value Added Services (Bulk email validation)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=NeYP4HHqCgQ" data-sub-html="Value Added Services (Issue QR code with and without password)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/NeYP4HHqCgQ/maxresdefault.jpg" alt="Value Added Services (Issue QR code with and without password)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=nXAgnybLSMc" data-sub-html="Value Added Services (Shorten url and event calender generation)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/nXAgnybLSMc/maxresdefault.jpg" alt="Value Added Services (Shorten url and event calender generation)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=KuL2_rSqUJc" data-sub-html="Value Added Services (Validation of mobile number, credit card, format of file and bank details)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/nXAgnybLSMc/maxresdefault.jpg" alt="Value Added Services (Validation of mobile number, credit card, format of file and bank details)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=xDBSjR968T0" data-sub-html="IP2GEO" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/xDBSjR968T0/maxresdefault.jpg" alt="IP2GEO">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=Y3R_3HVAMfI" data-sub-html="Instant web chat" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/Y3R_3HVAMfI/maxresdefault.jpg" alt="Instant web chat">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=EvFjboHxqeg" data-sub-html="Reverse Proxy" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/Y3R_3HVAMfI/maxresdefault.jpg" alt="Reverse Proxy">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=75qQvOlHwNI" data-sub-html="InAppPush (Creation of Developer and Application Id)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="images/video/InAppPushService.jpg" alt="InAppPush (Creation of Developer and Application Id)">
                                                                <div class="demo-gallery-poster">                                                                    
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=eCO6rbVifko" data-sub-html="InAppPush (Safari Push Notification)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/eCO6rbVifko/maxresdefault.jpg" alt="InAppPush (Safari Push Notification)">
                                                                <div class="demo-gallery-poster">                                                                    
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=on1_MHs8Lfs" data-sub-html="InAppPush (iOS Push Notification)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/on1_MHs8Lfs/maxresdefault.jpg" alt="InAppPush (iOS Push Notification)">
                                                                <div class="demo-gallery-poster">                                                                    
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=C4qzC5Ft6RU" data-sub-html="Server Monitoring (Creation of app groups and apps and make them start)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/C4qzC5Ft6RU/maxresdefault.jpg" alt="Server Monitoring (Creation of app groups and apps and make them start)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=w2RnYRHzQZM" data-sub-html="Server Monitoring (Editing of apps group and apps details)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/w2RnYRHzQZM/maxresdefault.jpg" alt="Server Monitoring (Editing of apps group and apps details)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=5BihokbW9JM" data-sub-html="Server Monitoring (Listing of apps group and app details)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/5BihokbW9JM/maxresdefault.jpg" alt="Server Monitoring (Listing of apps group and app details)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=La204FLC3bA" data-sub-html="Server Monitoring (Stopping groups, apps and remove app details)" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/La204FLC3bA/maxresdefault.jpg" alt="Server Monitoring (Stopping groups, apps and remove app details)">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=Flstok_J-Yo" data-sub-html="Certificate Issuer" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/Flstok_J-Yo/maxresdefault.jpg" alt="Certificate Issuer">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=PvUbuo5zUNg" data-sub-html="SSL Cert Discovery" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/PvUbuo5zUNg/maxresdefault.jpg" alt="SSL Cert Discovery">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                        <li class="video" data-src="https://www.youtube.com/watch?v=hOGEHDxV9NM" data-sub-html="Dynamic Image Auth" >
                                                            <a href="">
                                                                <img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/hOGEHDxV9NM/maxresdefault.jpg" alt="Dynamic Image Auth">
                                                                <div class="demo-gallery-poster">
                                                                    <img src="images/video/play-button.png" alt=""/>
                                                                </div>
                                                            </a> 
                                                        </li>
                                                    </ul>
                                                </div>                                    
                                            </div>				                                     
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="scripts/video/fileinput.min.js" type="text/javascript"></script>
<script src="scripts/video/bootstrap-select.min.js" type="text/javascript"></script>
<script src="scripts/video/accordionSnippet.js" type="text/javascript"></script>
<script src="scripts/video/tabSnippet.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
    });
</script>
<script src="scripts/video/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
format: 'mm-dd-yyyy H:ii P',
autoclose: 1,
showMeridian: 1
});
$(".form_date").datetimepicker({
format: 'mm-dd-yyyy',
autoclose: 1,
showMeridian: 1,
minView: 2,
forceParse: 0
});
$(".form_time").datetimepicker({
format: 'H:ii P',
autoclose: 1,
showMeridian: 1,
startView: 1,
minView: 0,
maxView: 1,
forceParse: 0
});
</script>
<script src="scripts/video/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

<script src="scripts/video/picturefill.min.js" type="text/javascript"></script>
<script src="scripts/video/lightgallery.js" type="text/javascript"></script>
<script src="scripts/video/lg-fullscreen.js" type="text/javascript"></script>
<script src="scripts/video/lg-thumbnail.js" type="text/javascript"></script>
<script src="scripts/video/lg-video.js" type="text/javascript"></script>
<script src="scripts/video/lg-autoplay.js" type="text/javascript"></script>
<script src="scripts/video/demos.js" type="text/javascript"></script>
<script src="scripts/video/lg-zoom.js" type="text/javascript"></script>
<script src="scripts/video/lg-hash.js" type="text/javascript"></script>
<script src="scripts/video/lg-pager.js" type="text/javascript"></script>
<script src="scripts/video/jquery.mousewheel.min.js" type="text/javascript"></script>
<link href="scripts/video/lightgallery.css" rel="stylesheet" type="text/css"/>
<link href="scripts/video/videogallery.css" rel="stylesheet" type="text/css"/>