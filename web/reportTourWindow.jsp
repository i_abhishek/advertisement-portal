<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<!--<script src="scripts/allReportsV2.js" type="text/javascript"></script>
<script src="scripts/billingReport.js" type="text/javascript"></script>
<script src="scripts/partnerRequest.js" type="text/javascript"></script>-->
<div class="small-header transition animated fadeIn">
    <div class="hpanel tour-reportMenu">
        <div class="panel-body">                
            <div id="hbreadcrumb">                    
                <p style="font-size:18px !important;" ><span id="resourceName" style="padding-right: 65%;width: 5%"></span> Select Report 
                    <select  style="width: 20%;margin-left: 40px" id="apiConsoleSelectBox" name="apiConsoleSelectBox" onchange="getReportConsole(this.value)">
                        <optgroup label="Select Report">                                
                            <option value="0" selected>Credit Usage</option>
                            <option value="1">API Usage</option>
                            <option value="2">Transaction Report</option> 
                            <option value="3">Performance Report</option> 
                        </optgroup>
                    </select>
                </p>    
            </div>                         
        </div>
    </div>
</div>                                
<div class="wrapper">
    <div class="content animate-panel tour-reportWindow">        
        <div class="row ">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <h4>Credit Information And Statistics</h4>
                    </div>
                    <div class="panel-body">
                        <div class="text-left">
                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 days expenditure</span>
                        </div>
                        <div id="report_data_button">
                            <div class="btn-group" style="padding-left: 70%">
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="dailyTxReport()"> Daily</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="lastSevenDayEx()"> Last 7 days</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="monthlyTxReportByButton()"> Last 30 days</button>
                            </div>
                        </div>
                        <hr class="m-b-xl"/>                         
                        <div id="expenditureReport">
                        </div>
                        <div id="noRecordFoundData" style="display: none" style="margin-bottom: 30%">
                            <img src="images/no_record_found.jpg" alt="No record found" width="300px" height="300px" style="margin-left: 35%"/>
                        </div> 
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>
   
<!--</div>-->
<script>
    $(function () {
    
     var dateArr2 = ["Data e Ora",'2017-07-30','2017-07-31','2017-08-01','2017-08-02','2017-08-03','2017-08-04','2017-08-05'];
    var txAmount2 = ['CreditUsed',10,50,60,90,40,100,40];
    var callCountArr2 = ['Call Count',2,15,20,30,15,40,20];        
    console.log("arrXaxis >> " + dateArr2);
    console.log("callCountArr2 >> " + callCountArr2);
    console.log("txAmount2  >> " + txAmount2);   
    
    var chart = c3.generate({
        bindto: '#expenditureReport',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d',
            columns: [
                dateArr2,                
                txAmount2,
                callCountArr2
            ],
            colors: {
//                TransactionAmount: '#62cb31',
//                callCountArr2: '#FF7F50'
                  dateArr2: '#62cb31',
                  CreditUsed: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CreditUsed:'bar'
            },
            groups: [
                ['Transcation Amount', 'Call Count']
            ]
        },
        subchart: {
            show: true
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%d-%m-%Y'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    });              
});
</script>

