<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.sun.xml.internal.messaging.saaj.util.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>

<script src="scripts/apiConsoleV2.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<style>
    #resourceName {
        text-transform: capitalize;
    }
</style>
<style>
    .copied::after {
        position: absolute;
        top: 12%;
        right: 110%;
        display: block;
        content: "copied";
        font-size: 0.75em;
        padding: 2px 3px;
        color: #FFF;
        background-color: #2381c4;
        border-radius: 3px;
        opacity: 0;
        will-change: opacity, transform;
        animation: showcopied 1.5s ease;
    }

    @keyframes showcopied {
        0% {
            opacity: 0;
            transform: translateX(100%);
        }
        70% {
            opacity: 1;
            transform: translateX(0);
        }
        100% {
            opacity: 0;
        }
    }
</style>
<div class="modal fade hmodal-success" id="tmTermsAndCondition" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header" style="padding: 10px 0px !important">
                <h4 id="tokenType" style="margin-left: 2%">Your APITokenId is
                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
            </div>
            <div class="modal-body" style="padding-bottom: 20px !important">
                <!--                <img src="images/TokenLogo.png" alt="" style="margin-left: 35%; margin-bottom: 7%"/>-->
                <div class="form-group">
                    <div class="col-lg-7">
                        <p id="test1"></p>
                    </div>                                        
                    <div class="col-lg-5"> 
                        <div id="inputTag"></div>
                    </div>
                </div>
            </div>   
            <div class="modal-footer">
                <div class="col-lg-8 col-sm-8 col-md-8"></div>
                <div class="col-lg-2 col-sm-2 col-md-2" id="copyDiv"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade hmodal-success" id="apiDescriptionModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header" style="padding: 10px 0px !important">
                <h4 id="apiDescriptionFor" style="margin-left: 2%">Your API description is
                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
            </div>
            <div class="modal-body" style="padding-bottom: 50px !important">
                <!--                <img src="images/TokenLogo.png" alt="" style="margin-left: 35%; margin-bottom: 7%"/>-->
                <div class="form-group">
                    <div class="col-lg-12">
                        <p id="apiDescription"></p>
                    </div>                                        
                    <div class="col-lg-5"> 
                        <div id="inputTag"></div>
                        <div id="copyDiv"></div>
                    </div>
                </div>
            </div>                                
        </div>
    </div>
</div>

<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li style="font-size:18px !important;">Change Resource
                        <select  id="apiConsoleSelectBox" name="apiConsoleSelectBox"  onchange="getAPIConsole(this.value)">
                            <optgroup label="Select API End Point">
                                <%
                                    Accesspoint[] accessObj = new AccessPointManagement().getAceesspoints();
                                    Warfiles warfiles = null;
                                    String accessPointId = request.getParameter("accesspointId");
                                    String resID = request.getParameter("resourceId");
                                    int acid = 0;
                                    int resid = 0;
                                    if (accessPointId != null) {
                                        acid = Integer.parseInt(accessPointId);
                                    }
                                    if (resID != null) {
                                        resid = Integer.parseInt(resID);
                                    }
                                    boolean resourceFound = false;
                                    if (accessObj != null) {
                                        for (int acci = 0; acci < accessObj.length; acci++) {
                                            if (accessObj[acci].getStatus() != GlobalStatus.DELETED && accessObj[acci].getStatus() != GlobalStatus.SUSPEND) {
                                                Accesspoint apdetails = accessObj[acci];
                                                TransformDetails transformDetails = new TransformManagement().getTransformDetails(apdetails.getChannelid(), apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                if (transformDetails != null) {
                                                    warfiles = new WarFileManagement().getWarFile(apdetails.getChannelid(), apdetails.getApId());
                                                    if (warfiles != null) {
                                                        String[] resources = apdetails.getResources().split(",");
                                                        JSONObject errJSON = null;
                                                        ResourceDetails resObj = null;
                                                        if (resources != null) {
                                                            for (int j = 0; j < resources.length; j++) {
                                                                int resourceVersion = 1;
                                                                resObj = new ResourceManagement().getResourceById(Integer.parseInt(resources[j]));
                                                                resourceFound = true;
                                                                String apName = apdetails.getName();
                                                                if (apdetails.getDisplayName() != null) {
                                                                    apName = apdetails.getDisplayName();
                                                                }
                                                                if (apdetails.getApId() == acid && resid == resObj.getResourceId()) {

                                %>
                                <option value="<%=apdetails.getApId()%>:<%=resObj.getResourceId()%>:<%=apName%>" selected><%=apName%></option>
                                <%                      } else {%>
                                <option value="<%=apdetails.getApId()%>:<%=resObj.getResourceId()%>:<%=apName%>"><%=apName%></option>                  

                                <%}
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                %>                                                    
                                <option value="0:0">NA</option>                            
                                <%}%>                                    
                            </optgroup>
                        </select>
                    </li>

                </ol>
            </div>
            <h2 class="font-light m-b-xs" style="font-size: 22px!important">
                <font id="resourceName" style="font-weight: 80%">Resource</font>
            </h2>

        </div>
    </div>
</div>

<div id="apiDetails" class="content animate-panel">
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br>
    <div class="col-lg-12"><br></div>
    <div class="col-md-12" style="margin-bottom: 90%"></div>
</div>                             

<script>
    $(function () {
        $('#api').dataTable();
    });
    $(function () {
        $('#data_type').dataTable();
    });
    $(document).ready(function () {
        var apandresourceId = document.getElementById("apiConsoleSelectBox").value;
        getAPIConsole(apandresourceId);
    });
</script>
