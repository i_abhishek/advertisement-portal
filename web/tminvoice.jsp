<%@page import="java.net.URLEncoder"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgDailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgHourlyTXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgTranscationdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CAASCDRCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CDRTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCaastransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LatePenalitiesCalculationManagement"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.GenerateInvoiceId"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Developer Portal</title>
        <!-- Bootstrap Core CSS -->
        <!--        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
        <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!--    <link href="invoice_css/invoiceDetail.css" rel="stylesheet" type="text/css"/>
            <link href="invoice_css/style.css" rel="stylesheet" type="text/css"/>-->
        <!-- Custom CSS -->        
        <link href="styles/shop-homepage.css" rel="stylesheet" type="text/css"/>
        <script src="jshash/sha1-min.js" type="text/javascript"></script>
        <script src="jshash/sha1.js" type="text/javascript"></script>        
        <script src="scripts/paymentOperation.js" type="text/javascript"></script>
        <!--        <script src="js/promocode.js" type="text/javascript"></script>-->
        <!-- jQuery -->
        <!--        <script src="js/jquery.js"></script>-->
        <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>        
        <!--        <script src="js/jquery-ui-1.10.4.min.js"></script>-->        
        <script src="vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!--        <script src="js/jquery-1.8.3.min.js"></script>-->
        <script src="vendor/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <!--        <script src="js/bootstrap.min.js"></script>-->
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <!--        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>-->
        <!--        <script src="./js/modal.js"></script>-->
    </head>
    <%
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        String packageName = request.getParameter("_packageName");
        String _paymentMode = request.getParameter("_paymentMode");
        String outPackageName = packageName;
        if (packageName != null && !packageName.isEmpty()) {
            if (packageName.length() >= 16) {
                outPackageName = packageName.substring(0, 14);
            }
        }
        SgPartnerrequest reqPar = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, parObj.getPartnerId());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String invoiceDate = dateFormat.format(new Date());
        String invoiceid = GenerateInvoiceId.getDate() + parObj.getPartnerId() + GenerateInvoiceId.getRandom();
        
        SgReqbucketdetails packageObject =  new RequestPackageManagement().getReqPackageByName(SessionId, ChannelId, packageName);
        String paymentType = packageObject.getPaymentMode();
        //SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, ChannelId, parObj.getPartnerId());
        NumberFormat form = new DecimalFormat("#0.00");
        Double totalPaid = new Double(0);
        Double changeOnPackageCharge = null;
        Double reActivationCharge = null;
        Double cancellationCharge = null;
        Double latePenaltyCharge = null;

        // variable for gst tax cal
        Double totalAmountWithoutTax = 0.00;
        Double gstTax = 0.00;
        Double vatTax = 0.00;
        Double serviceTax = 0.00;

        Map<String, Double> amount = new LinkedHashMap<String, Double>();
        Map<String, String> taxMap = new LinkedHashMap<String, String>();

        if (packageObject != null) {
            SgSubscriptionDetails subscriObjPrepaid = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
            packageName = packageObject.getBucketName();
            double planAmount = packageObject.getPlanAmount();
            amount.put("Package Amount", planAmount);
            totalPaid += planAmount;

            double serviceCharge = Float.parseFloat(packageObject.getServiceCharge());
            amount.put("Service Charge", serviceCharge);
            totalPaid += serviceCharge;
            if (subscriObjPrepaid != null) {

                if (!packageObject.getBucketName().equals(subscriObjPrepaid.getBucketName())) {
                    double changePackageCharge = subscriObjPrepaid.getChangePackageRate();
                    amount.put("Change Package Charge", changePackageCharge);
                    totalPaid += changePackageCharge;
                    changeOnPackageCharge = changePackageCharge;
                    session.setAttribute("_changeOnPackageCharge", String.valueOf(changePackageCharge));
                }

                if (packageObject.getBucketName().equals(subscriObjPrepaid.getBucketName())) {
                    double reactivationCharge = Float.parseFloat(String.valueOf(subscriObjPrepaid.getReActivationCharge()));
                    amount.put("Reactivation Charge", reactivationCharge);
                    totalPaid += reactivationCharge;
                    reActivationCharge = reactivationCharge;
                    session.setAttribute("_reactivationCharge", String.valueOf(reactivationCharge));
                }

                String expiryDate = dateFormat.format(subscriObjPrepaid.getExpiryDateNTime());
                String strDate = dateFormat.format(new Date());
                Date curDate = dateFormat.parse(strDate);
                Date exDate = dateFormat.parse(expiryDate);
                if (curDate.before(exDate)) {
                    double packageCancellationCharge = subscriObjPrepaid.getCancellationRate();
                    amount.put("Cancellation Charge", packageCancellationCharge);
                    totalPaid += packageCancellationCharge;
                    cancellationCharge = packageCancellationCharge;
                    session.setAttribute("_cancellationCharge", String.valueOf(packageCancellationCharge));
                }
                // get info about loan

//                    loanObj = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObjPrepaid.getBucketId());
//                    if (loanObj != null) {
//                        if (loanObj.getStatus() == GlobalStatus.UNPAID) {
//                            String approveDate = dateFormat.format(loanObj.getLoanApproveOn());
//                            String loanDetails = "Loan " + loanObj.getLoanAmount() + " on package " + subscriObjPrepaid.getBucketName() + " approved on " + approveDate + " with interest " + loanObj.getInterestRate();
//                            float intererstOnLoan = (loanObj.getLoanAmount() * loanObj.getInterestRate()) / 100;
//                            float totalLoan = loanObj.getLoanAmount() + intererstOnLoan;
//                            loanDetails = loanDetails + ":" + totalLoan;
//                            taxMap.put("Loan ", loanDetails);
//                        }
//                    }
            }
            totalAmountWithoutTax = totalPaid;
            String taxRate = packageObject.getTax();
            taxMap = TaxCalculationManagement.calculateTax(taxRate, totalPaid);

            String calculatedGSTDetail = (String) taxMap.get("GST Tax");
            String calculatedVATDetail = (String) taxMap.get("VAT Tax");
            String calculatedSTDetail = (String) taxMap.get("Service Tax");

            String[] calculatedGST = calculatedGSTDetail.split(":");
            String[] calculatedVAT = calculatedVATDetail.split(":");
            String[] calculatedST = calculatedSTDetail.split(":");

            totalPaid += Double.parseDouble(calculatedGST[1]);
//                totalPaid += Double.parseDouble(calculatedVAT[1]);
//                totalPaid += Double.parseDouble(calculatedST[1]);

            gstTax = Double.parseDouble(calculatedGST[0]);
            vatTax = Double.parseDouble(calculatedVAT[0]);
            serviceTax = Double.parseDouble(calculatedST[0]);
        }

        double roundedTMREQAMOUNT = (double) Math.round(totalPaid * 100) / 100;
        String tmWellFormAmount = form.format(roundedTMREQAMOUNT);
        String wellFormTotalBeforeTax = form.format(totalAmountWithoutTax);

        String tmMerchantID = LoadSettings.g_sSettings.getProperty("tm.pg.merchantId");
        String tmMerchantPWD = LoadSettings.g_sSettings.getProperty("tm.pg.merchantPWD");

        long tmMerchant_TranID = (long) (Math.random() * 100000000000000L);
        tmMerchant_TranID += 500000000000000L;

        JSONObject partnerObj = new JSONObject();

        session.setAttribute("tierOrSlabUsageDetails", partnerObj);
        SimpleDateFormat invoiceDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Double flatPrice = 0.00;

        SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        String billingAddress = "NA";
        String billingState = "NA";
        String billingPostCode = "NA";
        String billingCountry = "NA";

        if (productionEnvt != null) {
            billingAddress = productionEnvt.getBillingAddress();
            billingState = productionEnvt.getBillingState();
            billingPostCode = productionEnvt.getBillingPostCode();
            billingCountry = productionEnvt.getBillingCountry();
        }
        //String jsonFlatPriceDetails = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        String paymode = URLEncoder.encode(new String(Base64.encode(_paymentMode.getBytes())), "UTF-8");
        System.out.println("paymode >>>> " + paymode);

        session.setAttribute("_originalPackageName", packageName);
        session.setAttribute("_invoiceId", invoiceid);
        session.setAttribute("_packageObject", packageObject);
         String stripeAmount="0";
    %>
    <script>
        function print_specific_div_content() {
            var win = window.open('', '', 'left=0,top=0,width=700,height=800,toolbar=0,scrollbars=0,status =0');
            var content = "<html>";
            content += "<link href='css/shop-homepage.css' rel='stylesheet' type='text/css'/>";
            content += "<body onload=\"window.print(); window.close();\">";
            content += document.getElementById("invoice-container1").innerHTML;
            content += "</body>";
            content += "</html>";
            win.document.write(content);
            win.document.close();
        }
    </script>
    <div id="havePromocode" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 id="validatesPackageModal">Have a promocode ?</h4>
                </div>          
                <div class="modal-body" style="height: 70px">
                    <form  id="validatePromoForm">                        
                        <label class="control-label col-lg-3"  for="partnername">Promo code</label>
                        <div class="controls col-lg-9">
                            <input type="hidden"  id="_promoId" name="_promoId" >
                            <input type="hidden"  id="_promoDiscount" name="_promoDiscount" value=0 >
                            <input type="hidden"  id="_promostatus" name="_promostatus" >
                            <input type="hidden"  class="form-group-sm" id="_promoPackage" name="_promoPackage" value="<%=packageName%>">
                            <input type="text"  id="_promoCode" name="_promoCode" autofocus>                                    
                        </div>
                    </form>
                </div>                                  
                <div class="modal-footer">
                    <div id="validate-promo-result"></div>
                    <button class="btn btn-info btn-xs" data-dismiss="modal" >Close</button>
                    <button class="btn btn-success btn-xs" onclick="validatePromoCode('<%=_paymentMode%>', '<%=packageName%>')" id="addPartnerButtonE">Validate</button>
                </div>
                <script>
                    var input = document.getElementById('_promoCode');
                    input.onkeyup = function () {
                        this.value = this.value.toUpperCase();
                    }
                </script>                                    
            </div>
        </div>
    </div>

    <body>
        <div style="padding-left: 80px;padding-top: 10px">                      
<!--            <a href="checkout.jsp?payMode=<%=paymode%>" class="btn btn-primary"> Pay with PayPal</a>-->
<!--            <a href="stripePay.jsp" class="btn btn-info stripe-button"> Pay</a>-->
            <form action="./StripeCreateCharge" method="POST">
                 <script
                  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                  data-key="pk_test_PeQG0xzLGX0rfkPKvRs7odeM"
                  data-amount="<%=stripeAmount%>"
                  data-name="Ready API"
                  data-description="<%=packageName%>"
                  data-image="images/front-logo.png?AWSAccessKeyId=AKIAI6GNVFWZW64GL7WQ&amp;Expires=1659968447&amp;Signature=OWgLMRrhGTSjgzz4LdFrUpI9nic%3D"                  
                  data-locale="auto">
                </script>
            </form>
        </div>
        <div id="invoice-container1">
            <!-- Navigation -->
            <nav class="navbar-fixed-top" role="navigation" style="background-color: #fff;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img src="images/bluebrick_logo.png" width="150" style="padding-left: 20px!important" alt=""/></a>
                    </div>
                </div>
                <!-- /.container -->
            </nav>
            <!-- Page Content -->
            <div class="container invoice">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
<!--                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <h3>Tax Invoice</h3>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6 company-name">
                                    <p>Blue Bricks Pty. Ltd.</p>
                                </div>
                            </div>-->
                            <div class="col-sm-12 col-lg-12 col-md-12" style="margin-top: 1%">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                        <tr class="information">
                                            <td><strong>Name</strong></td>
                                            <td>:</td>
                                            <td><%=parObj.getPartnerName()%></td>
                                        </tr>
                                        <tr class="information">
                                            <td><strong>Email Id</strong></td>
                                            <td>:</td>
                                            <td><%=parObj.getPartnerEmailid()%></td>
                                        </tr>
                                        <tr class="information">
                                            <td><strong>Package</strong></td>
                                            <td>:</td>
                                            <td><%=packageObject.getBucketName()%></td>
                                        </tr>
<!--                                            <td>
                                                <p><%=parObj.getPartnerName()%><br/>
                                                    <%=parObj.getPartnerEmailid()%><br/>
                                                    <%=billingState%><br/>
                                                    <%=billingPostCode%><br/>
                                                    <%=billingCountry%><br/>
                                            </td>-->
                                        </tr>
                                        </table>
                                            </td>
                                        </tr>
                                    </table>	
<!--                                    <table>
                                        <tr>
                                            <td class="table-header">Remit To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Please issue cheque payable to :</strong><br/>
                                                    Blue Bricks Pty. Ltd.<br/>
                                                    And send to :<br/>
                                                    Unit 39, 118 Adderton Road, Carlingford 2118 NSW, Australia
                                                </p>
                                            </td>
                                        </tr>
                                    </table>								-->
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Information</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr class="information">
                                                        <td><strong>Invoice Number</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceid%> </td>
                                                    </tr>
                                                    
                                                    <tr class="information">
                                                        <td><strong>Payment Date</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceDateFormat.format(new Date())%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Term Of Payment</strong></td>
                                                        <td>:</td>
                                                        <td><%=packageObject.getBucketDuration()%></td>
                                                    </tr>
<!--                                                    <tr class="information">
                                                        <td><strong>Currency</strong></td>
                                                        <td>:</td>
                                                        <td>USD</td>
                                                    </tr>-->
                                                    <!--											<tr class="information">
                                                                                                                                                    <td><strong>Revenue Code</strong></td>
                                                                                                                                                    <td>:</td>
                                                                                                                                                    <td>951</td>                                                                                                                                            </tr>-->
                                                </table>
                                            </td>
                                        </tr>
                                    </table>	
                                </div>
                            </div>					
                            <div class="col-sm-12 col-lg-12 col-md-12" style="padding: 30px;">						
                                <table id="invoiceDetailsTable">
                                    <thead>
                                        <tr>								
<!--                                            <td class="table-header">Access point</td>
                                            <td class="table-header">Duration</td>-->
                                            <td class="table-header">Description</td>
                                            <td class="table-header">Details</td>
                                            <td class="table-header">Unit Price</td>
                                            <td class="table-header">Total</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tr>
                                        <%
                                            int count = 1;
                                            Double totalAmount = 0.0;
                                            DecimalFormat df = new DecimalFormat();
                                            df.setMaximumFractionDigits(2);
                                            String apName = "NA";
                                            String resName = "NA";
                                            String envt = "NA";
                                            String apiName = "NA";
                                            int version = 0;
                                            int callCount = 0;
                                            float amountS = 0;
                                            if (partnerObj.length() != 0) {
                                                Iterator itrS = partnerObj.keys();
                                                while (itrS.hasNext()) {
                                                    Object keyS = itrS.next();
                                                    Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString());
                                                    JSONObject data = new JSONObject(partnerObj.getString((String) keyS));
                                                    Iterator itr = data.keys();
                                                    while (itr.hasNext()) {
                                                        float totalCharge = 0.0f;
                                                        Object key = itr.next();
                                                        String keyData = (String) key;
                                                        String value = data.getString((String) key);
                                                        int acc = Integer.parseInt(keyData.split(":")[0]);
                                                        int res = Integer.parseInt(keyData.split(":")[1]);
                                                        version = Integer.parseInt(keyData.split(":")[2]);
                                                        envt = (keyData.split(":")[3]);
                                                        apiName = (keyData.split(":")[4]);
                                                        Accesspoint ap = new AccessPointManagement().getAccessPointById(ChannelId, acc);
                                                        ResourceDetails rs = new ResourceManagement().getResourceById(res);
                                                        if (ap != null) {
                                                            apName = ap.getName();
                                                        }
                                                        if (rs != null) {
                                                            resName = rs.getName();
                                                        }
                                                        if (!envt.equalsIgnoreCase("Live")) {
                                                            continue;
                                                        }
                                                        callCount = Integer.parseInt(value.split(":")[0]);
                                                        amountS = Float.parseFloat(value.split(":")[1]);

                                                    }
                                                }
                                            }
                                            if (packageObject != null && packageObject.getFlatPrice() != null) {
                                                String flatPricede = packageObject.getFlatPrice();
                                                JSONArray jsOld = new JSONArray(flatPricede);
                                                for (int j = 0; j < jsOld.length(); j++) {
                                                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                    Iterator<String> keys = jsonexists1.keys();
                                                    if (keys.hasNext()) {
                                                        String key = (String) keys.next();
                                                        String[] keyArr = key.split(":");
                                                        String value = jsonexists1.getString(key);
                                                        String[] valueArr = value.split(":");
                                        %>
                                    <tr>
                                        <td><%=keyArr[0]%></td>
                                        <td></td>
                                        <td>Flat Price</td>
                                        <td></td>
                                        <td>USD <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                        <td>USD <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                    </tr>        
                                    <%
                                                    totalAmount = totalAmount + Double.parseDouble(valueArr[1]);

                                                }
                                            }
                                        }

                                        for (Map.Entry<String, Double> entry : amount.entrySet()) {
                                    %>                                                                  
                                    <tr>
                                        <!--<td colspan="2"></td>-->
                                        <td><%=entry.getKey()%></td>
                                        <td></td>
                                        <td>USD <%=df.format(entry.getValue())%></td>
                                        <td>USD <%=df.format(entry.getValue())%></td>
                                    </tr>
                                    <%      count++;
                                            totalAmount = totalAmount + entry.getValue();
                                        }
                                        totalAmountWithoutTax = totalAmount;
                                        String taxRate = packageObject.getTax();
                                        session.setAttribute("totalPaymentAmountWithoutTax", totalAmountWithoutTax);
                                        taxMap = TaxCalculationManagement.calculateTax(taxRate, totalAmountWithoutTax);
                                        if (!taxMap.isEmpty()) {
                                            for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                                if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                                                    String[] taxArr = entry.getValue().split(":");

                                    %>
                                    <tr>
                                        <!--<td colspan="2"></td>-->
                                        <td><%=entry.getKey()%></td>
                                        <td><%=taxArr[0]%> %</td>
                                        <td>USD <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                        <td>USD <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                    </tr>
                                    <%
                                                    count++;
                                                    totalAmount = totalAmount + Float.parseFloat(taxArr[1]);
                                                }
                                            }
                                        }
                                    %>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td style="text-align: right"><strong>TOTAL</strong></td>
                                        <td id="totalAmount">USD <%=df.format(totalAmount)%></td>
                                    </tr>
                                    <%
                                        count++;
//                                        String tmWellFormAmount = form.format(totalAmount);
//                                        String wellFormTotalBeforeTax = form.format(totalAmountWithoutTax);
                                        //session.setAttribute("totalPaymentAmountWithTax", tmWellFormAmount);
                                        session.setAttribute("packageNameStripe", packageName);
                                        session.setAttribute("_grossAmount", tmWellFormAmount);
                                        stripeAmount = tmWellFormAmount.replace(".", "");
                                        
                                    %>
                                    <tr>
                                        <td colspan="6">
                                            <h4>PRIVACY STATEMENT</h4>
                                            <p>PENYATAAN PRIVASI Blue Bricks Dalam usaha memastikan pematuhan kepada Akta Perlindungan Data Peribadi 2010 (APDP), Blue Bricks telah mewujudkan satu dasar perlindungan data peribadi yang akan mengawal penggunaan dan perlindungan data peribadi anda sebagai pelanggan Blue Bricks. Untuk mengetahui dasar tersebut secara terperinci, sila rujuk Penyataan Privasi Blue Bricks di http:///www.blue-bricks.com, yang mana tertakluk kepada perubahan dari masa ke semasa oleh Blue Bricks. 
                                                <br><br>
                                                Blue Bricks'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), Blue Bricks has put in place a personal data protection policy which shall govern the use and protection of your personal data as Blue Bricks's customer. For details of the policy, please refer to Blue Bricks's Privacy Statement at http://www.blue-bricks.com, which may be reviewed by Blue Bricks from time to time.
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" id="countDetails" name="countDetails" value="<%=count - 1%>">
                                <input type="hidden" id="appliedPromocode" name="appliedPromocode" value="no">                        
                                <input type="hidden" id="finalPaymentAmount" name="finalPaymentAmount" value="<%=tmWellFormAmount%>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form class="form-horizontal" id="paymentDetailsForm"> 
                <input type="hidden" id="totalBenefitAmount" name="totalBenefitAmount" value="<%=totalAmount%>">
                <input type="hidden" id="_promocodeId" name="_promocodeId">
                <input type="hidden" id="_packageName" name="_packageName" value="<%=outPackageName%>">
                <input type="hidden" id="originalPackageN" name="originalPackageN" value="<%=packageName%>">
                <input type="hidden" id="_invoiceId" name="_invoiceId" value="<%=invoiceid%>">
                <input type="hidden" id="_paymentMode" name="_paymentMode" value="<%=_paymentMode%>">
                <input type="hidden" id="_totalAmountWithoutTax" name="_totalAmountWithoutTax" value="<%=wellFormTotalBeforeTax%>">
                <input type="hidden" id="_gstTax" name="_gstTax" value="<%=gstTax%>">
                <input type="hidden" id="_vatTax" name="_vatTax" value="<%=vatTax%>">
                <input type="hidden" id="_serviceTax" name="_serviceTax" value="<%=serviceTax%>">            
                <input type="hidden" id="MERCHANTID" name="MERCHANTID" value="<%=tmMerchantID%>">
                <input type="hidden" id="PSWD" name="PSWD" value="<%=tmMerchantPWD%>">
                <input type="hidden" id="MERCHANT_TRANID" name="MERCHANT_TRANID" value="<%=tmMerchant_TranID%>">
                <input type="hidden" id="AMOUNT" name="AMOUNT" value="<%=tmWellFormAmount%>">
                <input type="hidden" id="SIGNATURE" name="SIGNATURE" >
                <%if (changeOnPackageCharge != null) {%>
                <input type="hidden" id="_changeOnPackageCharge" name="_changeOnPackageCharge" value="<%=changeOnPackageCharge%>">
                <%}
                    if (reActivationCharge != null) {%>       
                <input type="hidden" id="_reactivationCharge" name="_reactivationCharge" value="<%=reActivationCharge%>">
                <%}
                    if (cancellationCharge != null) {%>        
                <input type="hidden" id="_cancellationCharge" name="_cancellationCharge" value="<%=cancellationCharge%>">
                <%}
                    if (latePenaltyCharge != null) {%>
                <input type="hidden" id="_latePenaltyCharge" name="_latePenaltyCharge" value="<%=latePenaltyCharge%>">
                <%}%>
            </form>                           
            <!-- /.container -->
            <div class="container">
                <hr>
                <!-- Footer -->
                <footer>
                    <div class="row">
                        <div class="col-lg-12">
                            <p><strong>Blue Bricks Pty. Ltd.</strong><br/>
                                Unit 39, 118 Adderton Road, Carlingford 2118 NSW, Australia<br/>                                
                                WWW.blue-bricks.com</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- /.container -->    
        <!-- Bootstrap Core JavaScript -->     
       
    </body>
</html>
