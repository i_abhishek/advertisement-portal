<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">
	
	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Token</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Token Management
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>
	
	<div class="normalheader transition animated fadeIn">
		<div class="hpanel">	
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#test"> Test Environment</a></li>
				<li class=""><a data-toggle="tab" href="#live"> Live Environment</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="test" class="tab-pane active">
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-2">
								<button class="btn btn-xs btn-primary " type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-primary2 " type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-info " type="button"><i class="fa fa-eye"></i> <span class="bold">View Token</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-warning " type="button"><i class="fa fa-refresh"></i> <span class="bold">Regenerate Token</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-success " type="button"><i class="fa fa-envelope"></i> <span class="bold">Send Token (Email)</span></button>
							</div>
							<div class="col-sm-2">
							</div>
						</div>
					</div>
				</div>
				<div id="live" class="tab-pane">
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-2">
								<button class="btn btn-xs btn-primary " type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-primary2 " type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-info " type="button"><i class="fa fa-eye"></i> <span class="bold">View Token</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-warning " type="button"><i class="fa fa-refresh"></i> <span class="bold">Regenerate Token</span></button>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-xs btn-success " type="button"><i class="fa fa-envelope"></i> <span class="bold">Send Token (Email)</span></button>
							</div>
							<div class="col-sm-2">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">

            <div class="text-center m-b-xl">
                <h3>APIs Token Management</h3>
            </div>

            <hr class="m-b-xl"/>

            <div class="row">
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">SMS</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-phone big-icon text-warning"></i>
							<h4 class="font-bold">
                            </h4>
                            <p class="text-muted">
                            </p>
                            <a href="token_manager_api.jsp" class="btn btn-warning btn-sm">View Token</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Map</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-map-marker big-icon text-success"></i>
                            <h4 class="font-bold">
                            </h4>
                            <p class="text-muted">
                            </p>
                            <a href="#" class="btn btn-success btn-sm">View Token</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">MFA</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-key big-icon text-info"></i>
                            <h4 class="font-bold">
                            </h4>
                            <p class="text-muted">
                            </p>
                            <a href="#" class="btn btn-info btn-sm">View Token</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Digital Signing</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-note big-icon text-danger"></i>
                            <h4 class="font-bold">
                            </h4>
                            <p class="text-muted">
                            </p>
                            <a href="#" class="btn btn-danger btn-sm">View Token</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</div>
	
    <!-- Footer-->
    <%@include file="footer.jsp"%>