<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Video Gallery
            </h2>
            <small>Learn with video tutorials </small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">
                <div class="panel-body">
                    <p>
                        <strong>Video Gallery,</strong>  within our gallery you will find Dashboard Tutorials and Service Tutorials. Learn More about Ready APIs from here.
                    </p>
                    <br>
                    <ul class="nav nav-tabs">                        
                        <li class="active"><a data-toggle="tab" href="#tab-2"> Dashboard</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Services</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active"> 
                            <br>
                            <div class="main-container-wide">
                                <div class="&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;main-container-wrapper&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;">
                                    <div class="row" id="content">
                                        <div class="main-content" role="main">			

                                            <div class="row">				
                                                <div class="col-sm-8 col-sm-offset-2">					
                                                    <div class="lightbox-gallery dark mrb35">
                                                        <ul id="videos-without-poster" class="list-unstyled row">                              
                                                            
                                                            <li class="video" data-src="https://www.youtube.com/watch?v=ZIMbwhKZ7cs" data-sub-html="Dashboard">
                                                                <a href="" data-toggle="tooltip" data-placement="right" title="This video gives the details about dashboard screen with explanation of each menu.">
                                                                    <img class="img-responsive" src="https://img.youtube.com/vi/ZIMbwhKZ7cs/default.jpg" alt="Dashboard">
                                                                    <p style="float: left;"><b>1) Dashboard </b><span >test area</span></p>
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="images/video/play-button.png">
                                                                    </div>
                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=2X6XOykpFJY" data-sub-html="Change Password and My Profile">
                                                                <a href="" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                                                    <img class="img-responsive" src="images/video/chnagePaddword.jpg" alt="Change Password and My Profile">
                                                                    <p style="float: left;"><b>2) Change Password & Profile</b></p>                                                                    
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="images/video/play-button.png">                                                                        
                                                                    </div>
                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Curabitur non placerat lorem">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/166b6e0e-156b-4e99-8409-fad627ee398a.jpg" alt="Curabitur non placerat lorem">
                                                                    <p style="float: left;"><b>4) Google 2FA Token</b></p>                                                                                                                                        
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="images/video/play-button.png">
                                                                    </div>
                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/09194722-27aa-4ea1-b6c4-c84cfeae6641.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>4) Image Utility</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>4) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/ecaa08f9-b30e-4102-9f50-ae53bf539809.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>5) Tokenization</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>5) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9ec3b7e1-9f72-42c1-a8de-ec8a87721979.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>6) Value Added Service</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>6) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/e0ab68dc-f0a0-4b14-8bb5-9e82eba3fec1.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">

                                                                    <p style="float: left;"><b>7) IP2GEO</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>7) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="A New Zealand Working Dog">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/00a3c536-1152-4908-9418-490caf5e95e8.jpg" alt="A New Zealand Working Dog">
                                                                    <p style="float: left;"><b>8)Instant WebChat</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>8) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>
                                                        </ul>
                                                        
                                                        <table id="videos-without-poster">
                                                            <tr>
                                                                <td class="video" data-src="https://www.youtube.com/watch?v=ZIMbwhKZ7cs" data-sub-html="Dashboard">
                                                                    <a href="" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                                                        <img class="img-responsive" src="images/video/chnagePaddword.jpg" alt="Change Password and My Profile">
                                                                        <p style="float: left;"><b>2) Change Password & Profile</b></p>                                                                    
                                                                        <div class="demo-gallery-poster">
                                                                            <img src="images/video/play-button.png">                                                                        
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </div>				
                                                </div>			
                                            </div>						                  			
                                            <hr>			                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="main-container-wide">
                                <div class="&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;main-container-wrapper&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;">
                                    <div class="row" id="content">
                                        <div class="main-content" role="main">			
                                            <div class="row">				
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <div class="lightbox-gallery dark mrb35">
                                                    <ul id="videos-without-poster" class="list-unstyled row">                              
                                                            <li class="video" data-src="https://www.youtube.com/watch?v=CZd6jWdm6eU" data-sub-html="Good Girls become Guide Dogs">
                                                                <a href="" data-toggle="tooltip" data-placement="right" title="By using DB As Service we can expose our database like oracle, mysql and cassandra . User can fired various queries on database as per the requirement and easily access the database. We can also save our database connection using this services.">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9a655a8d-622a-4a43-849d-79de45846ce3.jpg" alt="Good Girls become Guide Dogs">


                                                                    <p style="float: left;"><b>2)  DB As Service</b></p>
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>
                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=dQw4w9WgXcQ" data-sub-html="Red Hat Club">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/25df9b02-3c31-4e27-80a2-948cbe043a5d.jpg" alt="Red Hat Club">

                                                                    <p style="float: left;"><b>2) Document Utility</b></p>
                                                                    <!--                                      
                                                                                                        <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>2) Document Utility (Document Signing)</b><br> 
                                                                                                                        <p>
                                                                                                                            Service that lets <br>us sign documents <br>digitally without printing,<br> scanning, or faxing.<br> It's a win for <br>both business and personal <br>users because electronic <br>signing is fast, easy,<br> and secure. Electronic <br>signatures are trusted<br> by millions around <br>the world and are <br>available in dozens <br>of languages. Send<br> and sign anywhere, anytime,<br> on any Internet-enabled <br>device. Contracts, agreements,<br> loans, leases, all <br>can be signed quickly and securely.
                                                                                                                        </p> </span>-->
                                                                    <!--                                        <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>2) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Curabitur non placerat lorem">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/166b6e0e-156b-4e99-8409-fad627ee398a.jpg" alt="Curabitur non placerat lorem">

                                                                    <p style="float: left;"><b>4) Google 2FA Token</b></p>
                                                                    <!--                                      The token is a reference (i.e. identifier) that maps back <br>to the sensitive data through a tokenisation system. <br>With tokens you do not need to share confidential<br> data between various system.<br> You can enjoy complete end to end token<br> lifecycle through tokenization.-->
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>3) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/09194722-27aa-4ea1-b6c4-c84cfeae6641.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>4) Image Utility</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>4) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/ecaa08f9-b30e-4102-9f50-ae53bf539809.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>5) Tokenization</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>5) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=K1_JWrl3_NQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/9ec3b7e1-9f72-42c1-a8de-ec8a87721979.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                    <p style="float: left;"><b>6) Value Added Service</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>6) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=PrQb19l7jOQ" data-sub-html="Lorem Ipsum Dolor Sit Amet Consecteteur">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/e0ab68dc-f0a0-4b14-8bb5-9e82eba3fec1.jpg" alt="Lorem Ipsum Dolor Sit Amet Consecteteur">

                                                                    <p style="float: left;"><b>7) IP2GEO</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>7) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>

                                                            <li class="video" data-src="https://www.youtube.com/watch?v=fDq3TK8id6w" data-sub-html="A New Zealand Working Dog">
                                                                <a href="">
                                                                    <img class="img-responsive" src="https://templates.usu.edu/ldp/galleries/.private_ldp/a104300/production/thumb/00a3c536-1152-4908-9418-490caf5e95e8.jpg" alt="A New Zealand Working Dog">
                                                                    <p style="float: left;"><b>8)Instant WebChat</b></p>
                                                                    <!--                                      <span style="text-align: center!important;vertical-align: middle!important;float: left;"><b>8) Document Utility (Document Signing)  </b><br> </span>                -->
                                                                    <div class="demo-gallery-poster">
                                                                        <img src="https://ouresources.usu.edu/_assets/galleries/light-gallery/img/play-button.png">
                                                                    </div>

                                                                </a>
                                                            </li>
                                                        </ul>
                                                </div>                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="scripts/video/fileinput.min.js" type="text/javascript"></script>
        <script src="scripts/video/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="scripts/video/accordionSnippet.js" type="text/javascript"></script>
        <script src="scripts/video/tabSnippet.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('.selectpicker').selectpicker();
            });
        </script>
        <script src="scripts/video/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(".form_datetime").datetimepicker({
                format: 'mm-dd-yyyy H:ii P',
                autoclose: 1,
                showMeridian: 1
            });
            $(".form_date").datetimepicker({
                format: 'mm-dd-yyyy',
                autoclose: 1,
                showMeridian: 1,
                minView: 2,
                forceParse: 0
            });
            $(".form_time").datetimepicker({
                format: 'H:ii P',
                autoclose: 1,
                showMeridian: 1,
                startView: 1,
                minView: 0,
                maxView: 1,
                forceParse: 0
            });
        </script>
        <script src="scripts/video/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

        <script src="scripts/video/picturefill.min.js" type="text/javascript"></script>
        <script src="scripts/video/lightgallery.js" type="text/javascript"></script>
        <script src="scripts/video/lg-fullscreen.js" type="text/javascript"></script>
        <script src="scripts/video/lg-thumbnail.js" type="text/javascript"></script>
        <script src="scripts/video/lg-video.js" type="text/javascript"></script>
        <script src="scripts/video/lg-autoplay.js" type="text/javascript"></script>
        <script src="scripts/video/demos.js" type="text/javascript"></script>
        <script src="scripts/video/lg-zoom.js" type="text/javascript"></script>
        <script src="scripts/video/lg-hash.js" type="text/javascript"></script>
        <script src="scripts/video/lg-pager.js" type="text/javascript"></script>
        <script src="scripts/video/jquery.mousewheel.min.js" type="text/javascript"></script>
        <link href="scripts/video/lightgallery.css" rel="stylesheet" type="text/css"/>
        <link href="scripts/video/videogallery.css" rel="stylesheet" type="text/css"/>
