
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-6 tour-21">				
                <div class="hpanel">		
                    <div class="panel-heading">
                        <h3>Your request</h3>
                    </div>				
                    <div class="panel-body">					                                        
                        <form  role="form" class="form-horizontal" id="getBody" name="getBody">
                            <h3>Header</h3>
                            <table id="_headerTable" CELLSPACING="15" class="tour-23">                                                
                                <tr>                                                    
                                    <td width="35%">
                                        <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="APITokenId">                                                        
                                    </td>
                                    <td width="90%">
                                        <div style='padding: 1%'>
                                            <input type="text" class="form-control" id="_myResType" name="_myResType" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">
                                        <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerId">                                                        
                                    </td>
                                    <td width="90%">
                                        <div style='padding: 1%'>
                                            <input type="text" class="form-control" id="_myResType" name="_myResType" value="XteIg8dYAYwRiHGz0A300Lwog+w=">                                                        
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">
                                        <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerTokenId">                                                        
                                    </td>
                                    <td width="90%">
                                        <div style='padding: 1%'>
                                            <input type="text" class="form-control" id="_myResType" name="_myResType" value="wkV52UiG64dweuOFX+5UoJXNfG4=">                                                        
                                        </div>
                                    </td>
                                    <td >                                                        
                                        <a class='btn btn-info btn-sm'  onclick="addHeader()" id="addUserButton"><span class='fa fa-plus-circle'></span></a>
                                    </td>  
                                    <td style="padding: 1%">
                                        <a class='btn btn-warning btn-sm' onclick="removeHeader()" id="minusUserButton"><span class='fa fa-minus-circle'></span></a>
                                    </td>                                              
                                </tr>                                                   
                            </table>
                            <div class="hr-line-dashed"></div>
                            <div style="display: none">
                                <h3>Basic Authentication</h3>
                                <div class="form-group">
                                    <div class="col-sm-6"><input type="text" id="_userNameAuth" name="_userNameAuth"  class="form-control" placeholder="Username"></div>
                                    <div class="col-sm-6"><input type="text" id="_passwordAuth" name="_passwordAuth" class="form-control" placeholder="Password"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div class="tour-24">
                                <h3>Body</h3>
                                <textarea class="form-control" id="_APIbody" name="_APIbody" style="min-height: 500px;">
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.wsdl.wadl.sg.mollatech.com/">
        <soapenv:Header/>
        <soapenv:Body>
            <web:createChatRoom>
                <!--Optional:-->
                <developerEmail>abhishek@mollatech.com</developerEmail>
                <!--Optional:-->
                <numberOfUsers>2</numberOfUsers>
                <!--Optional:-->
                <groupName>Buzz</groupName>
                <!--Zero or more repetitions:-->
                <userEmailIds>jerry@noemail.com</userEmailIds>
                <userEmailIds>tom@noemail.com</userEmailIds>
                <!--Optional:-->
                <urlexpiryTimeInMinutes>15</urlexpiryTimeInMinutes>
            </web:createChatRoom>
        </soapenv:Body>
    </soapenv:Envelope>
                                </textarea>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <button class="btn btn-primary ladda-button" data-style="zoom-in" onclick="SendRequest()" id="apiConsoleRequest" type="button"><i class="fa fa-terminal"></i> Submit</button>                                    
                        </form>								
                    </div>					
                </div>				
            </div>
            <div class="col-lg-6 tour-22">
                <div class="hpanel">
                    <div class="panel-heading">
                        <h3>Your response</h3>
                    </div>
                    <div class="panel-body">
                        <form>
                            <textarea style="min-height: 220px;" class="form-control" id="responseID" name="responseID">
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createChatRoomResponse xmlns:ns2="http://webservice.wsdl.wadl.sg.mollatech.com/" xmlns:ns3="http://com.mollatech.sg.sgholder">
            <return>
                <chatURL>https://www.readyapis.com:9443/WebChat/index.jsp?3a4e8f81-f33d-4c81-8b04-54a162de020c</chatURL>
                <resultCode>0</resultCode>
                <result>3a4e8f81-f33d-4c81-8b04-54a162de020c Chat Room Is Created Successfully.</result>
                <password>55f0a7</password>
            </return>
        </ns2:createChatRoomResponse>
    </S:Body>
</S:Envelope>
                            </textarea>
                        </form>
                        <br>
                        <div id="startValuesAndTargetExample">
                            <p class="text-info"><i class="fa fa-check-square"></i> Congratulations, Your API takes  00:06 sec to get response. </p>
                        </div>
                    </div>                            
                </div>
                <div id="questionAndAnswer">
                    <div class="hpanel panel-group tour-25" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel-body">
                            <h4 class="m-t-none m-b-none">Need Help </h4>
                            <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                            <a  class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button" href="#" onclick="apiHelpDesk()" style="margin-left: 1px"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                        </div>
                        <div class="panel-body" id="_apiLevelToken" >
                            <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                Where can I get APITokenId?
                            </a>
                            <div id="q1" class="panel-collapse collapse">
                                <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                            </div>
                        </div>                                                               <!--
                        -->                                <div class="panel-body" id="_headerError">
                            <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                Why need Headers and How to add?
                            </a>
                            <div id="q2" class="panel-collapse collapse">
                                <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                            </div>
                        </div><!--
                        -->                             <div class="panel-body" id="connectionrefuse" style="display: none">
                            <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                Connection refused (Connection refused)?
                            </a>
                            <div id="q3" class="panel-collapse collapse">
                                <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                            </div>
                        </div>                                               
                        <div class="panel-body">
                            <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                No Such Webservice is Available.
                            </a>
                            <div id="q5" class="panel-collapse collapse">
                                <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                            </div>
                        </div>                                                                                                                         
                        <div class="panel-body">
                            <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                Service Guard Error: Please Contact Administrator.
                            </a>
                            <div id="q12" class="panel-collapse collapse">
                                <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                            </div>                                
                        </div>                     
                    </div>
                </div>
            </div>             
        </div>
    </div>    
<script>
    var doc = document.getElementById('_APIbody');
    var dataCode = CodeMirror.fromTextArea(doc, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
    dataCode.on('change', function ash() {
        $('#_APIbody').val(dataCode.getValue());
    });

    var doc = document.getElementById('responseID');
    var dataCode = CodeMirror.fromTextArea(doc, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
    dataCode.on('change', function ash() {
        $('#responseID').val(dataCode.getValue());
    });
</script>