
<style>
.input-color {
    position: relative;
}
.input-color input {
    padding-left: 20px;
}
.input-color .color-box {
    width: 10px;
    height: 10px;
    display: inline-block;
    background-color: #ccc;
    position: absolute;
    left: 5px;
    top: 5px;
    
}
</style>
<% 
    String type = request.getParameter("type");
    int ity = 0;
    if(type != null){
        ity = Integer.parseInt(type);
    }
%>
<div class="tab-pane active" id="usercharts">
    <div class="row-fluid">
        <div class="span12">                       
            <div  class="span12" style="alignment-adjust: central">
                
                <%if(ity == 2){%>
                <h5 align="center" class="text-primary" style="font-weight: bold">API TRANSCATION REPORT</h5>
                <font style="font-family: monospace">
                API Transcation count
                </font>
                <%}else{%>
                <h5 align="center" class="text-primary" style="font-weight: bold">API CALL USAGE REPORT</h5>
                <font style="font-family: monospace">
                API USAGE count
                </font>
                <%}%>
                <div>
                    <canvas id="linegraph"></canvas>
                </div>         
                <div  align="center" style="font-family: monospace" >
                    Day
                </div>    
                    <%if(ity == 2){%>
                    <ul>
                        <li>
                            <div class="input-color">
                                <input type="text" value="Transcation Success" disabled style="font-family: monospace"/>
                                <div align="center" class="color-box" style="background-color: green"></div>                                
                            </div>
                        </li>
                        <li>
                            <div class="input-color">
                                <input type="text" value="Transcation Failed" disabled style="font-family: monospace"/>
                                <div align="center" class="color-box" style="background-color: red;"></div>                                
                            </div>
                        </li>
                    </ul>
                    <%}%>
                
            </div>                        
        </div>
    </div>
    <div class="tab-pane" id="userreport">            
    </div>
</div>    
<br>
<br>                    
