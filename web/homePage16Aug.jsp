<%@include file="header.jsp"%>
<!-- Main Wrapper -->
<body>
<div id="wrapper">
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12 text-center m-t-md">                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel stats">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Today's total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-share fa-4x"></i>
                        <h1 class="m-xs text-success" id="todayUcredit"></h1>
                        <small id="todayMsg"></small>
                        <br>
                        <h3 class="font-extra-bold no-margins text-success">
                            Today expense
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last week total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-graph1 fa-4x"></i>

                        <h1 class="m-xs text-success" id="lastWeekAmount"></h1>
                        <small id="weekMsg"></small>
                        <br>
                        <h3 class="font-extra-bold no-margins text-success">
                            Weekly expense
                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-3" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last month total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-global fa-4x"></i>
                        <h1 class="m-xs text-success" id="monthCredit"></h1>
                        <small id="monthMsg"></small>
                        <h3 class="font-extra-bold no-margins text-success row">
                            Monthly usage
                        </h3>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3" id="leastUsedService" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="leastusedTitle"></span>
                        
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart m-t-lg" style="height: 120px">
                            <div id="flot-line-chart" style="height: 120px"></div>
                        </div>
                    </div>
                </div>
            </div>
            
<!--            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Your subscribed package
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-photo-gallery fa-4x"></i>
                        <h1 class="m-xs text-success" id="_packamount"></h1>
                        <small id="subMsg"></small>
                        <h1 class="font-bold no-margins text-success row" id="subscribePackage"></h1>
                    </div>
                </div>
            </div>-->
            
        </div>
        <div class="row">
            <div class="col-lg-9" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Dashboard Information And Statistics
                    </div>
                    <div class="panel-body">
                        <div class="text-left">
                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                        </div>
                        <div data-toggle="buttons" class="btn-group">
                            <div style="padding-left: 40%">
                                <button  class="btn btn-success ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top five service</button>
                                <button  class="btn btn-success ladda-button" data-style="zoom-in" onclick="creditperSer()">Today total expenditure</button>
                                <button  class="btn btn-success ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Todays top five trending services</button>
                                </div>
                                </div>
                         <div  id="topFiveSer">
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="mostlyFive"></span>
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart m-t-lg" style="height: 155px">
                            <div class="flot-chart-content" id="flot-pie-chart" style="height: 120px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3" id = "mostlyservices" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Your mostly used services
                    </div>
                    <div class="panel-body text-center h-200">
                        <img src="images/oops.jpg" alt="Smiley face" width="170" height="150"><br>
                        <small>Sorry you didn't used any service from last month.</small>
                    </div>
                </div>
            </div>
        </div>
<%@include file="footer.jsp"%>
    </div>
</div>
<script type="text/javascript">
   
document.onreadystatechange = function(){
     if(document.readyState === 'complete'){
        $('#todayUcredit').ready(function() {
        todayusedCredit("today");
        }); 
        $('#lastWeekAmount').ready(function() {
        weeklyusedCredit("lastweek");
        });
        $('#monthCredit').ready(function() {
        monthlyCredit("month");
        });
        $('#flot-line-chart').ready(function() {
        leastUsedService();
        });
         $('#flot-pie-chart').ready(function() {
        yourMostlyService();
        });
//        $('#ct-chart4').ready(function() {
//        toptrndingServices();
//        });
//        $('#subscribePackage').ready(function() {
//        devSubscribePack();
//        });
        $('#homeReportLable').ready(function() {
        generateTopService();
        });
        $('#topFiveSer').ready(function() {
        creditperSer();
        });
    }
    };
</script>
</body>