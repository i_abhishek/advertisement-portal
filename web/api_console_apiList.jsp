<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethodParams"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.HttpResorcemanagment"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.HttpresourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="org.json.JSONObject"%>
<%

    String reqData = request.getParameter("methodName");
    String[] reqArr = reqData.split(":");
    String _resId = request.getParameter("_resId");
    int resId = Integer.parseInt(reqArr[1]);
    //String value = request.getParameter("value");
    String _apid = request.getParameter("_apid");
    String envmt = request.getParameter("envt");
    int apId = Integer.parseInt(reqArr[0]);
    String mName = request.getParameter("passedAPI");

    String requestType = request.getParameter("type");
    String restType = request.getParameter("restType");

    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apId);
    ResourceDetails rs = new ResourceManagement().getResourceById(resId);
    String resourceName = rs.getName();
    String apName = ap.getName();
    int accesspointId = ap.getApId();
    int resourceId = rs.getResourceId();
    Map classMap = null;
    Map methodMap = null;
    String value = apName;
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apId, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());

    byte[] me = tf.getMethods();
    byte[] cl = tf.getClasses();

    if (classMap == null) {
        classMap = new HashMap();
    }
    if (methodMap == null) {
        methodMap = new HashMap();
    }
    int version = 1;
    if (warFiles != null) {
        Map warMap = (Map) (Serializer.deserialize(warFiles.getWfile()));
        version = warMap.size() / 2;
    }
    Object obj = Serializer.deserialize(me);
    Map tmpMethods = (Map) obj;
    Methods methods = (Methods) tmpMethods.get("" + version);
    Object objC = Serializer.deserialize(cl);
    Map tempClass = (Map) objC;
    Classes classes = (Classes) tempClass.get("" + version);
    classMap.put(value, classes);
    methodMap.put(value, methods);
%>&nbsp; API 
<select class="js-source-states" style="width: 200px" id="methodName" name="methodName" onchange="getRequestType()">
    <optgroup label="Select API">
        <%
            String apiName = "";
            List list = methods.methodClassName.methodNames;
            for (int i = 0; i < list.size(); i++) {
                MethodName methodName = (MethodName) list.get(i);
                if (methodName.visibility.equalsIgnoreCase("yes")) {
                    apiName = methodName.methodname.split("::")[0];
                    if (!methodName.transformedname.equals("")) {
                        apiName = methodName.transformedname.split("::")[0];
                    }
                    if (apiName.equalsIgnoreCase(mName)) {%>
        <option value="<%=apiName%>" selected><%=apiName%></option>                            
        <%} else {
        %>                                                    
        <option value="<%=apiName%>"><%=apiName%></option>                            
        <%}
                }
            }
        %>
    </optgroup>
</select>
<script>

    //getAPIConsoleWindow(apiMethodName);
</script>        