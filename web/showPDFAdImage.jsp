

<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%
    String id = request.getParameter("_id");
    SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(Integer.parseInt(id));
%>


<div id="rejectPackage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="rejectPackageModal">Reject package</b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPackageForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">                                    
                                    <image src="data:image/jpg;base64,<%=adDetails.getPdfAdImage()%>" alt="PDF Ad Image" width="80" height="80"> 
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>
