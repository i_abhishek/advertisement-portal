<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
    
<script src="scripts/homepage.js" type="text/javascript"></script>
<%@include file="header.jsp"%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12 text-center m-t-md">                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="hpanel stats">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Today's total credit used
                    </div>
                    <div class="panel-body h-200">
                        <div class="stats-title pull-left">
                            <h4>Users Activity</h4>
                        </div>
                        <div class="stats-icon pull-right">
                            <i class="pe-7s-share fa-4x"></i>
                        </div>
                        <div class="m-t-xl">
                            <h3 style="font-weight: 500" class="m-b-xs text-center" id="todayUcredit"></h3><br>
                            <div class="text-center">
                                <small id="todayMsg"></small>
                                <h3 class="font-extra-bold no-margins text-success">
                                    Today expense
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last week total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-graph1 fa-4x"></i>

                        <h1 class="m-xs" id="lastWeekAmount"></h1>
                        <small id="weekMsg"></small>
                        <br>
                        <h3 class="font-extra-bold no-margins text-success">
                            Weekly expense
                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last month total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-global fa-4x"></i>
                        <h1 class="m-xs text-success" id="monthCredit"></h1>
                        <small id="monthMsg"></small>
                        <h3 class="font-bold no-margins text-success row">
                            Monthly usage
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="leastUsedService">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="leastusedTitle"></span>
                        
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart m-t-lg" style="height: 120px">
                            <div class="flot-chart-content" id="flot-line-chart" style="height: 120px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Todays Top Trending Services
                    </div>
                    <div class="panel-body">
                        <div>
                            <div style="width: 450; height: 150" id="ct-chart4"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3" id="topmostServices">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="mostlyFive"></span>
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart m-t-lg" style="height: 120px">
                            <div class="flot-chart-content" id="flot-pie-chart" style="height: 120px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last month total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-global fa-4x"></i>
                        <h1 class="m-xs text-success" id="subscribePackage"></h1>
                        <small id="subMsg"></small>
                        <h3 class="font-bold no-margins text-success row">
                            Your Package
                        </h3>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Dashboard Information And Statistics
                    </div>
                    <div class="panel-body">
                        <div class="text-left">
                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable"> Top five services</span>
                        </div>
                        <div id="report_data_button">
                            <div style="padding-left: 75%">
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top five service</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today total expenditure</button>
                            </div>
                        </div>
                        <hr class="m-b-xl"/>
                         
                         <div  id="topFiveSer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
<%@include file="footer.jsp"%>
    </div>
</div>
<script type="text/javascript">
   
document.onreadystatechange = function(){
     if(document.readyState === 'complete'){
        $('#todayUcredit').ready(function() {
        todayusedCredit("today");
        }); 
        $('#lastWeekAmount').ready(function() {
        weeklyusedCredit("lastweek");
        });
        $('#monthCredit').ready(function() {
        monthlyCredit("month");
        });
        $('#flot-line-chart').ready(function() {
        leastUsedService();
        });
         $('#flot-pie-chart').ready(function() {
        yourMostlyService();
        });
        $('#ct-chart4').ready(function() {
        toptrndingServices();
        });
        $('#subscribePackage').ready(function() {
        devSubscribePack();
        });
        $('#homeReportLable').ready(function() {
        generateTopService();
        });
        $('#topFiveSer').ready(function() {
        creditperSer();
        });
    }
    };
</script>