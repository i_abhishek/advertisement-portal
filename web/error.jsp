<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ include file="incl.header.jsp" %>

		<div class="container-fluid">

            <div class="row">

                <div class="col-sm-4 colCentered" >
                	<br/>
					<br/>
		   

    <% String error = (String)request.getAttribute("error"); %>
    
    <% if (! (error == null || error.isEmpty()) ) { %> 
					<div class="alert alert-danger">
						<b>Error:</b> <%= error %>
					</div>
		
    <% } %>	

	
	
    <% String message = request.getParameter("message"); %>
    
    <% if (! (message == null || message.isEmpty()) ) { %> 
					<div class="alert alert-danger">
						<b>Error:</b> <%= message %>
					</div>
    <% } %>	
	
	                <br/>
					<br/>
	
					<h3>Return to <a href='home.jsp'>Home Page</a></h3>
					
  				</div>
  			</div>
		</div>
		
<%@ include file="incl.footer.jsp" %>