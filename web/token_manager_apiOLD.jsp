<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
						<li><a href="token_manager.jsp">Token Management</a></li>
                        <li class="active">
                            <span>SMS</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Token Management - SMS
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    Access Points 
                </div>
                <div class="panel-body">
				<div class="col-sm-12">
					<div class="col-sm-3">
						<select class="form-control m-b" name="api">
							<option>SMS</option>
						</select>
					</div>
					<div class="col-sm-2">
						<select class="form-control m-b" name="ver">
							<option>1</option>
						</select>
					</div>
				</div>
                <table id="api" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
					<th>API Sequence</th>
                    <th>API Name</th>
                    <th></th>
					<th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
					<td>01</td>
                    <td>login</td>
					<td><a class="btn btn-warning btn-xs" type="button" href="#"><i class="fa fa-eye"></i> <span class="bold">View Token</span></a></td>
                    <td><a class="btn btn-success btn-xs" type="button" href="#"><i class="fa fa-refresh"></i> <span class="bold">Assign Token</span></a></td>
                    <td><a class="btn btn-info btn-xs" type="button" href="#"><i class="fa fa-envelope"></i> <span class="bold">Email Token</span></a></td>
                </tr>
                <tr>
					<td>02</td>
                    <td>sendmsg</td>
                    <td><a class="btn btn-warning btn-xs" type="button" href="#"><i class="fa fa-eye"></i> <span class="bold">View Token</span></a></td>
                    <td><a class="btn btn-success btn-xs" type="button" href="#"><i class="fa fa-refresh"></i> <span class="bold">Assign Token</span></a></td>
                    <td><a class="btn btn-info btn-xs" type="button" href="#"><i class="fa fa-envelope"></i> <span class="bold">Email Token</span></a></td>
                </tr>
                <tr>
					<td>03</td>
                    <td>delmsg</td>
                    <td><a class="btn btn-warning btn-xs" type="button" href="#"><i class="fa fa-eye"></i> <span class="bold">View Token</span></a></td>
                    <td><a class="btn btn-success btn-xs" type="button" href="#"><i class="fa fa-refresh"></i> <span class="bold">Assign Token</span></a></td>
                    <td><a class="btn btn-info btn-xs" type="button" href="#"><i class="fa fa-envelope"></i> <span class="bold">Email Token</span></a></td>
                </tr>
                </tbody>
                </table>

                </div>
            </div>
        </div>
		
    </div>

    <%@include file="footer.jsp"%> 


<script>

    $(function () {

        // Initialize Example 2
        $('#api').dataTable();

    });
	
	$(function () {

        // Initialize Example 2
        $('#data_type').dataTable();

    });

</script>