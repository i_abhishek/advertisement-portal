<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>TM API</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
    <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
    <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
    <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
	<link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
	<link rel="stylesheet" href="vendor/dropzone/dropzone.css">

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/static_custom.css">
    <link rel="stylesheet" href="styles/style.css">
	
	<!-- Need call first -->
	<script src="vendor/jquery/dist/jquery.min.js"></script>
	<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
	<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="vendor/summernote/dist/summernote.min.js"></script>

</head>
<body class="fixed-navbar fixed-sidebar">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
        <span>
            Developer Portal
        </span>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="small-logo">
            <span class="text-primary">HOMER APP</span>
        </div>
        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="" href="login.jsp">Login</a>
                    </li>
                    <li>
                        <a class="" href="login.jsp">Logout</a>
                    </li>
                    <li>
                        <a class="" href="profile.jsp">Profile</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav no-borders">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <i class="pe-7s-keypad" title="My Menu"></i>
                    </a>

                    <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                        <table>
                            <tbody>
                            <tr>
								<td>
                                    <a href="change_password.jsp">
                                        <i class="pe pe-7s-key text-info"></i>
                                        <h5>Change Password</h5>
                                    </a>
                                </td>
                                <td>
                                    <a href="my_profile.jsp">
                                        <i class="pe pe-7s-users text-success"></i>
                                        <h5>My Profile</h5>
                                    </a>
                                </td>
								<td>
                                    <a href="compose_email.jsp">
                                        <i class="pe pe-7s-mail text-warning"></i>
                                        <h5>Compose Email</h5>
                                    </a>
                                </td>                         
                            </tr>
                            <tr>
								<td>
                                    <a href="my_subscription_package.jsp">
                                        <i class="pe pe-7s-shopbag text-danger"></i>
                                        <h5>My Subscription</h5>
                                    </a>
                                </td> 
								<td>
                                    <a href="my_invoice.jsp">
                                        <i class="pe pe-7s-cash text-warning"></i>
                                        <h5>My Invoice</h5>
                                    </a>
                                </td> 
                                <td>
                                    <a href="https://developer.tm.com.my/APIDoc/apidocs.jsp" target="_blank">
                                        <i class="pe pe-7s-help2 text-info"></i>
                                        <h5>API User Guide</h5>
                                    </a>
                                </td>
                            </tr>
							<tr style="display: none">
								<td>
									<!-- Jack Remark: Temporary hide it as no need from now -->
                                    <a href="access_point_policy.jsp">
                                        <i class="pe pe-7s-speaker text-warning"></i>
                                        <h5>Access Point Policy</h5>
                                    </a>
                                </td>
							</tr>
                            </tbody>
                        </table>
                    </div>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                        <i class="pe-7s-mail"></i>
                        <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu hdropdown animated flipInX">
                        <div class="title">
                            You have 4 new messages
                        </div>
                        <li>
                            <a>
                                It is a long established.
                            </a>
                        </li>
                        <li>
                            <a>
                                There are many variations.
                            </a>
                        </li>
                        <li>
                            <a>
                                Lorem Ipsum is simply dummy.
                            </a>
                        </li>
                        <li>
                            <a>
                                Contrary to popular belief.
                            </a>
                        </li>
                        <li class="summary"><a href="#">See All Messages</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#">
                        <i class="pe-7s-upload pe-rotate-90" title="Logout"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="dashboard.jsp">
                <img src="images/profile.jpg" class="img-circle m-b" alt="logo">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase">Robert Razer</span>
            </div>
			
			<hr/>
			<div>
				<h4 class="font-extra-bold m-b-xs">
					RM 95.00
				</h4>
				<small class="text-muted">Balance Credit</small>
			</div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="active">
                <a href="dashboard.jsp"> <span class="nav-label">Dashboard</span> </a>
            </li>
			<li>
                <a href="token_manager.jsp"> <span class="nav-label">API Tokens</span></a> <!-- Jack Remark: Temporary hide it as no need from now -->
            </li>
            <li>
                <a href="api_console.jsp"> <span class="nav-label">API Console</span></a>
            </li>
            <li>
                <a href="api_usage.jsp"> <span class="nav-label">API Usage</span></a>
            </li>
            <li>
                <a href="#"><span class="nav-label">Reports</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="graphical_transaction_report.jsp">Transaction Report</a></li>
                    <li><a href="graphical_performance_report.jsp">Performance Report</a></li>
                </ul>
            </li>
			<li>
                <a href="compose_production.jsp"> <span class="nav-label">Subscription</span></a>
            </li>
			<li>
                <a href="my_invoice.jsp"> <span class="nav-label">My Invoice</span></a>
            </li>
			<li>
                <a href="virtual_number.jsp"> <span class="nav-label">Virtual Number</span></a>
            </li>
			<li>
                <a href="compose_email_technical.jsp"> <span class="nav-label">Helpdesk</span></a>
            </li>
        </ul>
    </div>
</aside>