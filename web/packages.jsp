<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>Ready APIs</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
        <script src="vendor/jquery/dist/jquery.min.js"></script>
         <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<!--<link href="styles/toggleButton/bootstrap-switch.css" rel="stylesheet" type="text/css"/>
        <script src="styles/toggleButton/bootstrap-switch.js" type="text/javascript"></script>-->
        
        <link href="styles/switchUserDefined.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/switchJquery.js" type="text/javascript"></script>
    </head>
    <body class="blank">
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="color-line"></div>

        <div class="row  animate-panel" data-effect="rotateInDownRight" data-child="element">
            <h3 style="text-align: center">Subscribe a package to continue with our services</h3>
            <div class="container">
                
                <%
                    SgReqbucketdetails[] packageObj = null; boolean packageFound=true;
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
                    //String basicMonth = null, basicYear = null, studentMonth = null, studentYear = null, standardMonth = null, standardYear = null, enterpriseMonth = null, enterpriseYear = null;
                    String basicMonth = null, basicYear = null, studentMonth = null, studentYear = null, standardMonth = null, standardYear = null, enterpriseMonth = null, enterpriseYear = null;
    float basicMAmount = 0;
    float basicYAmount = 0;
    float studentMAMount = 0;
    float studentYAmount = 0;
    float stdMAMount = 0;
    float stdYAmount = 0;
    float entMntAmount = 0;
    float entYAmount = 0;
    String strbasicMAmount = "0.00";
    String strbasicYAmount = "0.00";
    String strstudentMAMount = "0.00";
    String strstudentYAmount = "0.00";
    String strstdMAMount = "0.00";
    String strstdYAmount = "0.00";
    String strentMntAmount = "0.00";
    String strentYAmount = "0.00";
    DecimalFormat df = new DecimalFormat("#0.00");
    if (packageObj != null) {
        for (SgReqbucketdetails reqbucketdetails : packageObj) {
            String bucketName = reqbucketdetails.getBucketName().toLowerCase();
            if (bucketName.contains("basic") && bucketName.contains("month")) {
                basicMonth = reqbucketdetails.getBucketName();
                basicMAmount = reqbucketdetails.getPlanAmount();
                strbasicMAmount = df.format(basicMAmount);
                packageFound = false;
            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                basicYear = reqbucketdetails.getBucketName();
                basicYAmount = reqbucketdetails.getPlanAmount();
                strbasicYAmount = df.format(basicYAmount);
                packageFound = false;
            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                studentMonth = reqbucketdetails.getBucketName();
                studentMAMount = reqbucketdetails.getPlanAmount();
                strstudentMAMount = df.format(studentMAMount);
                packageFound = false;
            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                studentYear = reqbucketdetails.getBucketName();
                studentYAmount = reqbucketdetails.getPlanAmount();
                strstudentYAmount = df.format(studentYAmount);
                packageFound = false;
            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                standardMonth = reqbucketdetails.getBucketName();
                stdMAMount = reqbucketdetails.getPlanAmount();
                strstdMAMount = df.format(stdMAMount);
                packageFound = false;
            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                standardYear = reqbucketdetails.getBucketName();
                stdYAmount = reqbucketdetails.getPlanAmount();
                strstdYAmount = df.format(stdYAmount);
                packageFound = false;
            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                enterpriseMonth = reqbucketdetails.getBucketName();
                strentMntAmount = df.format(reqbucketdetails.getPlanAmount());
                packageFound = false;
            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                enterpriseYear = reqbucketdetails.getBucketName();
                strentYAmount = df.format(reqbucketdetails.getPlanAmount());
                packageFound = false;
            }
        }
    }  
                %>                                    
                <div class="row">
                    &nbsp;&nbsp;&nbsp;&nbsp;
<!--                    <input id="TheCheckBox" type="checkbox" data-off-text="Yearly" data-on-text="Monthly" checked="false" class="BSswitch">
                    <script>                                
                                    $('.BSswitch').bootstrapSwitch('state', true);

                                    //$('#CheckBoxValue').text($("#TheCheckBox").bootstrapSwitch('state'));
                                    $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
                                        //$("#CheckBoxValue").text($('#TheCheckBox').bootstrapSwitch('state'));

                                        if($('#TheCheckBox').bootstrapSwitch('state')){
                                            $("#monthly").show();
                                            $("#yearly").hide();
                                        }else{
                                            $("#yearly").show();
                                            $("#monthly").hide();
                                        }
                                    });
                    </script>-->
                    <table> 
                        <tr>
                            <td style="padding: 10px!important; "><div id="monthlyLable"><h4>Monthly</h4></div> </td>
                            <td><input class="checkbox" name="checkboxName" type="checkbox"></td>
                            <td style="padding: 10px!important;"> <div id="yearlyLable" style="display: none"><h4>Yearly</h4></div></td>
                        </tr>                  
                    </table>
                    <br><br><br>
                    <div id="monthly">
                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hyellow active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Basic</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit. 
                            </p>
                                                       
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                Features
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (3)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (1000 Credits)
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                                                          
                            
                            <h3 class="font-bold">
                                A$ <%=strbasicMAmount%>/Month
                            </h3>                           
                            <a href="#" onclick="subscribePrepaid('<%=basicMonth%>', 'prepaid')" class="btn btn-warning btn-sm m-t-xs btn-outline">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-lg-3">
                    <div class="hpanel plan-box hblue active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Student</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                 <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits  (1500)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strstudentMAMount%>/Month
                            </h3>  
                            <a href="#" class="btn btn-info btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=studentMonth%>', 'prepaid')">Sign Up</a>

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hgreen active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Startup</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (5000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strstdMAMount%>/Month
                            </h3>                                                              
                            <a href="#" class="btn btn-success btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=standardMonth%>', 'prepaid')">Sign Up</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hred active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Enterprise</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call) 
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                       
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (36000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strentMntAmount%>/Month
                            </h3>
                            <a href="#" class="btn btn-danger btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=enterpriseMonth%>', 'prepaid')">Sign Up</a>
                        </div>
                    </div>
                </div>
                </div>
                <div id="yearly" style="display: none">
                        <div class="col-sm-3">
                    <div class="hpanel plan-box hyellow active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Basic</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second  (3)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (50000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strbasicYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=basicYear%>', 'prepaid')" class="btn btn-warning disabled btn-sm m-t-xs btn-outline">Sign Up</a>
                        </div>
                    </div>
                </div>
                        <div class="col-sm-3">
                    <div class="hpanel plan-box hblue active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Student</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (150000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strstudentYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=studentYear%>', 'prepaid')" class="btn btn-info btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                        </div>
                    </div>
                </div>
                         <div class="col-sm-3">
                    <div class="hpanel plan-box hgreen active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Startup</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (500000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strstdYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=standardYear%>', 'prepaid')" class="btn btn-success disabled btn-sm m-t-xs btn-outline">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box hred active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Enterprise</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (5400000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strentYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=enterpriseYear%>', 'prepaid')" class="btn btn-danger btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                        </div>
                    </div>
                </div>
                    </div>                
            </div>
            </div>

            <%
                          
                if (packageFound) {
            %>
            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <i class="pe-7s-way text-success big-icon"></i>
                            <h1>Not found</h1>
                            <strong>No package found</strong>
                            <p>
                                Sorry, but the package list as per your production request has not been found.

                            </p>
                            <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                        </div>
                    </div>
                </div>
            </div>     
            <%}%>
        </div>

        <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Powered by Service Guard And Axiom Protect 2.0</strong>
                </div>
                <%
                    java.util.Date dFooter = new java.util.Date();
                    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
                    String strYYYY = sdfFooter.format(dFooter);
                %>
                <div align="center"  class="col-md-12 text-center">
                    <p>Platform Owned By BlueBricks Technologies 2009-<%=strYYYY%> (<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</p>
                </div>
            </div>
    </div>

    <!-- Vendor scripts -->
    
    <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
   
    <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="vendor/iCheck/icheck.min.js"></script>
    <script src="vendor/sparkline/index.js"></script>
    <script src="vendor/ladda/dist/spin.min.js"></script>
    <script src="vendor/ladda/dist/ladda.min.js"></script>
    <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
    <script src="vendor/toastr/build/toastr.min.js"></script>
    <script src="scripts/utilityFunction.js" type="text/javascript"></script>
    <!-- App scripts -->
    <script src="scripts/homer.js"></script>
</body>
</html>