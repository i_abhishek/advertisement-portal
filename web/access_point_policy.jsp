<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">
	
	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Access Point Policy</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Access Point Policy
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>
	
	<div class="normalheader transition animated fadeIn">
		<div class="col-sm-12">
			<div class="col-sm-6">
				<select class="form-control m-b" name="ver">
					<option>Select Access Point</option>
					<option>SMS</option>
					<option>Map</option>
					<option>MFA</option>
					<option>Digital Signing</option>
				</select>
			</div>
		</div>
	
		<div class="hpanel">	
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#test"> Test Environment</a></li>
				<li class=""><a data-toggle="tab" href="#live"> Live Environment</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="test" class="tab-pane active">
					<div class="panel-body">
						<div class="col-lg-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required TPS</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required TPD</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required Start Day</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required End Day</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required Start Time</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required End Time</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Authentication Restriction</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Per Transaction Billing</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Certificate Check</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Token Check</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Currency Code</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Amount</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="live" class="tab-pane">
					<div class="panel-body">
						<div class="col-lg-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required TPS</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required TPD</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required Start Day</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required End Day</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required Start Time</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Required End Time</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Authentication Restriction</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Per Transaction Billing</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Certificate Check</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Token Check</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Currency Code</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-6 control-label">Amount</label>
									<div class="col-sm-6"><input type="text" class="form-control" disabled></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>
	
    <!-- Footer-->
    <%@include file="footer.jsp"%>