<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>TM API | Login</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">

</head>
<body class="blank" style="background: rgba(43,48,131,1);
background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<div class="back-link">
    <a href="registration.jsp" class="btn btn-primary">Guest? Register here</a>
</div>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3 style="color:#fff">TM API Login</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
					<form action="#" id="loginForm">
						<div class="form-group">
							<label class="control-label" for="email">Email</label>
							<input type="text" placeholder="example@gmail.com" title="Please enter you email" required="" value="" name="email" id="email" class="form-control">
							<span class="help-block small">Your registered email</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="otp">OTP</label>
							<input type="otp" title="Please enter your OTP" placeholder="******" required="" value="" name="otp" id="otp" class="form-control">
							<span class="help-block small"></span>
						</div>
						<button href="login.jsp" class="btn btn-success btn-block">Login</button>
						<button class="btn btn-default btn-block" data-toggle="modal" data-target="#sendotp">Resend OTP</button>
					</form>
                </div>
            </div>
			<div class="modal fade" id="sendotp" tabindex="-1" role="dialog"  aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="color-line"></div>
						<div class="modal-header">
							<h4 class="modal-title">Success!</h4>
							<small class="font-bold"></small>
						</div>
						<div class="modal-body">
							<p>OTP resend to your registered mobile phone number.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center" style="color:#fff">
            <strong>TM API</strong>
        </div>
    </div>
</div>


<!-- Vendor scripts -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>

<!-- App scripts -->
<script src="scripts/homer.js"></script>

</body>
</html>