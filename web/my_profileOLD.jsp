<%@include file="header.jsp"%>

<!-- Main Wrapper -->
<div id="wrapper">

	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Profile</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    My Profile
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

<div class="content animate-panel">

<div>
    <div class="row">
		<form method="get" class="form-horizontal">
			<div class="col-lg-6">
				<div class="hpanel">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="showhide"><i class="fa fa-chevron-up"></i></a>
						</div>
						Personal Information
					</div>
					<div class="panel-body">
						<!-- Jack Remark: Those field cannot edit mean profile information not allowed to change by developer -->
						<div class="form-group">
							<label class="col-sm-4 control-label">Developer Name</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Mobile No.</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Email</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Avatar</label>
							<div class="col-sm-8"><input type="file" class="form-control"></div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-default" type="submit">Cancel</button>
								<button class="btn btn-primary" type="submit">Save changes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="hpanel">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="showhide"><i class="fa fa-chevron-up"></i></a>
						</div>
						Company Information <!-- Jack Remark: Only display if under production -->
					</div>
					<div class="panel-body">		
						<div class="form-group">
							<label class="col-sm-4 control-label">I/C No.</label> <!-- Jack Remark: If developer sign up as individual -->
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Company Name</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Company Registration No.</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">GST No.</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Fixed Line Number</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Address</label>
							<div class="col-sm-8"><textarea type="text" class="form-control"></textarea></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">State</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Postcode</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Country</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="hr-line-dashed"></div>
						<h3>Billing Details</h3>
						<div class="form-group">
							<label class="col-sm-4 control-label">Address</label>
							<div class="col-sm-8"><textarea type="text" class="form-control"></textarea></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">State</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Postcode</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Country</label>
							<div class="col-sm-8"><input type="text" class="form-control"></div>
						</div>
						<div class="hr-line-dashed"></div>
						<h3>Bank Account Details</h3>
						<div class="form-group">
							<label class="col-sm-4 control-label">Bank Name</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Bank Branch</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Bank Account No.</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">SWIFT Code</label>
							<div class="col-sm-8"><input type="text" class="form-control" disabled></div>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>

    <!-- Footer-->
    <%@include file="footer.jsp"%>