<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.xpay.mockpurchase.AllVerifier"%>
<%@page import="com.mollatech.xpay.mockpurchase.AllTrustManager"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="java.security.KeyManagementException"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>
        <%        
        boolean txnFlag = false;
        String paymentMode = "";
        paymentMode   = (String)session.getAttribute("_packageType");
        String loanId = (String)session.getAttribute("_loanId");
        try {
                // TODO initialize WS operation arguments here
                
                java.lang.String promocodeId    = request.getParameter("_promocodeId");
                
                String packageName  = request.getParameter("_packageName");
                String invoiceId    = request.getParameter("_invoiceId");
                String _grossAmount = request.getParameter("_grossAmount");
                
                String _changeOnPackageCharge = request.getParameter("_changeOnPackageCharge");
                String _reactivationCharge = request.getParameter("_reactivationCharge");
                String _cancellationCharge = request.getParameter("_cancellationCharge");
                String _latePenaltyCharge = request.getParameter("_latePenaltyCharge");
                
                String _totalAmountWithoutTax = request.getParameter("_totalAmountWithoutTax");
                String _gstTax = request.getParameter("_gstTax");
                String _vatTax = request.getParameter("_vatTax");
                String _serviceTax = request.getParameter("_serviceTax");
               
        %>
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Payment Response</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Payment Response
                </h2>
                <small>Your online payment response</small>
            </div>
        </div>
    </div>
        <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            Transaction Status
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">
        <input type="hidden" id="gstRate" name="gstRate" value="<%=_gstTax%>">
        <input type="hidden" id="vatRate" name="vatRate" value="<%=_vatTax%>">
        <input type="hidden" id="stRate" name="stRate" value="<%=_serviceTax%>">
        <input type="hidden" id="totalPaid" name="totalPaid" value="<%=_totalAmountWithoutTax%>">

        <form class="form-horizontal" id="packagePaymentDetails"> 
            <input type="hidden" id="packageName" name="packageName" value="<%=packageName%>">
            <input type="hidden" id="invoiceId" name="invoiceId" value="<%=invoiceId%>">
            <input type="hidden" id="amount" name="amount" value="<%=_grossAmount%>">
            
            <input type="hidden" id="promoCodeId" name="promoCodeId" value="<%=promocodeId%>">
            <input type="hidden" id="changePackageCharge" name="changePackageCharge" value="<%=_changeOnPackageCharge%>">
            <input type="hidden" id="reactivationCharge" name="reactivationCharge" value="<%=_reactivationCharge%>">
            <input type="hidden" id="cancellationCharge" name="cancellationCharge" value="<%=_cancellationCharge%>">
            <input type="hidden" id="latePenaltyCharge" name="latePenaltyCharge" value="<%=_latePenaltyCharge%>">
        </form>
        <%           
            String MERCHANTID = request.getParameter("MERCHANTID");
            String MERCHANT_TRANID = request.getParameter("MERCHANT_TRANID");
            String ERR_CODE = request.getParameter("ERR_CODE");
            String ERR_DESC = request.getParameter("ERR_DESC");
            String AMOUNT = request.getParameter("AMOUNT");
            String TXN_STATUS = request.getParameter("TXN_STATUS");
            String USR_MSG = request.getParameter("USR_MSG");
            String USR_CODE = request.getParameter("USR_CODE");
            
            if(TXN_STATUS != null && !TXN_STATUS.isEmpty()){
                if(TXN_STATUS.equals("S")){
                    txnFlag = true;
                }
            }
        %>
        <div class="p-xs">
                    <form class="form-horizontal" method="post">
                        <div class="form-group has-success">
                            <%if(txnFlag){%>
                            <div class="col-sm-4 text-success">
                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Transaction Status</span>
                            </div>
                            <%}else{%>
                            <div class="col-sm-4 text-danger">
                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Transaction Status</span>
                            </div>
                            <%}%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Merchant Id         : </label> <%=MERCHANTID%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Merchant_TRANID     : </label> <%=MERCHANT_TRANID%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Error code          : </label> <%=ERR_CODE%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Error Desc          : </label> <%=ERR_DESC%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">User_Msg            : </label> <%=USR_MSG%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">User_Code           : </label> <%=USR_CODE%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Transaction Status  : </label> <%=TXN_STATUS%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Amount              : </label> <%=AMOUNT%>
                        </div> 
                        <div class="form-group">
                            <div class="col-sm-2"><a class="btn btn-sm btn-success" href="home.jsp">Go Back to Home</a></div>
                        </div>
                    </form>
                    <%
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    %>
                </div>
            </div>
        </div>
    </div>
</div>
<%if(txnFlag){%>                
<script>

//    var packageName = document.getElementById("packageName").value;
//    var invoiceId = document.getElementById("invoiceId").value;
//    var Amount = document.getElementById("amount").value;
//    var promocodeId = document.getElementById("promoCodeId").value;
    function subscribePackage() {
        var paymentMode = '<%=paymentMode%>';
        var loanId      = '<%=loanId%>';
        var s = './SubscribePackage';
        if (paymentMode === 'postpaid') {
            s = './PostpaidPayment';
        }
        if (loanId.trim() !== '' && loanId !== 'null') {
            s = './LoanPayment';
        }
        
        //pleasewait();
        $.ajax({
            type: 'POST',
            url: s,
            dataType: 'json',
            //data: $("#packagePaymentDetails").serialize(),
            success: function (data) {
                if (strCompare(data.result, "error") === 0) {

                } else if (strCompare(data.result, "success") === 0) {
                    addGST();
                    setTimeout(function () {
                        window.location = "./home.jsp";
                    }, 3000);
                } else if (strCompare(data.result, "redirect") === 0) {
                    waiting.modal('hide');
                    showAlert(data.message, "danger", 6000);
                    var packageName = data.packageName;
                    setTimeout(function () {
                        window.location = "./invoiceDetail.jsp?package=" + packageName;
                    }, 4000);
                }
            }
        });
    }

    $(document).ready(function () {
        subscribePackage();
    });
</script>
<script>

    function addGST() {
//        var gstRate = document.getElementById("gstRate").value;
//        var vatTax = document.getElementById("vatRate").value;
//        var stTax = document.getElementById("stRate").value;
//        var totalPaid = document.getElementById("totalPaid").value;
//        var s = './AddTaxDetails?gstTax=' + gstRate + '&vatTax=' + vatTax + '&stTax=' + stTax + '&_Amount=' + totalPaid;
//        var s   = './AddTaxDetails';
//        $.ajax({
//            type: 'POST',
//            url: s,
//            dataType: 'json',
//            success: function (data) {
//
//            }
//        });

    }

</script>
<%}%>

<%@include file="footer.jsp" %>  