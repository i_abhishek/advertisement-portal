<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/pdfAd.js" type="text/javascript"></script>

<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Email Ad
            </h2>
            <small>Add your Ad details </small>
        </div>
    </div>
</div>

<%
    SgAdvertiseSubscriptionDetails subscriptionDetails = (SgAdvertiseSubscriptionDetails) request.getSession().getAttribute("advertiserSubscriptionObject") ;
    String emailMessageLength = ""; String message = "NA"; int width = 160; int height = 160;  Integer emailLength = 0;
    if(subscriptionDetails != null){
        String emailAdDetails = subscriptionDetails.getEmailAdConfiguration();
        JSONObject pdfDetails = new JSONObject(emailAdDetails);
        if(pdfDetails.has("emailMessageCharacter")){
            emailMessageLength = pdfDetails.getString("emailMessageCharacter");
            emailLength = Integer.parseInt(emailMessageLength);
        }
    }
    SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdvertiserDetails(subscriptionDetails.getAdvertiserId());
    String emailAdContent = null;int newCharLength =0;
    if(adDetails != null){
        emailAdContent = adDetails.getEmailAdContent();
        char[] countemailAdContent = emailAdContent.toCharArray();
        int charPresent = countemailAdContent.length;
        emailLength = emailLength - charPresent;
        // adjustment for Character count JS require 
        newCharLength = emailLength - charPresent;
        newCharLength = emailLength + charPresent;
        //adjustment end
    }
    message = "Upload Advertisement email logo. Support Image File size limit 1mb with at least Width =  "+ width +" ,Height = "+height;          

    
%>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                                                                                                           
                    <h6><i class="fa fa-image fa-2x text-info"> Ad Title</h6></i> <br/>
                    <form id="emailAdvertisementForm">
                    <div style="background: #fff;">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <input type="text" id="emailAdTitle" name="emailAdTitle" class="form-control">
                        </div>
                    </div>
                        <br><br><br>
                    <h6><i class="fa fa-image fa-2x text-info"> Ad Description</h6></i> <br/>                    
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <%if(emailAdContent == null){%>
                            <textarea rows="7" cols="80" class="form-control" name="message" id="message" placeholder="Your Ad description" onkeydown="limitText(this.form.message,this.form.countdown,<%=emailLength%>);" onkeyup='limitText(this.form.message,this.form.countdown,<%=emailLength%>);'></textarea>
                            <%}else{%>
                            <textarea rows="7" cols="80" class="form-control" name="message" id="message" placeholder="Your Ad description" onkeydown="limitText(this.form.message,this.form.countdown,<%=newCharLength%>);" onkeyup='limitText(this.form.message,this.form.countdown,<%=newCharLength%>);'><%=emailAdContent%></textarea>
                            <%}%>
                            <br>
                            You have
                            <input readonly type="text" name="countdown" size="3" value="<%=emailLength%>"> chars left
                        </div>
                    </div>
                    <button class="btn btn-primary ladda-button validateEmailForm" data-style="zoom-in" onclick="validateEmailAdForm()" style="display: none">Save changes</button>        
                    </form>
                    <h6><i class="fa fa-image fa-2x text-info"> Logo</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	                            
                            <small><%=message%></small>
                        </div>
                    </div> 
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateEmailAdForm()">Save</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 50%"></div>
    </div>
</div>
<script type="text/javascript">
       function limitText(limitField, limitCount, limitNum) {
          if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
          } else {
            limitCount.value = limitNum - limitField.value.length;
          }
        }
</script>
                        
<script>
    $(document).ready(function () {        
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadEmailAdImage",
            uploadMultiple: false,            
            maxFilesize: 5,
            maxFiles: 1,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop advertisement image here to upload",
            init: function() {
                // Register for the thumbnail callback.
                // When the thumbnail is created the image dimensions are set.
                this.on("thumbnail", function(file) {
                  // Do the dimension checks you want to do
                  console.log("file.width == "+file.width);
                  console.log("file.height == "+file.height);                  
                  if (file.width >= <%=width%> || file.height >= <%=height%>) {
                    file.rejectDimensions()
                  }
                  else {
                    file.acceptDimensions();
                    //file.rejectDimensions()
                  }
                });
              },
              // Instead of directly accepting / rejecting the file, setup two
            // functions on the file that can be called later to accept / reject
            // the file.
            accept: function(file, done) {
              file.acceptDimensions = done;
              file.rejectDimensions = function() { done("The image must be equal to or less than <%=width%> x <%=height%>"); };
              // Of course you could also just put the `done` function in the file
              // and call it either with or without error in the `thumbnail` event
              // callback, but I think that this is cleaner.
            },
            removedfile: function(file) {
                removePDFAdImageFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

