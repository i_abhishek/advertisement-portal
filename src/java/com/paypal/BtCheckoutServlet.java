package com.paypal;

/*==================================================================
  Call to Get Access Token API and Create Payment (to get approval url or redirect url) from PayPal
 ===================================================================
 */
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "BtCheckoutServlet", urlPatterns = {"/BtCheckoutServlet"})
public class BtCheckoutServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //System.out.println("doPost");	
        RequestDispatcher dispatcher = null;

        dispatcher = request.getRequestDispatcher("error.jsp");

        dispatcher.forward(request, response);
//                return;
        BtTransactionHelper btHelper = new BtTransactionHelper();
        BtParameterVo vo = new BtParameterVo();
        vo.addParameter(request);
        try {
            btHelper.doTransaction(
                    vo.getd("payment-method-nonce"),
                    vo.getd("total-amount"),
                    vo.getd("first-name"),
                    vo.getd("last-name"),
                    vo.getd("line1"),
                    vo.getd("line2"),
                    vo.getd("city"),
                    vo.getd("state"),
                    vo.getd("postal-code"),
                    vo.getd("country-code")
            );
            if (btHelper.isSuccess()) {
//                dispatcher = request.getRequestDispatcher("/paymentResponse.jsp");
                btHelper.addSuccessParameters(vo);
                vo.setRequestAttributes(request);
                request.getSession().setAttribute("payPalFirstName", vo.getd("first-name"));
                request.getSession().setAttribute("payPalline1", vo.getd("line1"));
                request.getSession().setAttribute("payPalline2", vo.getd("line2"));
                request.getSession().setAttribute("payPalcity", vo.getd("city"));
                request.getSession().setAttribute("payPalcity", vo.getd("city"));
                request.getSession().setAttribute("payPalstate", vo.getd("state"));
                request.getSession().setAttribute("payPalpostal-code", vo.getd("postal-code"));
                request.getSession().setAttribute("payPalcountry-code", vo.getd("country-code"));
                request.getSession().setAttribute("payPaltransactionid", request.getAttribute("transactionid"));
                request.getSession().setAttribute("payPalstatus", vo.getd("status"));
                request.getSession().setAttribute("_grossAmount", vo.getd("total-amount"));
                response.sendRedirect("./paymentResponse.jsp");
            } else if (btHelper.getTransaction() != null) {
                btHelper.failurTransactionMessage();
                dispatcher = request.getRequestDispatcher("error.jsp");
            } else {
                try {
                    btHelper.addFailueLog();
                    dispatcher = request.getRequestDispatcher("/error.jsp?message=" + btHelper.getErrorMessage());
                } catch (Exception ex) {
                    dispatcher = request.getRequestDispatcher("/error.jsp?message=The transaction contained invalid data.");
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            //dispatcher = request.getRequestDispatcher("error.jsp");
            response.sendRedirect("./error.jsp");
        }

        //dispatcher.forward(request, response);
        response.sendRedirect("./paymentResponse.jsp");
        return;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
