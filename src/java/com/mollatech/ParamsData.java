/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech;

import com.mollatech.serviceguard.nucleus.commons.ClassName;
import com.mollatech.serviceguard.nucleus.commons.Classes;
import com.mollatech.serviceguard.nucleus.commons.Params;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ashu
 */
public class ParamsData {

    private Classes classes;
    private Map<String, List<Params>> classesMap;
    private Map<String, String> nameMap;

    public ParamsData(Classes classes) {
        this.classes = classes;
        classesMap = new HashMap();
        nameMap = new HashMap();
        loadMap();
    }

    private void loadMap() {
        if (classes != null) {
            List<ClassName> classs = classes.pname.classs;
            for (ClassName name : classs) {
                classesMap.put(name.classname, name.paramses);
                nameMap.put(name.classname, name.transformedclassname);
            }
        }
    }

    public Data getData(String className) {
        Data data = new Data();
        data.paramses = classesMap.get(className);
        data.transformedclassname = nameMap.get(className);
        return data;
    }

    public static class Data {

        public String transformedclassname;
        public List<Params> paramses;
    }
}
