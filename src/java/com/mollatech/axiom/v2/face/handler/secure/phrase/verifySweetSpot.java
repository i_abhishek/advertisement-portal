package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AuthenticationPackage;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author AMOL
 */
@WebServlet(name = "verifySweetSpot", urlPatterns = { "/verifySweetSpot" })
public class verifySweetSpot extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {
            String _userName = (String) request.getSession().getAttribute("_username");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            String _xCoordinate = request.getParameter("_xCoordinate");
            String _yCoordinate = request.getParameter("_yCoordinate");
            int ixCoordinate = -1;
            if (_xCoordinate != null) {
                ixCoordinate = Integer.parseInt(_xCoordinate);
            }
            int iyCoordinate = -1;
            if (_yCoordinate != null) {
                iyCoordinate = Integer.parseInt(_yCoordinate);
            }
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
            }
            String result = "success";
            String message = "successful credential verification.";
            String url = "./home.jsp";
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials";
                url = "login.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            String strPayLoad = null;
            AxiomWrapper axWrapper = new AxiomWrapper();
            RssUserCerdentials userObj = axWrapper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {
                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                axiomCredentialDetails.setXCoordinate(ixCoordinate);
                                axiomCredentialDetails.setYCoordinate(iyCoordinate);
                                userObj.getTokenDetails().set(i, axiomCredentialDetails);
                            }
                        }
                    }
                }
            }
            AuthenticationPackage authPackage = axWrapper.generateAuthenticationPackage(sessionId, userObj, "cookie", strPayLoad);
            if (authPackage != null) {
                System.out.println("Sweet Spot =" + authPackage.getSweetSpot());
                if (authPackage.getGetValidImage() != 0) {
                    result = "error";
                    message = "Invalid Image.";
                    url = "login.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            }
            HttpSession session = request.getSession(true);
            AxiomStatus aStatus = null;
            if (authPackage.getSweetSpot() == 0) {
                json.put("_result", "success");
                json.put("_message", "sweet spot is verified");
                json.put("_url", "home.jsp");
                out.print(json);
                out.flush();
                return;
            } else {
                json.put("_result", "error");
                json.put("_message", "sweet spot is not verified");
                json.put("_url", "login.jsp");
                out.print(json);
                out.flush();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
