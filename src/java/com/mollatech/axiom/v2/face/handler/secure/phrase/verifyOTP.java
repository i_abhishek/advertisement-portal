package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author AMOL
 */
@WebServlet(name = "verifyOTP", urlPatterns = { "/verifyOTP" })
public class verifyOTP extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {
            String _userName = (String) request.getSession().getAttribute("_username");
            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String otp = (String) request.getParameter("_otp");
            String result = "success";
            String message = "successful credential verification.";
            String url = "./home.jsp";
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials";
                url = "login.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            String strPayLoad = null;
            AxiomWrapper axWrapper = new AxiomWrapper();
            String TrustPayload = null;
            VerifyRequest vr = new VerifyRequest();
            vr.setCategory(AxiomWrapper.SOFTWARE_TOKEN);
            vr.setSubcategory(AxiomWrapper.SW_MOBILE_TOKEN);
            vr.setCredential(otp);
            AxiomStatus astatus = axWrapper.verifyCredential(sessionId, rssUserObj.getRssUser().getUserId(), vr, TrustPayload);
            if (astatus.getErrorcode() == 0) {
                json.put("_result", "success");
                json.put("_message", "otp is verified");
                json.put("_url", "home.jsp");
                out.print(json);
                out.flush();
                return;
            } else {
                json.put("_result", "error");
                json.put("_message", "sweet spot is not verified");
                json.put("_url", "login.jsp");
                out.print(json);
                out.flush();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
