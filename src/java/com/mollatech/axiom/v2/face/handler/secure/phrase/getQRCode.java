package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import org.bouncycastle.util.encoders.Base64;

@WebServlet(name = "getQRCode", urlPatterns = { "/getQRCode" })
public class getQRCode extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setContentType("image/gif");
            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String sessionid = (String) request.getSession().getAttribute("_userSessinId");
            AxiomWrapper wrapper = new AxiomWrapper();
            AxiomStatus astatus = wrapper.getQRImage(sessionid, rssUserObj.getRssUser().getUserId(), 1);
            if (astatus != null) {
                byte[] plainImage = Base64.decode(astatus.getRegCodeMessage());
                MagicMatch match = Magic.getMagicMatch(plainImage, true);
                String mimeType = match.getMimeType();
                if (mimeType.startsWith("image")) {
                    response.setContentType(mimeType);
                    response.setContentLength(plainImage.length);
                    OutputStream out = response.getOutputStream();
                    out.write(plainImage, 0, plainImage.length);
                } else {
                    return;
                }
            } else {
                return;
            }
        } catch (MagicParseException ex) {
            Logger.getLogger(getQRCode.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MagicMatchNotFoundException ex) {
            Logger.getLogger(getQRCode.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MagicException ex) {
            Logger.getLogger(getQRCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
