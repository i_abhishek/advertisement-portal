package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ReAssignAPToken", urlPatterns = { "/ReAssignAPToken" })
public class ReAssignAPToken extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Token regenerated successfully";
        PrintWriter out = response.getWriter();
        try {
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channel = (String) request.getSession().getAttribute("_ChannelId");
            int partnerId = -1;
            String _partnerId = request.getParameter("_partnerid");
            String env = request.getParameter("env");
            if (_partnerId != null) {
                partnerId = Integer.parseInt(_partnerId);
            }
            String partnerName = request.getParameter("_partnername");
            String partnerEmail = request.getParameter("_partneremail");
            String partnerPhone = request.getParameter("_partnerphone");
            PartnerManagement partMngt = new PartnerManagement();
            int resp = partMngt.RegenerateToken(sessionId, channel, partnerId, partnerName, partnerEmail, partnerPhone, env);
            if (resp == 0) {
                result = "success";
                message = "Token regenerated for " + env + " environment successfully.";
            } else {
                result = "error";
                message = "Token not regenerated successfully.";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
