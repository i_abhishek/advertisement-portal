package com.mollatech.SGportal.partner;

import com.mollatech.SGportal.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class partnerRequestStatus extends HttpServlet {

    public final int PASSWORD = 1;

    public final int APPROVE = 1;

    public final int REJECT = -1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Partner Status change successful.";
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("text/html;charset=UTF-8");
            String partnerRequestId = request.getParameter("partnerRequestId");
            String partnerRequestStatus = request.getParameter("partnerRequestStatus");
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_ChannelId");
            PartnerRequestManagement ppw = new PartnerRequestManagement();
            int ipartnerRequestId = 0;
            if (partnerRequestId != null) {
                ipartnerRequestId = Integer.parseInt(partnerRequestId);
            }
            int ipartnerRequestStatus = 0;
            if (partnerRequestStatus != null) {
                ipartnerRequestStatus = Integer.parseInt(partnerRequestStatus);
            }
            if (ipartnerRequestStatus == APPROVE) {
                SgPartnerrequest sgpartner = ppw.getPartnerRequestsbyReqId(sessionId, ipartnerRequestId);
                if (sgpartner != null) {
                    int pStatus = ppw.ChangePartnerRequestStatus(sessionId, ipartnerRequestId, APPROVE);
                    if (pStatus == 0) {
                        String _groupId = request.getParameter("_groupId");
                        String token = sgpartner.getName() + sgpartner.getEmail() + new Date() + sessionId + sgpartner.getPhone();
                        byte[] SHA1hash = CommonUtility.SHA1(token);
                        token = new String(Base64.encode(SHA1hash));
                        int pstatus = new PartnerManagement().CreatePartner(sessionId, "", sgpartner.getName(), sgpartner.getEmail(), APPROVE, sgpartner.getPhone(), _groupId, APPROVE, sgpartner.getIpAddress(), token, sgpartner.getIpLive());
                        if (pstatus != -1) {
                            int pstatus1 = ppw.ChangePartnerRequest(sessionId, ipartnerRequestId, sgpartner.getPartnerid());
                            if (pstatus1 == 0) {
                                message = " Partner Request Approved.";
                                json.put("_result", result);
                                json.put("_message", message);
                            } else {
                                ppw.ChangePartnerRequestStatus(sessionId, ipartnerRequestId, REJECT);
                                new PartnerManagement().deletePartnerDetailByToken(sessionId, null, token);
                                result = "error";
                                message = "Unable to approve Developer Request.";
                                json.put("_result", result);
                                json.put("_message", message);
                                return;
                            }
                        } else {
                            ppw.ChangePartnerRequestStatus(sessionId, ipartnerRequestId, REJECT);
                            result = "error";
                            message = "Unable to approve Developer Request.";
                            json.put("_result", result);
                            json.put("_message", message);
                            return;
                        }
                    } else {
                        ppw.ChangePartnerRequestStatus(sessionId, ipartnerRequestId, REJECT);
                        result = "error";
                        message = "Unable to approve Developer Request.";
                        json.put("_result", result);
                        json.put("_message", message);
                        return;
                    }
                }
            } else if (ipartnerRequestStatus == REJECT) {
                int pStatus = ppw.ChangePartnerRequestStatus(sessionId, ipartnerRequestId, REJECT);
                if (pStatus == 0) {
                    result = "success";
                    message = "Developer Request Rejected.";
                    json.put("_result", result);
                    json.put("_message", message);
                } else if (pStatus != 0) {
                    result = "error";
                    message = "Failed to reject Developer request.";
                    json.put("_result", result);
                    json.put("_message", message);
                    return;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            return;
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
