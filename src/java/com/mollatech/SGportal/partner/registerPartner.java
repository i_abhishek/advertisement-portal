/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.RequestedAccessPolicyEntry;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.GroupDetails;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
@WebServlet(name = "registerPartner", urlPatterns = {"/registerPartner"})
public class registerPartner extends HttpServlet {

    static final Logger logger = Logger.getLogger(registerPartner.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        logger.info("Request servlet is #registerPartner from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "You have been successfully sign up with Developer Portal";
        PrintWriter out = response.getWriter();
        int TPD = -1, TPS = -1;
        SessionManagement sManagement = new SessionManagement();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String pName = request.getParameter("partnerName");
        logger.debug("value of pName : " + pName);
        String pEmail = request.getParameter("partnerEmail");
        logger.debug("value of pEmail : " + pEmail);
        String pMobileNo = request.getParameter("partnerMobNo");
        logger.debug("value of pMobileNo : " + pMobileNo);
        //pMobileNo = "91" + pMobileNo;
        int pStatus = GlobalStatus.PENDING;
        String automation = LoadSettings.g_sSettings.getProperty("partner.automation");
        if (automation != null) {
            if (automation.equalsIgnoreCase("true")) {
                pStatus = GlobalStatus.ACTIVE;
            }
        }
        if (pMobileNo != null && !pMobileNo.isEmpty()) {
            pMobileNo = pMobileNo.replaceAll("[-+.^:,]", "").trim();
        }
        String ipAddress = "";
        String ipLive = "";
        String[] ipTest = request.getParameterValues("partnerIP");
        logger.debug("value of ipAddress : " + ipAddress);
        String[] ipProd = request.getParameterValues("partnerIPLive");
        logger.debug("value of ipLive : " + ipLive);
        String pWebsite = request.getParameter("partnerWebsite");
        logger.debug("value of pWebsite : " + pWebsite);
        String pLandline = request.getParameter("partnerLandlineNo");
        logger.debug("value of pLandline : " + pLandline);
        String pFax = request.getParameter("partnerFax");
        logger.debug("value of pFax : " + pFax);
        String pCompanyName = request.getParameter("partnerComName");
        logger.debug("value of pCompanyName : " + pCompanyName);
        String pAddress = request.getParameter("partnerAddress");
        logger.debug("value of pAddress : " + pAddress);
        String pPincode = request.getParameter("partnerPincode");
        logger.debug("value of pPincode : " + pPincode);

        String pSD = request.getParameter("partnerSD");
        logger.debug("value of pSD : " + pSD);
        String pED = request.getParameter("partnerED");
        logger.debug("value of pED : " + pED);
        String pST = request.getParameter("partnerST");
        logger.debug("value of pST : " + pST);
        String pET = request.getParameter("partnerET");
        logger.debug("value of pET : " + pET);
        String _pTPD = request.getParameter("partnerTPD");
        logger.debug("value of _pTPD : " + _pTPD);
        String _pTPS = request.getParameter("partnerTPS");
        logger.debug("value of _pTPS : " + _pTPS);

        String zippath = (String) request.getSession().getAttribute("_ZipPath");
        logger.debug("value of zippath : " + zippath);
        String pp_password = request.getParameter("pp_password");
        logger.debug("value of _pTPS : " + pp_password);
        String termsAndCondition = request.getParameter("termsCondition");
        logger.debug("value of termsAndCondition : " + termsAndCondition);
        String type = request.getParameter("type");
        logger.debug("value of type : " + type);
        if (pp_password != null) {
            pName = request.getParameter("username_pp");
            zippath = (new File("documents.zip")).getAbsolutePath();
            pWebsite = "https://www.tmapi.com";
            pLandline = pFax = pMobileNo;
            pCompanyName = "Test Company";
            pAddress = "US";
            pPincode = "12345";
            //pName = "User" + CommonUtility.randomString(2);
            ipTest = new String[1];
            ipTest[0] = "*";
            ipProd = new String[1];
            ipProd[0] = "*";
        }
        Path path = Paths.get(zippath);
        byte[] compressed_document = Files.readAllBytes(path);

        if (ipTest != null) {
            for (int i = 0; i < ipTest.length; i++) {
                ipAddress += ipTest[i] + ",";
            }
        }
        if (ipProd != null) {
            for (int i = 0; i < ipProd.length; i++) {
                ipLive += ipProd[i] + ",";
            }
        }
        String Operator = "Admin";
// start validation

//  mobile number validation
        if (pMobileNo.length() < 8 || pMobileNo.length() > 16) {
            result = "error";
            message = "Mobile length cann't be less than 8 digit";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        } else if (UtilityFunctions.isValidPhoneNumber(pMobileNo) == false) {
            result = "error";
            message = "Enter Valid mobile number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(pLandline) == false) {
            result = "error";
            message = "Enter Valid landline number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(pFax) == false) {
            result = "error";
            message = "Enter Valid fax number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
//  end landline
// start pincode validation
        if (UtilityFunctions.isValidPhoneNumber(pPincode) == false) {
            result = "error";
            message = "Enter Valid fax number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
// end pincode   
//        end validation
        if (_pTPD != null) {
            if (!_pTPD.isEmpty()) {
                // start tpd validation
                if (UtilityFunctions.isValidPhoneNumber(_pTPD) == false) {
                    result = "error";
                    message = "Enter valid TPD";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
// end tpd
                TPD = Integer.parseInt(_pTPD);
            }
        }
        if (type != null && type.equalsIgnoreCase("html") && termsAndCondition == null) {
            result = "error";
            message = "Please accept terms and condition";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (_pTPS != null) {
            if (!_pTPS.isEmpty()) {
                // start tps validation
                if (UtilityFunctions.isValidPhoneNumber(_pTPS) == false) {
                    result = "error";
                    message = "Enter valid TPS";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
// end tps
                TPS = Integer.parseInt(_pTPS);
            }
        }
        int retValue = new PartnerManagement().checkIsUniqueInPartner(SessionId, channel.getChannelid(), pName, pEmail, pMobileNo);

        if (retValue != 0) {
            result = "error";
            message = "Email id or Phone no is already in use, Please try with other";
            try {
                json.put("_result", result);
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RequestedAccessPolicyEntry ap = new RequestedAccessPolicyEntry();
        ap.setApEndTime(pET);
        ap.setApStartTime(pST);
        ap.setDayFrom(pSD);
        ap.setDayTo(pED);
        ap.setTpd(TPD);
        ap.setTps(TPS);
        GroupDetails details = new GroupManagement().checkIsUniqueInGroup(SessionId, channel.getChannelid(), "MPServices");
        if (details == null) {
            pStatus = GlobalStatus.PENDING;
        }
        SgBucketdetails packageObject = new PackageManagement().getPackageByDefault(SessionId, channel.getChannelid());
        if (packageObject == null) {
            pStatus = GlobalStatus.PENDING;
        }
        if (SessionId != null) {
            PartnerRequestManagement ppw = new PartnerRequestManagement();
            int poStatus = ppw.addPartnerRequest(SessionId, pName, pStatus, pMobileNo, pEmail, ipAddress, pWebsite, pFax, pLandline, pAddress, pPincode, pCompanyName, ap, compressed_document, ipLive, pp_password);
            if (poStatus != -1) {
                if (pStatus == GlobalStatus.ACTIVE) {
                    final int ADMIN = 0;
                    SgPartnerrequest sgpartner = new PartnerRequestManagement().getPartnerRequestsbyReqId(SessionId, poStatus);
                    String token = sgpartner.getName() + sgpartner.getEmail() + new Date() + SessionId + sgpartner.getPhone();
                    byte[] SHA1hash = CommonUtility.SHA1(token);
                    token = new String(Base64.encode(SHA1hash));
                    int pstatus = new PartnerManagement().CreatePartner(SessionId, channel.getChannelid(), sgpartner.getName(), sgpartner.getEmail(), GlobalStatus.ACTIVE, sgpartner.getPhone(), "" + details.getGroupId(), GlobalStatus.ACTIVE, sgpartner.getIpAddress(), token, sgpartner.getIpLive());
                    sgpartner.setPartnerid(pstatus);
                    new PartnerRequestManagement().editPartnerRequest(SessionId, sgpartner);
                    SgUsers sguser = new SgUsers();
                    sguser.setUsername(sgpartner.getName());
                    sguser.setEmail(sgpartner.getEmail());
                    sguser.setPhone(sgpartner.getPhone());
                    sguser.setChannelid(channel.getChannelid());
                    sguser.setAttempts(0);
                    sguser.setPartnerid(pstatus);
                    sguser.setCreatedon(new Date());
                    sguser.setLastlogindate(new Date());
                    sguser.setStatus(GlobalStatus.ACTIVE);
                    sguser.setType(ADMIN);
                    sguser.setPassword(sgpartner.getField1());
                    sguser.setFirstLoginFlag(GlobalStatus.FIRSTTIMELOGIN);
                    new UsersManagement().AddSgUsers(SessionId, channel.getChannelid(), sguser);
                    SgSubscriptionDetails subscriptionObject = new SgSubscriptionDetails();
                    subscriptionObject.setTierRateDetails(packageObject.getTierRateDetails());
                    subscriptionObject.setApRateDetails(packageObject.getApRateDetails());
                    subscriptionObject.setBucketDuration(packageObject.getBucketDuration());
                    subscriptionObject.setBucketName(packageObject.getBucketName());
                    subscriptionObject.setCancellationRate(packageObject.getCancellationRate());
                    subscriptionObject.setChangePackageRate(packageObject.getChangePackageRate());
                    subscriptionObject.setChannelId(packageObject.getChannelId());
                    subscriptionObject.setCreationDate(new Date());
                    subscriptionObject.setCreatedOn(new Date());
                    subscriptionObject.setBucketId(packageObject.getBucketId());
                    subscriptionObject.setDaysForFreeTrial(packageObject.getDaysForFreeTrial());
                    subscriptionObject.setFeatureList(packageObject.getFeatureList());
                    subscriptionObject.setFreeCredits(packageObject.getFreeCredits());
                    subscriptionObject.setLatePenaltyRate(packageObject.getLatePenaltyRate());
                    subscriptionObject.setMinimumBalance(packageObject.getMinimumBalance());
                    subscriptionObject.setPartnerId(pstatus);
                    subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                    subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                    subscriptionObject.setReActivationCharge(packageObject.getReActivationCharge());
                    subscriptionObject.setSecurityAndAlertDetails(packageObject.getSecurityAndAlertDetails());
                    subscriptionObject.setServiceCharge(packageObject.getServiceCharge());
                    subscriptionObject.setSlabApRateDetails(packageObject.getSlabApRateDetails());
                    subscriptionObject.setStatus(GlobalStatus.DEFAULT);
                    subscriptionObject.setTax(packageObject.getTax());
                    subscriptionObject.setMainCredits(packageObject.getMainCredits());
                    subscriptionObject.setFlatPrice(packageObject.getFlatPrice());

                    int days = 0;
                    if (packageObject.getBucketDuration().equalsIgnoreCase("daily")) {
                        days = 1;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("weekly")) {
                        days = 7;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("biMonthly")) {
                        days = 15;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("monthly")) {
                        days = 30;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("quarterly")) {
                        days = 90;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("halfYearly")) {
                        days = 180;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("yearly")) {
                        days = 365;
                    }
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.add(Calendar.DATE, days);
                    subscriptionObject.setExpiryDateNTime(calendar.getTime());
                    new PackageSubscriptionManagement().CreateSubscriptionDetails(SessionId, channel.getChannelid(), subscriptionObject);
                    SgCreditInfo info = new SgCreditInfo();
                    info.setStatus(GlobalStatus.SUCCESS);
                    info.setApiRates(subscriptionObject.getApRateDetails());
                    info.setFreeCredit(subscriptionObject.getFreeCredits());
                    info.setMainCredit(subscriptionObject.getMainCredits());
                    info.setPartnerId(subscriptionObject.getPartnerId());
                    new CreditManagement().addDetails(info);
                }

                result = "success";
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  result is " + result);
                message = "You have been successfully sign up with Developer Portal !";
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  message is " + message);

            } else {
                result = "error";
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  result is " + result);
                message = "Sign up Failed !";
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  message is " + message);
            }
        } else {
            result = "error";
            logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  result is " + result);
            message = "Sign up Failed !";
            logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  message is " + message);
        }

        try {
            json.put("_result", result);
            logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter result is " + result);
            json.put("_message", message);
            logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
