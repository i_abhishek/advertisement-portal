/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.simple.JSONObject;

@WebServlet(name = "CheckLogin", urlPatterns = {"/CheckLogin"})
public class CheckLogin extends HttpServlet {
    public static final int PENDING = 2;
    public static final int SENT = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        boolean otpUse = false;
        boolean pass = true;
        try {
            String mailId           = request.getParameter("mailId");            
            String useOtp = LoadSettings.g_sSettings.getProperty("use.otp");
            SgUsers users = new UsersManagement().getSgUsersByEmail(mailId);
            if (users == null) {
                json.put("_result", "error");
                json.put("_message", "Please Enter Correct Email Id.");
                return;
            }
            if (useOtp != null) {
                if (useOtp.equalsIgnoreCase("yes")) {
                    otpUse = true;
                }
                if (users.getFirstLoginFlag() == GlobalStatus.FIRSTTIMELOGINDONE) {
                    otpUse = false;
                }
            }
            if (otpUse) {
                SendNotification notification = new SendNotification();
                String _channelName = "ServiceGuard";
                SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                Session sChannel = suChannel.openSession();
                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels channels = cUtil.getChannel(_channelName);
                 String Emailmessage = LoadSettings.g_templateSettings.getProperty("email.otp.message");
                if (users.getOtpExpiry() != null) {
                    if (users.getOtpExpiry().after(new Date())) {
                        String message = LoadSettings.g_templateSettings.getProperty("mobile.otp.message");                       
                        message = message.replace("#OTP#", String.valueOf(users.getOtp()));
                        Emailmessage = Emailmessage.replace("#OTP#", String.valueOf(users.getOtp()));
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, Integer.parseInt(LoadSettings.g_sSettings.getProperty("otp.expiry.time.in,minutes")));
                        message = message.replace("#OTP#", String.valueOf(users.getOtp()));
                        Emailmessage = Emailmessage.replace("#OTP#", String.valueOf(users.getOtp()));
                        message = message.replace("#Date#", UtilityFunctions.getTMReqDate(users.getOtpExpiry()));                        
                        Emailmessage = Emailmessage.replace("#Date#", UtilityFunctions.getTMReqDate(users.getOtpExpiry()));
                        Emailmessage = Emailmessage.replace("#Name#", users.getUsername());
                        Emailmessage = Emailmessage.replace("#channel#", "Multi Purpose Service Portal");
                        SGStatus status = notification.SendOnMobileByPreference(channels.getChannelid(), users.getPhone(), message, 1, 1, 1);
                        if (status.iStatus == 0) {
                            json.put("_result", "otp");
                            json.put("_message", "OTP resend on your registered mobile number.");
                            json.put("_id", mailId);
                            return;
                        }else{
//                            json.put("_result", "error");
//                            json.put("_message", "OTP, SMS failed to send on your registered mobile number.");
//                            json.put("_id", mailId);
//                            return;
                            // if SMS failed to sent OTP, Sent OTP over email
                            status = new SendNotification().SendEmail(channels.getChannelid(), users.getEmail(), "Blue Bricks API Login OTP", Emailmessage, null, null, null, null, 3);
                            if (status.iStatus == PENDING || status.iStatus == SENT) {
                                json.put("_result", "otp");                                
                                json.put("_message", "OTP resend on your registered email.");  
                                json.put("_id", mailId);
                                return;
                            } else {                                
                                message = "OTP failed to send on email";
                                try {
                                    json.put("_result", "error");                                    
                                    json.put("_message", message);
                                    json.put("_id", mailId);
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();                                    
                                }
                            }
                        }
                    }
                }
                if (users.getFirstLoginFlag() == GlobalStatus.FIRSTTIMELOGIN) {                    
                    Random r = new Random();
                    String number = "";
                    int counter = 0;
                    while (counter++ < Integer.parseInt(LoadSettings.g_sSettings.getProperty("otp.length"))) {
                        number += r.nextInt(9);
                    }
                    String message = LoadSettings.g_templateSettings.getProperty("mobile.otp.message");
                    message = message.replace("#OTP#", number);
                    Emailmessage = Emailmessage.replace("#OTP#", String.valueOf(number));
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MINUTE, Integer.parseInt(LoadSettings.g_sSettings.getProperty("otp.expiry.time.in,minutes")));
                    message = message.replace("#OTP#", number);
                    message = message.replace("#Date#", UtilityFunctions.getTMReqDate(calendar.getTime()));
                    Emailmessage = Emailmessage.replace("#OTP#", String.valueOf(users.getOtp()));
                    Emailmessage = Emailmessage.replace("#Date#", UtilityFunctions.getTMReqDate(calendar.getTime()));
                    Emailmessage = Emailmessage.replace("#Name#", users.getUsername());
                    Emailmessage = Emailmessage.replace("#channel#", "Developer Portal");
                    SGStatus status = notification.SendOnMobileByPreference(channels.getChannelid(), users.getPhone(), message, 1, 1, 1);
                    if (status.iStatus == 0) {
                        pass = false;
                        json.put("_result", "otp");
                        json.put("_message", "OTP resend on your registered mobile phone number.");
                        json.put("_id", mailId);
                        users.setOtp(Integer.parseInt(number));
                        users.setOtpExpiry(calendar.getTime());
                        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
                        Session sRemoteAcess = suRemoteAcess.openSession();
                        SessionManagement sManagement = new SessionManagement();
                        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
                        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channels.getChannelid());
                        String SessionId = sManagement.OpenSession(channels.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
                        new UsersManagement().editSgUsers(SessionId, channels.getChannelid(), users);
                    } else {
//                        json.put("_result", "error");
//                        json.put("_message", "OTP, SMS failed to send on your registered mobile number.");
//                        json.put("_id", mailId);
//                        return;

                        status = new SendNotification().SendEmail(channels.getChannelid(), users.getEmail(), "Blue Bricks API Login OTP", Emailmessage, null, null, null, null, 3);
                        if (status.iStatus == PENDING || status.iStatus == SENT) {
                            json.put("_result", "otp");
                            json.put("_message", "OTP resend on your registered email.");
                            json.put("_id", mailId);
                            pass = false;
                            users.setOtp(Integer.parseInt(number));
                            users.setOtpExpiry(calendar.getTime());
                            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
                            Session sRemoteAcess = suRemoteAcess.openSession();
                            SessionManagement sManagement = new SessionManagement();
                            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
                            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channels.getChannelid());
                            String SessionId = sManagement.OpenSession(channels.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
                            new UsersManagement().editSgUsers(SessionId, channels.getChannelid(), users);
                        } else {
                            message = "OTP failed to send on email";
                            try {
                                json.put("_result", "error");
                                json.put("_message", message);
                                json.put("_id", mailId);
                                return;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    pass = true;
                }
            }else{
                json.put("_result", "secondTimeLogin");
                json.put("_id", mailId);
                json.put("_message", "Please Enter Password.");
                return;
            }
            if (pass) {
                json.put("_result", "password");
                json.put("_id", mailId);
                json.put("_message", "Please Enter Password.");
            }
        } catch (Exception ex) {
            json.put("_result", "password");
            json.put("_message", "Please Enter Password.");
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
