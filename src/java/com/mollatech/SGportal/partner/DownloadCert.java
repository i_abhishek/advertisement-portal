package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "DownloadCert", urlPatterns = { "/DownloadCert" })
public class DownloadCert extends HttpServlet {

    static final Logger logger = Logger.getLogger(DownloadCert.class);

    private String filePath;

    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Requested Servlet is DownloadCert at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String pId = request.getParameter("pid");
        logger.debug("Value of pId = " + pId);
        String env = request.getParameter("env");
        logger.debug("Value of env = " + env);
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channels = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails details = new PartnerManagement().getPartnerDetails(sessionId, channels, Integer.parseInt(pId));
        File file = File.createTempFile(details.getPartnerName(), ".cer");
        filePath = file.getCanonicalPath();
        try {
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filePath);
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            FileOutputStream out = new FileOutputStream(filePath);
            if (env.equalsIgnoreCase("test")) {
                out.write(details.getCertData());
            } else {
                out.write(details.getCertDataForLive());
            }
            out.close();
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filePath)).getName();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (Exception e) {
            logger.error("Exception at DownloadCert ", e);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
