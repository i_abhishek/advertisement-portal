/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;


import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UpdateSafariDetailsOld", urlPatterns = {"/UpdateSafariDetailsOld"})
public class UpdateSafariDetailsOld extends HttpServlet {

    static final Logger logger = Logger.getLogger(UpdateSafariDetailsOld.class);
    private static final int BUFFER_SIZE = 4096;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Safari details updated successfully";
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is UpdateSafariDetails for PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String domainName = request.getParameter("domainName");
        logger.debug("value of domainName : " + domainName);
        String siteName = request.getParameter("siteName");
        logger.debug("value of siteName : " + siteName);
        String websitePushID = request.getParameter("websitePushID");
        logger.debug("value of websitePushID : " + websitePushID);
        String p12Password = request.getParameter("p12Password");
        logger.debug("value of websitePushID : " + p12Password);
        
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails pdetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String iconSet = (String) request.getSession().getAttribute("_IconSetUploaded");
        String _p12FileForSafari = (String) request.getSession().getAttribute("_p12FileForSafari");
        String p12FileName = (String) request.getSession().getAttribute("_uploadedp12FileName");
        SgUsers sgUserObj = new UsersManagement().getSgUsersByPartnerId(pdetails.getPartnerId());
        //String webSiteName = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.webSiteName");
        //String websitePushID = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.websitePushID");
        //String urlFormatString = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.urlFormatString");
        String authenticationToken = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.authenticationToken");
        String webServiceURL = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.webServiceURL");
        
        String phpServiceURL = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.pushPackageCreator");
        String unzipIconSet = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.iconSetLocation");
        String dirName = LoadSettings.g_sSettings.getProperty("safari.websiteJSON.websitJsonLocation");
        String p12FileLocation = LoadSettings.g_sSettings.getProperty("safari.p12.fileLocation");
        String phpFileLocation = LoadSettings.g_sSettings.getProperty("safari.createPackage.phpFileLocation");
        try{
            String fileSeprator = System.getProperty("file.separator");
            if(_p12FileForSafari == null){
                json.put("_result", "error");
                json.put("_message", "Please upload your valid p12");
                out.print(json);
                out.flush();
                return;
            }else{                
                File dest = new File(p12FileLocation+fileSeprator+websitePushID+fileSeprator);
                if(!dest.isDirectory()){
                    dest.mkdir();
                }
                File sourceFile = new File(_p12FileForSafari);               
                FileUtils.copyFileToDirectory(sourceFile, dest);                
            }
        // copy php pushPackageCreator File 
            File dest = new File(p12FileLocation+fileSeprator);
            if (!dest.isDirectory()) {
                dest.mkdir();
            }
            File sourceFile = new File(p12FileLocation + fileSeprator + "originalPHP" + fileSeprator + "createPushPackage.php");
            FileUtils.copyFileToDirectory(sourceFile, dest);  
        // filepath for pushPackage
        String strPath = "";
        //String savepath = "";
        String strPushPackageSave="";
        String defaultPathForIconSet = "";
        boolean unzipStatus = false;
        URL myAppUrl = new URL(domainName);
        strPath =  System.getProperty("catalina.home");
        if (strPath == null) {
            strPath = System.getenv("catalina.home");
        }
        strPath += System.getProperty("file.separator");
//        strPath += "webapps";
//        strPath += System.getProperty("file.separator");
        
//        savepath = strPath;
//        savepath += "safari_push_package_creator";
//        savepath += System.getProperty("file.separator");        
//        savepath += "pushPackage.raw";
//        savepath += System.getProperty("file.separator");
        
        //String dirName = savepath +"website.json";

        defaultPathForIconSet = strPath;
        defaultPathForIconSet += "serviceguard-settings";
        defaultPathForIconSet += System.getProperty("file.separator");  
        defaultPathForIconSet += "icon.iconset.zip";
        
        strPushPackageSave = System.getProperty("catalina.home");
        if (strPushPackageSave == null) {
            strPushPackageSave = System.getenv("catalina.home");
        }
        strPushPackageSave += System.getProperty("file.separator");
        strPushPackageSave += "bin";
        strPushPackageSave += System.getProperty("file.separator");
        strPushPackageSave += "binaries";
        strPushPackageSave += System.getProperty("file.separator");
        if (iconSet != null) {
            File file = new File(iconSet);
            byte[] imgFileData = Files.readAllBytes(file.toPath());
            String imgBase64 = Base64.encode(imgFileData);            
            if(sgUserObj != null){
                sgUserObj.setSafariIconSet(imgBase64);
                sgUserObj.setSafariDomainName(domainName);
                sgUserObj.setSafariSiteName(siteName);
            }
            unzipStatus = unzip(iconSet, unzipIconSet);
            file.delete();
        }else{
            File file = new File(defaultPathForIconSet);
            byte[] imgFileData = Files.readAllBytes(file.toPath());
            String imgBase64 = Base64.encode(imgFileData);            
            if(sgUserObj != null){
                sgUserObj.setSafariIconSet(imgBase64); 
                sgUserObj.setSafariDomainName(domainName);
                sgUserObj.setSafariSiteName(siteName);
            }
            unzipStatus = unzip(defaultPathForIconSet, unzipIconSet);
        }  
        
        String web = "{\n" +
"    \"websiteName\": "+'"'+siteName+'"'+",\n" +
"    \"websitePushID\": "+'"'+websitePushID+'"'+",\n" +
"    \"allowedDomains\": ["+'"'+domainName+'"'+"],\n" +
"    \"urlFormatString\": "+'"'+domainName+"/%@"+'"'+",\n" +
"    \"authenticationToken\": "+'"'+authenticationToken+'"'+",\n" +
"    \"webServiceURL\": "+'"'+webServiceURL+'"'+"\n" +
"}";
        
            FileWriter file = new FileWriter(dirName);
            file.write(web);
            file.flush();
            int fileEdit = editPHPFile(phpFileLocation,websitePushID+fileSeprator+p12FileName,p12Password);
            json.put("_result", result);
            json.put("_message", message);
            request.getSession().setAttribute("_IconSetUploaded", null);
            String filePath = downloadFile(phpServiceURL,strPushPackageSave,myAppUrl.getHost()+".zip");
            if(filePath != null){
                if(sgUserObj != null){
                    int retValue = new UsersManagement().editSgUsers(SessionId, ChannelId, sgUserObj);
                    if(retValue == 0){
                        SgUsers userObj = new UsersManagement().getSgUsersByUserId(SessionId, ChannelId, sgUserObj.getUserid());
                        request.getSession().setAttribute("_SgUsers", userObj);
                    }
                }
            }else{
                json.put("_result", "error");
                json.put("_message", "Error while creating Push Package");
            }
            
            out.print(json);
            out.flush();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean unzip(String zipFilePath, String destDirectory) throws IOException {
        try{
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
        
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
    
    private int editPHPFile(String fileLocation, String p12FileName, String password){
        try
             {
             File file = new File(fileLocation);
             BufferedReader reader = new BufferedReader(new FileReader(file));
             String line = "", oldtext = "";
             while((line = reader.readLine()) != null)
                 {
                 oldtext += line + "\r\n";
             }
             reader.close();
             // replace a word in a file
             //String newtext = oldtext.replaceAll("drink", "Love");
            
             //To replace a line in a file
             String newtext = oldtext.replaceAll("web.p12", p12FileName);
             newtext = newtext.replaceAll("web", password);
             FileWriter writer = new FileWriter(fileLocation);
             writer.write(newtext);
             writer.close();
             return 0;
         }
         catch (IOException ioe)
             {
             ioe.printStackTrace();
             return -1;
         }
    }
    
    public static String downloadFile(String fileURL, String saveDir, String fileName) throws IOException {
        String saveFilePath = null;
        try {
            URL url = new URL(fileURL);
            int BUFFER_SIZE = 4096;
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //String fileName = "";
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String contentType = httpConn.getContentType();
                int contentLength = httpConn.getContentLength();

//            if (disposition != null) {
//                // extracts file name from header field
//                int index = disposition.indexOf("filename=");
//                if (index > 0) {
//                    fileName = disposition.substring(index + 10,
//                            disposition.length() - 1);
//                }
//            } else {
//                // extracts file name from URL
//                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
//                        fileURL.length());
//            }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + fileName);

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                saveFilePath = saveDir + fileName;

                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded");

            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                return null;
            }
            httpConn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return saveFilePath;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
