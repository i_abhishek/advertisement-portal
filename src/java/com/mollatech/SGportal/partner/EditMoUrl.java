/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author Manoj Sherkhane
 */
@WebServlet(name = "EditMoUrl", urlPatterns = {"/EditMoUrl"})
public class EditMoUrl extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditMoUrl.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "url updated successfully";
        PrintWriter out = response.getWriter();
        String moUrl = request.getParameter("moUrl");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String numberId = request.getParameter("numberID");
        int numId = 0;
        try {
            if (numberId != null && !numberId.isEmpty()) {
                numId = Integer.parseInt(numberId);
            }
            SgNumbers num = new NumbersManagement().getMobileNumberById(SessionId, numId);
            num.setMo_URL(moUrl);
            int ret = new NumbersManagement().updateDetails(num);
            if (ret == 0) {
                json.put("_result", result);
                json.put("_message", message);
            } else {
                result = "error";
                message = "url updation failed";
                json.put("_result", result);
                json.put("_message", message);
            }

            out.print(json);
            out.flush();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
