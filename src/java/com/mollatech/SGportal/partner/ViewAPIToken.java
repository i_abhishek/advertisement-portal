package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ViewAPIToken", urlPatterns = { "/ViewAPIToken" })
public class ViewAPIToken extends HttpServlet {

    static final Logger logger = Logger.getLogger(ViewAPIToken.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #ViewAPIToken from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "";        
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String apName = request.getParameter("apNameAT");
        logger.debug("Value of apName  = " + apName);
        String resourceName = request.getParameter("resourceNameAT");
        logger.debug("Value of resourceName  = " + resourceName);
        String version = request.getParameter("version");
        logger.debug("Value of No. version  = " + version);
        String apiName = request.getParameter("apiName");
        logger.debug("Value of AP Name  = " + apiName);
        String key = apName + ":" + resourceName + ":" + version + ":" + apiName;
        try {
            if (partnerObj.getApiLevelToken() == null) {
                result = "error";
                message = "Token detail not found";
                json.put("result", result);
                logger.debug("Response of ViewAPIToken Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of ViewAPIToken Servlet's Parameter  message is " + message);
            } else {
                String apiToken = partnerObj.getApiLevelToken();
                JSONObject jsonApi = new JSONObject(apiToken);
                if (jsonApi.has(key)) {
                    String tokenDetails = jsonApi.getString(key);
                    result = "success";
                    message = "Your token for API " + apiName + " is " +tokenDetails;
                    String encodeTokenDetails = new String(Base64.encode(tokenDetails.getBytes()));
                    json.put("result", result);
                    logger.debug("Response of ViewAPIToken Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    json.put("tokenDetails",tokenDetails);
                    json.put("encodeTokenDetails", encodeTokenDetails);
                    logger.debug("Response of ViewAPIToken Servlet's Parameter  message is " + message);
                } else {
                    result = "error";
                    message = "Token not assign to " + apiName;
                    json.put("result", result);
                    logger.debug("Response of ViewAPIToken Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of ViewAPIToken Servlet's Parameter  message is " + message);
                }
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #ViewAPIToken from #PPortal " + e);
        } finally {
            logger.info("Response of #ViewAPIToken from #PPortal " + json.toString());
            logger.info("Response of #ViewAPIToken from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
