/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.advertisement;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisementPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserPaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import com.stripe.model.Subscription;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "SubscribeAdertisementPackage", urlPatterns = {"/SubscribeAdertisementPackage"})
public class SubscribeAdertisementPackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(SubscribeAdertisementPackage.class);

    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #SubscribePackage from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "Advertisement Portal";
        String result = "success";
        String message = "Package subscriped successfully";
        String channelId = (String) request.getSession().getAttribute("_advertiserChannelId");
        String SessionId = (String) request.getSession().getAttribute("_advertisorSessionId");
        SgAdvertiserDetails advertiserObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");        
        String packageName = (String) request.getSession().getAttribute("_originalPackageName");
        String invoiceId = (String) request.getSession().getAttribute("_invoiceId");
        String paidAmount = (String) request.getSession().getAttribute("_grossAmount");                    
        
        DecimalFormat df = new DecimalFormat("#0.00");
        int retValue = -1;
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        SgAdCreditInfo info = null;
        float pAmount = 0;
        
        Double packageAmount = null;

        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");
        int productType = 3;
        Subscription charge = (Subscription) request.getSession().getAttribute("chargeObj");
        if (charge != null) {
            try {
                if (paidAmount != null) {
                    pAmount = Float.parseFloat(paidAmount);
                }
                SgAdvertiseSubscriptionDetails subscriptionObject = new SgAdvertiseSubscriptionDetails();                               
                int multiple=1;
                // add the subscription package details of a advertiser
                SgApprovedAdPackagedetails packageObject = new ApprovedAdPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
                subscriptionObject.setChannelId(packageObject.getChannelId());
                subscriptionObject.setCreationDate(new Date());                
                subscriptionObject.setAdvertiserId(advertiserObj.getAdId());
                subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                subscriptionObject.setStatus(GlobalStatus.UNPAID);
                subscriptionObject.setTax(packageObject.getTax());
                subscriptionObject.setMainCredits(packageObject.getMainCredits());                
                subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                subscriptionObject.setPushAdConfiguration(packageObject.getPushAdConfiguration());
                subscriptionObject.setEmailAdConfiguration(packageObject.getEmailAdConfiguration());
                subscriptionObject.setPdfAdConfiguration(packageObject.getPdfAdConfiguration());
                subscriptionObject.setPackageDuration(packageObject.getPackageDuration());
                subscriptionObject.setRecurrenceBillingPlanId(packageObject.getRecurrenceBillingPlanId());
                subscriptionObject.setPackageName(packageObject.getPackageName());
                int days = 0; int billingDays = 0;
                if (packageObject.getPackageDuration().equalsIgnoreCase("daily")) {
                    days = 1;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("weekly")) {
                    days = 7;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("biMonthly")) {
                    days = 15;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("monthly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodMonthly);
                        //multiple=2;
                        multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.mothly"));
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("quarterly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodQuaterly);
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("halfYearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodHalfYearly);
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("yearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodYearly);                        
                        multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.yearly"));
                    }
                    days = billingDays;
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, days);
                subscriptionObject.setExpiryDate(calendar.getTime());
                retValue = new AdvertiserSubscriptionManagement().CreateSubscriptionDetails(SessionId, channelId, subscriptionObject);
                // addtion of subscription package details end
                if (retValue > 0) {
                    // assign a credit to developer
                    info = new AdvertiserCreditManagement().getDetails(subscriptionObject.getAdvertiserId());
                    if (info != null) {
                        info.setStatus(GlobalStatus.UPDATED);
                        info.setMainCredit(subscriptionObject.getMainCredits() + info.getMainCredit());
                        info.setPdfAdDetails(subscriptionObject.getPdfAdConfiguration());
                        info.setPushAdDetails(subscriptionObject.getPushAdConfiguration());                        
                        info.setEmailAdDetails(subscriptionObject.getEmailAdConfiguration());
                        new AdvertiserCreditManagement().updateDetails(info);
                    } else {
                        info = new SgAdCreditInfo();
                        info.setStatus(GlobalStatus.SUCCESS);
                        info.setPdfAdDetails(subscriptionObject.getPdfAdConfiguration());
                        info.setPushAdDetails(subscriptionObject.getPushAdConfiguration());                        
                        info.setEmailAdDetails(subscriptionObject.getEmailAdConfiguration());
                        info.setMainCredit(subscriptionObject.getMainCredits());
                        info.setAdvertiserId(subscriptionObject.getAdvertiserId());
                        new AdvertiserCreditManagement().addDetails(info);
                    }
                    // credit assignment end                    
                    if (retValue > 0) {
                        subscriptionObject.setStatus(GlobalStatus.PAID);
                        new AdvertiserSubscriptionManagement().updateDetails(subscriptionObject);
                    }    
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                    Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                    String[] operatorEmail = null;
                    if (operatorObj != null) {
                        operatorEmail = new String[operatorObj.length];
                        for (int i = 0; i < operatorObj.length; i++) {
                            operatorEmail[i] = (String) operatorObj[i].getEmailid();
                        }
                    }                    
                    
                    String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.advertiser.payment");
                    String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                    String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                    String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                    String[] enquiryEmailDetails = enquiryId.split(":");
                    String[] supportEmailDetails = supportId.split(":");
                    String[] ideaEmailDetails = ideaId.split(":");                                                            
                    
                    MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                    String schemes = "http";
                    if (config != null) {
                        if (config.apssl.equalsIgnoreCase("yes")) {
                            schemes = "https";
                        }
                    }
                    String path = request.getContextPath();
                    String dashboardURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
                    String apihref = schemes + "://" + config.aphostIp + ":" + config.aphostPort + "/APIDoc";
                    String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                    String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                    String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                    String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                    String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                    
                    String bucketName = packageName.toLowerCase();                
                    if (bucketName.contains("basic") && bucketName.contains("month")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
                        packageName = "Student";
                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
                        packageName = "Student";
                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                        packageName = "Enterprise";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                        packageName = "Enterprise";
                    }
                    
                    if (tmessage != null) {
                        tmessage = tmessage.replaceAll("#partnerName#", advertiserObj.getAdvertisername());
                        tmessage = tmessage.replaceAll("#channel#", channelName);
                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
                        tmessage = tmessage.replaceAll("#package#", packageName);
                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(df.format(pAmount)));
                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                        tmessage = tmessage.replaceAll("#email#", advertiserObj.getEmail());

                        tmessage = tmessage.replaceAll("#apihref#", apihref);
                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                        tmessage = tmessage.replaceAll("#supportId#", supportId);
                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                    }
                    String mimeType[] = {"application/octet-stream"};
                    String invoiceFilePath = new PDFInvoiceManagement().createAdvertiserAPIInvoice(packageAmount, advertiserObj, advertiserObj.getComapanyAddr(), invoiceId, subscriptionObject);
                    
                    
                    String[] arrInvoicePath = {invoiceFilePath};
                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);
                    
                    // add payment details
                    SgAdvertisementPaymentdetails paymentObj = new SgAdvertisementPaymentdetails();
                                        
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidamount(pAmount);
                    paymentObj.setSubscriptionId(subscriptionObject.getAdSubscriptionId());
                    paymentObj.setAdvertiserId(advertiserObj.getAdId());
                    paymentObj.setInvoiceData(invoiceByteArray);
                    retValue = new AdvertiserPaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);                                        
                    
                    SendAdvertisementPaymentNotification signupNotification = new SendAdvertisementPaymentNotification(tmessage, channelId, SessionId, advertiserObj.getEmail(), null, arrInvoicePath, invoiceFilePath);
                    Thread signupNotificationThread = new Thread(signupNotification);
                    signupNotificationThread.start();

//                File deleteFile = new File(invoiceFilePath);
//                deleteFile.delete();
                    json.put("result", result);
                    json.put("message", message);
                    request.getSession().setAttribute("chargeObj", null);
                } else {
                    result = "error";
                    message = "Package subscription failed.";
                    json.put("result", result);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  message is " + message);
                }
                out.print(json);
                out.flush();
                out.close();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                logger.info("Response of #SubscribePackage from #PPortal " + json.toString());
                logger.info("Response of #SubscribePackage from #PPortal Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
class SendAdvertisementPaymentNotification implements Runnable {

    String email;
    String tmessage;
    String channels;
    String sessionId;
    String[] operatorEmail;
    String[] arrInvoicePath;
    String invoiceFilePath;
    public static final int PENDING = 2;
    public static final int SEND = 0;

    public SendAdvertisementPaymentNotification() {

    }

    public SendAdvertisementPaymentNotification(String message, String channel, String sessionId, String email, String[] operatorEmail, String[] arrInvoicePath, String invoiceFilePath) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;
        this.operatorEmail = operatorEmail;
        this.arrInvoicePath = arrInvoicePath;
        this.invoiceFilePath = invoiceFilePath;
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, "Payment for package made successfully", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println("Advertisement Package Payment Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println("Advertisement Package Payment Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }
        File deleteFile = new File(invoiceFilePath);
        deleteFile.delete();
    }
}
