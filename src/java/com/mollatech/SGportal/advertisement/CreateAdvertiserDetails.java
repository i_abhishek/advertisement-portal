/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.advertisement;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreateAdvertiserDetails", urlPatterns = {"/CreateAdvertiserDetails"})
public class CreateAdvertiserDetails extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreateAdvertiserDetails.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        logger.info("Request servlet is #RegisterDeveloperProductionDetail from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Your account created successfully, Please check your email for password.";
        PrintWriter out = response.getWriter();
        
        String advertiserName = request.getParameter("partnerName");
        String mobileNo = request.getParameter("mobileNo");
        String emailId = request.getParameter("emailId");
        String comapnyName = request.getParameter("comapnyName");
        String regNo = request.getParameter("regNo");
        String landlineNo = request.getParameter("landlineNo");
        String companyAddress = request.getParameter("companyAddress");
        String cityName = request.getParameter("cityName");
        String state = request.getParameter("state");
        String country = request.getParameter("country");
        String pinCode = request.getParameter("pinCode");                
        String zippath = (String) request.getSession().getAttribute("_KYCUploaded");        

        logger.debug("value of advertiserName : " + advertiserName);
        logger.debug("value of mobileNo : " + mobileNo);
        logger.debug("value of emailId : " + emailId);
        logger.debug("value of comapnyName : " + comapnyName);
        logger.debug("value of regNo : " + regNo);
        logger.debug("value of landlineNo : " + landlineNo);
        logger.debug("value of companyAddress : " + companyAddress);
        logger.debug("value of cityName : " + cityName);
        logger.debug("value of state : " + state);
        logger.debug("value of country : " + country);
        logger.debug("value of pinCode : " + pinCode);
        logger.debug("value of zippath : " + zippath);
        if (zippath == null) {
            try {
                message = "Please attach document in zip file format";
                json.put("_result", "error");
                logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is error");
                json.put("_message", message);
                logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        
        SessionManagement sManagement = new SessionManagement();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        
        byte[] compressed_document = null;
        String mimeType[] = {"application/octet-stream"};
        if (zippath != null) {
            Path path = Paths.get(zippath);
            compressed_document = Files.readAllBytes(path);
        }
        
        int retValue = -1;

        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.advertiser.signUp");
        retValue = new AdvertiserManagement().checkIsUniqueInAdvertiser(SessionId, channel.getChannelid(), advertiserName, emailId, mobileNo);

        if (retValue != 0) {
            result = "error";
            message = "Email id or Phone no is already in use, Please try with other";
            try {
                json.put("_result", result);
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of #registerPartner from #PPortal Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        String token = advertiserName + emailId + new Date() + mobileNo;
        byte[] SHA1hash = CommonUtility.SHA1(token);
        token = new String(Base64.encode(SHA1hash));
        String advertiserpassword = token.substring(0, 9);
        SgAdvertiserDetails advertiserDetails = new SgAdvertiserDetails();
        advertiserDetails.setAdvertisername(advertiserName);
        advertiserDetails.setChannelId(channel.getChannelid());
        advertiserDetails.setCity(country);
        advertiserDetails.setComapanyAddr(companyAddress);
        advertiserDetails.setCompanyName(comapnyName);
        advertiserDetails.setCompanyState(state);
        advertiserDetails.setCountry(country);
        advertiserDetails.setCreationDate(new Date());
        advertiserDetails.setEmail(emailId);
        advertiserDetails.setKycDocument(compressed_document);
        advertiserDetails.setPhone(mobileNo);
        advertiserDetails.setPincode(pinCode);
        advertiserDetails.setRegistrationNumber(regNo);
        advertiserDetails.setLandlineNumber(landlineNo);
        advertiserDetails.setPassword(advertiserpassword);
        advertiserDetails.setStatus(GlobalStatus.ACTIVE);
        retValue = new AdvertiserManagement().CreateAdvertiser(SessionId, channel.getChannelid(), advertiserDetails);
         
        if (retValue >= 0) {
            
            tmessage = tmessage.replaceAll("#email#", emailId);
            tmessage = tmessage.replaceAll("#password#", advertiserpassword);
            tmessage = tmessage.replaceAll("#datetime#", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            int productType = 3;
            String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
            String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
            String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
            String[] enquiryEmailDetails = enquiryId.split(":");
            String[] supportEmailDetails = supportId.split(":");
            String[] ideaEmailDetails = ideaId.split(":");
            
            MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channel.getChannelid(), SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                String schemes = "http";
                if (config != null) {
                    if (config.apssl.equalsIgnoreCase("yes")) {
                        schemes = "https";
                    }
                }
            String path = request.getContextPath();    
            String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                String webSiteURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
            tmessage = tmessage.replaceAll("#supportId#", supportId);
            tmessage = tmessage.replaceAll("#ideaId#", ideaId);

            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);    
            
            tmessage = tmessage.replace("#webSiteURL#", webSiteURL);
            String filePath[] = {zippath};
//            SGStatus status = new SendNotification().SendEmailWithAttachment(channels, sendEmailTo, "Production Request of " + partnerObj.getPartnerName(), tmessage, null, null, filePath, mimeType, 1);
            
            SendRegistrationNotification signupNotification = new SendRegistrationNotification(tmessage, channel.getChannelid(), SessionId, emailId);
            Thread signupNotificationThread = new Thread(signupNotification);
            signupNotificationThread.start();
//            SGStatus status = new SendNotification().SendEmail(channel.getChannelid(), emailId, "Welcome to advertisement portal ", tmessage, null, null, null, mimeType, productType);
//            if (status.iStatus != PENDING && status.iStatus != SEND) {
//                message = "Details saved but failed to send the email notification to the Admin, Please contact admin";
//                try {
//                    json.put("_result", "error");
//                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is error");
//                    json.put("_message", message);
//                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
            request.getSession().setAttribute("_advertisorSessionId", SessionId);
            request.getSession().setAttribute("_advertiserChannelId", channel.getChannelid());
            request.getSession().setAttribute("_advertisorDetails", advertiserDetails);
        } else {
            result = "error";
            message = "Error while creating your account.";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class SendRegistrationNotification implements Runnable {

    String email;
    String tmessage;
    String channels;
    String sessionId;   
    public static final int PENDING = 2;
    public static final int SEND = 0;

    public SendRegistrationNotification() {

    }

    public SendRegistrationNotification(String message, String channel, String sessionId, String email) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;        
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, "Welcome to advertisement portal", tmessage, null, null, null, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println("Advertisement Package Payment Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println("Advertisement Package Payment Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }       
    }
}