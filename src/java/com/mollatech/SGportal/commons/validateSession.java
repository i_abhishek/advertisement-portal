package com.mollatech.SGportal.commons;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Sessions;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "validateSession", urlPatterns = {"/validateSession"})
public class validateSession extends HttpServlet {

    static final Logger logger = Logger.getLogger(validateSession.class);

    final int ACTIVE_STATUS = 1;

    final int SUSPEND_STATUS = 0;

    private static SgUsers operators = null;

    private static String channelId = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Requested Servlet is validateSession at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (sessionId == null) {
                request.getSession().setAttribute("_partnerSessionId", null);
                json.put("_result", "error");
                return;
            }
            SessionManagement sManagement = new SessionManagement();
            int res = sManagement.GetSessionStatus(sessionId);
            if (res == GlobalStatus.ACTIVE) {
                sManagement.UpdateSession(sessionId);
            } else {
                request.getSession().setAttribute("_partnerSessionId", null);
                json.put("_result", "error");
                return;
            }
        } catch (Exception ex) {
            logger.error("Exception at validateSession ", ex);
            json.put("_result", "error");
            return;
        } finally {
            out.print(json);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
