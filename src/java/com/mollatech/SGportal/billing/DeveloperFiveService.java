/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeveloperFiveService", urlPatterns = {"/DeveloperFiveService"})
public class DeveloperFiveService extends HttpServlet {

    int ActiveStatus = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            SgAdvertiserDetails usrObj = (SgAdvertiserDetails) request.getSession().getAttribute("_advertisorDetails");
//            ResourceManagement resourseObj = new ResourceManagement();
//            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            //SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, (-30));
            Date froDate1 = cal.getTime();
            froDate1.setHours(00);
            froDate1.setMinutes(00);
            froDate1.setSeconds(00);
           // Map<Integer, String> resourceMap = new HashMap();
//            if (resoursedetails != null) {
//                for (ResourceDetails resData : resoursedetails) {
//                    resourceMap.put(resData.getResourceId(), resData.getName());
//                }
//            }
            AdvertisementTrackingManagement requesTr = new AdvertisementTrackingManagement();
            SgAdvertisertracking[] pdfAdtx = requesTr.getTxDetailsByAdvatisorId(usrObj.getAdId(), froDate1, endDate, GlobalStatus.PDF_AD);
            SgAdvertisertracking[] emailAdtx = requesTr.getTxDetailsByAdvatisorId(usrObj.getAdId(), froDate1, endDate, GlobalStatus.EMAIL_AD);
            SgAdvertisertracking[] pushAdtx = requesTr.getTxDetailsByAdvatisorId(usrObj.getAdId(), froDate1, endDate, GlobalStatus.PUSH_AD);
            int pdfAdLength = 0; int emailAdLength = 0; int pushAdLength = 0;
            if(pdfAdtx != null){
                pdfAdLength = pdfAdtx.length;
            }
            if(emailAdtx != null){
                emailAdLength = emailAdtx.length;
            }
            if(pushAdtx != null){
                pushAdLength = pushAdtx.length;
            }
            
            sample.add(new ApiDashboard("PDF Ad", pdfAdLength));
            sample.add(new ApiDashboard("Email Ad", emailAdLength));
            sample.add(new ApiDashboard("Push Ad", pushAdLength));

        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
