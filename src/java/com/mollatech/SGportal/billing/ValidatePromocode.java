package com.mollatech.SGportal.billing;

import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.SgPromocodeUsage;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeUsageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ValidatePromocode", urlPatterns = {"/ValidatePromocode"})
public class ValidatePromocode extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Promo code validate sucessfully!!!";
        PrintWriter out = response.getWriter();
        String promoCode = request.getParameter("promocode");
        String totalPayment = request.getParameter("totalPaymentAmount");
        String bucketName = request.getParameter("packageName");
        String paymentType = request.getParameter("paymentType");
        String promoPackage = request.getParameter("_promoPackage");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        int partnerId = (Integer) request.getSession().getAttribute("_partnerID");
        double itotalPayment   = 0;
        double benefitDiscount = 0;
        NumberFormat form = new DecimalFormat("#0.00");
        if (totalPayment != null) {
            itotalPayment = Double.parseDouble(totalPayment);
        }
        try {
            JSONObject featJSONObj = null;
            if (paymentType.equalsIgnoreCase("postpaid")) {
                SgSubscriptionDetails packageObject = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerId);
                if (packageObject != null) {
                    String feature = packageObject.getFeatureList();
                    if (feature != null) {
                        JSONArray alertJson = new JSONArray(feature);
                        for (int i = 0; i < alertJson.length(); i++) {
                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                            if (jsonexists1.has(promoPackage)) {
                                featJSONObj = jsonexists1.getJSONObject(promoPackage);
                                if (featJSONObj != null) {
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, bucketName);
                if (packageObject != null) {
                    String feature = packageObject.getFeatureList();
                    if (feature != null) {
                        JSONArray alertJson = new JSONArray(feature);
                        for (int i = 0; i < alertJson.length(); i++) {
                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                            if (jsonexists1.has(promoPackage)) {
                                featJSONObj = jsonexists1.getJSONObject(promoPackage);
                                if (featJSONObj != null) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (featJSONObj != null && featJSONObj.getString("promoCodeFlag").equals("Enable")) {
                SgPromocode promoObj = new PromocodeManagement().getPromocodeByPromocode(SessionId, channelId, promoCode);
                if (promoObj != null) {
                    Date usageDate = new Date();
                    if (usageDate.before(promoObj.getExpireOn())) {
                        if (promoObj.getPartnerId().contains("all") || promoObj.getPartnerId().indexOf(String.valueOf(partnerId)) >= 0) {
                            SgPromocodeUsage[] promoUsage = new PromocodeUsageManagement().getPromocodeUsageByPartnerId(SessionId, channelId, promoObj.getPromoCodeId(), partnerId);
                            int usageCount = 0;
                            if (promoUsage != null) {
                                usageCount = promoUsage.length;
                            }
                            if (usageCount < promoObj.getPartnerUsageCount()) {
                                if (usageCount < promoObj.getUsageCount()) {
                                    json.put("_discount", promoObj.getDiscount());
                                    if (promoObj.getDiscountType().equalsIgnoreCase("flat")) {                                        
                                        benefitDiscount = itotalPayment - promoObj.getDiscount();
                                        if(benefitDiscount < 0){
                                            benefitDiscount = 0.00;
                                        }
                                        else if(benefitDiscount < 5){
                                            result = "error";
                                            message = "Sorry. This promocode cannot be processed as the discount amount lower than the minimum amount allowed.";
                                            json.put("_result", result);
                                            json.put("_message", message);
                                            return;
                                        }
                                        benefitDiscount = (double) Math.round(benefitDiscount * 100) / 100;
                                        String tmWellFormbenefitDiscount   = form.format(benefitDiscount);
                                        json.put("_benefitDesc", promoObj.getDiscount() + " In Price");
                                        json.put("_discountAmount", promoObj.getDiscount());
                                        json.put("_benefitAmout", tmWellFormbenefitDiscount);
                                    } else {                                        
                                        benefitDiscount = (itotalPayment * promoObj.getDiscount()) / 100;                                        
                                        benefitDiscount = itotalPayment - benefitDiscount;
                                        if(benefitDiscount < 0){
                                            benefitDiscount = 0.00;
                                        }
                                        else if(benefitDiscount < 5){
                                            result = "error";
                                            message = "Sorry. This promocode cannot be processed as the discount amount lower than the minimum amount allowed.";
                                            json.put("_result", result);
                                            json.put("_message", message);
                                            return;
                                        }
                                        benefitDiscount = (double) Math.round(benefitDiscount * 100) / 100;
                                        String tmWellFormbenefitDiscount   = form.format(benefitDiscount);
                                        json.put("_benefitDesc", promoObj.getDiscount() + " In percentage");
                                        json.put("_discountAmount", benefitDiscount);
                                        json.put("_benefitAmout", tmWellFormbenefitDiscount);
                                    }
                                    json.put("_promocodeId", promoObj.getPromoCodeId());
                                    request.getSession().setAttribute("promoBenefitAmount", benefitDiscount);
                                } else {
                                    result = "error";
                                    message = "Promo code usage limit exceeds";
                                }
                            } else {
                                result = "error";
                                message = "You have exceed your promo code usage limit";
                            }
                        } else {
                            result = "error";
                            message = "This is not issued promocode";
                        }
                    } else {
                        result = "error";
                        message = "Promocode is expired !";
                    }
                } else {
                    result = "error";
                    message = "Please enter valid promocode";
                }
            } else {
                result = "error";
                message = promoPackage + " package does not have promo code facility";
            }
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
