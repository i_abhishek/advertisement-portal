package com.mollatech.SGportal.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.commons.bar;
import com.mollatech.serviceguard.nucleus.db.RequestTracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

@WebServlet(name = "barchartreport", urlPatterns = {"/barchartreport"})
public class barchartreport extends HttpServlet {

    final int ALLOWED = 0;

    final int BLOCK_TIME = -1;

    final int BLOCK_DAY = -2;

    final int BLOCK_IP = -3;

    final int BLOCK_TPS = -4;

    final int BLOCK_TPD = -5;

    final int BLOCK_SSL = -6;

    final int BLOCK_TOKEN = -7;

    final int SERVICE_UNAVAILABLE = -8;

    final int BLOCK_BLACK_IP = -9;

    final int PACKAGE_NOT_SUBSCRIBED = -10;

    final int API_LEVEL_TOKEN = -11;

    final int BILLING = -12;

    final int SERVER_ERROR = -13;

    static final Logger logger = Logger.getLogger(barchartreport.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Requested Servlet is barchartreport at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        int apid = -1, resid = -1, groupid = -1, partnerid = -1, sdate = -1, edate = -1, stime = -1, etime = -1;
        String channel = (String) request.getSession().getAttribute("_ChannelId");
        String _accesspoint = request.getParameter("_accesspoint");
        logger.debug("Value of _accesspoint  = " + _accesspoint);
        if (_accesspoint != null) {
            apid = Integer.parseInt(_accesspoint);
        }
        String _resourceid = request.getParameter("_resource");
        logger.debug("Value of _resourceid  = " + _resourceid);
        if (_resourceid != null) {
            resid = Integer.parseInt(_resourceid);
        }
        String _groupid = request.getParameter("_group");
        logger.debug("Value of _groupid  = " + _groupid);
        if (_groupid != null) {
            groupid = Integer.parseInt(_groupid);
        }
        String _partner = request.getParameter("_partner");
        logger.debug("Value of _partner  = " + _partner);
        if (_partner != null) {
            partnerid = Integer.parseInt(_partner);
        }
        String _sdate = request.getParameter("_sdate");
        logger.debug("Value of _sdate  = " + _sdate);
        String _edate = request.getParameter("_edate");
        logger.debug("Value of _edate  = " + _edate);
        String _stime = request.getParameter("_stime");
        logger.debug("Value of _stime  = " + _stime);
        String _etime = request.getParameter("_etime");
        logger.debug("Value of _etime  = " + _etime);
        String _repEnv = request.getParameter("_repEnv");
        if (_repEnv == null) {
            _repEnv = "Select";
        }
        logger.debug("Value of _repEnv  = " + _repEnv);
        String channelId = channel;
        try {
            RequestTrackingManagement rtMngt = new RequestTrackingManagement();
            int iAllowedCount = 0;
            int iBTimeCount = 0;
            int iBDayCount = 0;
            int iBIPCount = 0;
            int iBTPSCount = 0;
            int iBTPDCount = 0;
            int iBSSLCount = 0;
            int iTOKENCount = 0;
            int BLACK_IP = 0;
            int PSUBSCRIBED = 0;
            int API_TOKEN = 0;
            int iBILLING = 0;
            int SERVERError = 0;
            RequestTracking[] requestTracking = rtMngt.getDetails(channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, _repEnv);
            if (requestTracking != null) {
                for (RequestTracking requestTracking1 : requestTracking) {
                    if (requestTracking1.getStatus() == ALLOWED) {
                        iAllowedCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TIME) {
                        iBTimeCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_DAY) {
                        iBDayCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_IP) {
                        iBIPCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TPS) {
                        iBTPSCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TPD) {
                        iBTPDCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_SSL) {
                        iBSSLCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TOKEN) {
                        iTOKENCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_BLACK_IP) {
                        BLACK_IP++;
                    } else if (requestTracking1.getStatus() == PACKAGE_NOT_SUBSCRIBED) {
                        PSUBSCRIBED++;
                    } else if (requestTracking1.getStatus() == API_LEVEL_TOKEN) {
                        API_TOKEN++;
                    } else if (requestTracking1.getStatus() == BILLING) {
                        iBILLING++;
                    } else if (requestTracking1.getStatus() == SERVER_ERROR) {
                        SERVERError++;
                    }
                }
            }
            ArrayList<bar> sample = new ArrayList<bar>();
            sample.add(new bar(iAllowedCount, "ALLOWED"));
            sample.add(new bar(iBTimeCount, "TIME"));
            sample.add(new bar(iBDayCount, "DAY"));
            sample.add(new bar(iBIPCount, "IP"));
            sample.add(new bar(iBTPSCount, "TPS"));
            sample.add(new bar(iBTPDCount, "TPD"));
            sample.add(new bar(iBSSLCount, "SSL"));
            sample.add(new bar(iTOKENCount, "TOKEN"));
            sample.add(new bar(BLACK_IP, "BLACK_IP"));
            sample.add(new bar(PSUBSCRIBED, "SUBSCRIPTION"));
            sample.add(new bar(API_TOKEN, "APITOKEN"));
            sample.add(new bar(iBILLING, "BILLING"));
            sample.add(new bar(SERVERError, "SERVER_ERROR"));
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } finally {
            logger.info("Response of barchartreport Servlet at " + new Date());
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
