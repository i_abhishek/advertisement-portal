/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.RequestTracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ResponseReportV2", urlPatterns = {"/ResponseReportV2"})
public class ResponseReportV2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String Channel = null, sessionId = null;
        int OperatorType = -1;
        String _OperatorType = request.getParameter("_opType");
        if (_OperatorType != null) {
            OperatorType = Integer.parseInt(_OperatorType);
        }
        sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        int partnerid = -1, apid = -1, groupid = -1, resid = -1;
        String _accesspoint = request.getParameter("_accesspoint");
        if (_accesspoint != null) {
            apid = Integer.parseInt(_accesspoint);
        }
        String _group = request.getParameter("_group");
        if (_group != null) {
            groupid = Integer.parseInt(_group);
        }
        String _resources = request.getParameter("_resources");
        if (_resources != null) {
            resid = Integer.parseInt(_resources);
        }
        String _partner = request.getParameter("_partner");
        if (_partner != null) {
            partnerid = Integer.parseInt(_partner);
        }
        String month = request.getParameter("_apiCallMonth");
        String year = request.getParameter("_apiCallYear");
        String _apiCallDay = request.getParameter("_apiCallDay");
        String _stime = request.getParameter("_stime");
        String _etime = request.getParameter("_etime");

        String _sdate = request.getParameter("_stime");
        String _edate = request.getParameter("_etime");
        RequestTracking[] res = null;
        String dates1 = null;
        long responsetime = 0;
        String datess = null;
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        RequestTrackingManagement ppw = new RequestTrackingManagement();
        try {
            Date today = new Date();
            if (Integer.parseInt(_apiCallDay) <= 9) {
                today = sdf.parse(month + "/0" + _apiCallDay + "/" + year);
            } else {
                today = sdf.parse(month + "/" + _apiCallDay + "/" + year);
            }
            String reqDate = sdf.format(today);
            ArrayList<line> sample = new ArrayList<line>();
            res = ppw.getResponseTimebyDurationDev(sessionId, Channel, apid, resid, groupid, partnerid, reqDate, reqDate, _stime, _etime);
            if (res != null && res.length != 0) {
                for (int i = 0; i < res.length; i++) {
                    if (res[i].getResponsetime() != null) {
                        responsetime = Integer.parseInt(res[i].getResponsetime());
                        datess = formatter1.format(res[i].getCreatedOn());
                        if (!datess.equals(dates1)) {
                            sample.add(new line(responsetime, datess));
                        }
                        dates1 = datess;
                    }
                }
            } else {
                Date d = new Date();
                datess = formatter1.format(d);
                sample.add(new line(responsetime, datess));
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
